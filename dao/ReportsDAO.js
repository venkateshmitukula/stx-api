var config = require('config');
var logger = require('../lib/logger');
var moment = require('moment');
var execute = require('../common/dbConnection');
var SetupWorkerDetailDAO = require('../dao/SetupWorkerDetailDAO');
var uniqid = require('uniqid');
var dateFns = require('./../common/dateFunctions');

module.exports = {
    getTicketReports: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var searchData = req.body;
            if (searchData.search === 'betweenDate') {
                var sqlQuery = 'SELECT a.*, CONCAT(c.FirstName, " ", c.LastName)as FullName, pt.Name as paymentType ,ts.Net_Price__c as servicePrice, tp.Net_Price__c as productPrice, tto.Amount__c as otherAmount, '
                    + '(ts.Net_Price__c + tp.Net_Price__c + tto.Amount__c) as Total '
                    + ' FROM `Appt_Ticket__c` as a JOIN Contact__c as c on c.Id = a.`Client__c` and c.IsDeleted = 0'
                    + ' JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id '
                    + ' JOIN Ticket_Product__c as tp on tp.Appt_Ticket__c = a.Id '
                    + ' JOIN Ticket_Other__c as tto on tto.Ticket__c = a.Id '
                    + 'JOIN Ticket_Payment__c as tpt on tpt.Appt_Ticket__c = a.Id '
                    + ' JOIN Payment_Types__c as pt on pt.Id = tpt.Payment_Type__c WHERE `Appt_Date_Time__c` BETWEEN "' + searchData.startDate.split(' ')[0] + '" AND "' + searchData.endDate.split(' ')[0] + '" GROUP BY a.Id';
            } else if (searchData.search === 'todayDate') {
                var sqlQuery = 'SELECT a.*, CONCAT(c.FirstName, " ", c.LastName)as FullName, pt.Name as paymentType ,ts.Net_Price__c as servicePrice, tp.Net_Price__c as productPrice, tto.Amount__c as otherAmount, '
                    + '(ts.Net_Price__c + tp.Net_Price__c + tto.Amount__c) as Total '
                    + ' FROM `Appt_Ticket__c` as a JOIN Contact__c as c on c.Id = a.`Client__c` and c.IsDeleted = 0'
                    + ' JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id '
                    + ' JOIN Ticket_Product__c as tp on tp.Appt_Ticket__c = a.Id '
                    + ' JOIN Ticket_Other__c as tto on tto.Ticket__c = a.Id '
                    + 'JOIN Ticket_Payment__c as tpt on tpt.Appt_Ticket__c = a.Id '
                    + ' JOIN Payment_Types__c as pt on pt.Id = tpt.Payment_Type__c WHERE `Appt_Date_Time__c` = "' + searchData.todayDate.split(' ')[0] + '" GROUP BY a.Id';
            }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in reports dao - getreportsData:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in reports dao - getreportsData:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getCashInOutRecords: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var searchData = req.body;
            if (searchData.search === 'betweenDate') {
                var sqlQuery = 'SELECT * FROM `Cash_In_Out__c` WHERE `CreatedDate` BETWEEN "' + searchData.startDate.split(' ')[0] + '" AND "' + searchData.endDate.split(' ')[0] + '"';
            } else if (searchData.search === 'todayDate') {
                var sqlQuery = 'SELECT * FROM `Cash_In_Out__c` WHERE `CreatedDate` = "' + searchData.todayDate.split(' ')[0] + '"';
            }
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in reports dao - getCashInOutRecords:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in reports dao - getCashInOutRecords:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getDailyCashDrawerRecords: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT Cash_Drawer_Number__c as Drawer, Cash_Drawer__c as DrawerName, Opening_Cash__c as OpeningCash, Closing_Cash__c as ClosingCash, Cash_Over_Under__c as CashOverUnder, Date__c as date,Status__c as Status  FROM `Daily_Cash__c` WHERE `Date__c`= "' + req.params.seledate + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in reports dao - getDailyCashDrawerRecords:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in reports dao - getDailyCashDrawerRecords:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getDailyCashDrawer: function (req, callback) {
        var dbName = req.headers['db'];
        var cashDrawer = 'Cash Drawers';
        var paymentObj = [];
        var tipsObj = [];
        var totalTips = 0.00;
        var index = 0;
        var date = req.params.date;
        var cashInOutData;
        var cashDrawerData;
        var drawerData;
        var balanceDueAmountPerAppt = [];
        var startDatetime = new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate());
        var endDatetime = new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate());
        startDatetime = moment(startDatetime).format('YYYY-MM-DD');
        endDatetime = moment(endDatetime).format('YYYY-MM-DD');
        getCashInOutTicketsBalnceDue(startDatetime, endDatetime, dbName, function (err, done) {
            for (var i = 0; i < done[0].length; i++) {
                done[0][i]['balancedue'] = done[0][i]['includAmt'];
                done[0][i]['Ticket_Total__c'] = 0;
                var filter1 = done[1].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter2 = done[2].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter3 = done[3].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter4 = done[4].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter5 = done[5].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                if (filter1.length > 0) {
                    done[0][i]['Ticket_Total__c'] += filter1[0]['tsNet_Price__c'];
                }
                if (filter2.length > 0) {
                    done[0][i]['Ticket_Total__c'] += filter2[0]['tpNet_Price__c'];
                }
                if (filter3.length > 0) {
                    done[0][i]['Ticket_Total__c'] += filter3[0]['oAmount__c'];
                }
                if (filter5.length > 0) {
                    //CHANGE BACK LOGIC -- first get the full amount of tips on a ticket
                    filter5.forEach((tipObj) => {
                        done[0][i]['Ticket_Total__c'] += parseFloat(tipObj['tipTip_Amount__c']);
                        if (tipObj['Drawer_Number__c']) {
                            tipsObj.push({
                                'apptTicketName': done[0][i]['apptName'],
                                'paymentType': tipObj['Tip_Option__c'],
                                'paymentAmount': tipObj['tipTip_Amount__c'],
                                'receivedBy': tipObj['lastModify'],
                                'clientName': done[0][i]['FullName'],
                                'drawerNumber': tipObj['Drawer_Number__c'],
                                // 'changeBackAmount': (done[0][i]['balancedue'] + totalTips),
                                // 'balanceDue': done[0][i]['balancedue']
                            });
                        }
                    });
                }
                done[0][i]['payments'] = 0;
                var lastPaymentTime = 0;
                var drawerNumber = '';
                if (filter4.length > 0) {
                    filter4.forEach((payObj, j) => {
                        var time = dateFns.getDateTmFrmDBDateStr(payObj['LastModifiedDate']).getTime();
                        if (j === 0) {
                            lastPaymentTime = time;
                            drawerNumber = payObj['Drawer_Number__c'];
                        } else {
                            if (time > lastPaymentTime) {
                                lastPaymentTime = time;
                                drawerNumber = payObj['Drawer_Number__c'];
                            }
                        }
                        done[0][i]['payments'] += payObj['tpyAmount_Paid__c'];

                        if (payObj['Drawer_Number__c'] || payObj['Drawer_Number__c'] === 0) {
                            if (payObj['paymentType'] === 'Gift Redeem' &&
                                payObj['tpyAmount_Paid__c'] === done[0][i]['Ticket_Total__c']) {
                                // paymentObj.push([]);
                            } else {
                                paymentObj.push({
                                    'apptTicketName': done[0][i]['apptName'],
                                    'paymentType': payObj['paymentType'],
                                    'paymentAmount': payObj['tpyAmount_Paid__c'],
                                    'drawerNumber': payObj['Drawer_Number__c'],
                                    'receivedBy': payObj['lastModify'],
                                    'clientName': done[0][i]['FullName'],
                                    'isRefund': done[0][i]['isRefund__c'],
                                    // 'balanceDue': done[0][i]['balancedue']
                                });
                            }

                        }
                        paymentObj = paymentObj.sort(function (a, b) {
                            return b.drawerNumber - a.drawerNumber
                        });
                    });
                    if (drawerNumber === 0 || drawerNumber) {
                        var balancedue = done[0][i]['balancedue'] + done[0][i]['Ticket_Total__c'] - done[0][i]['payments'];
                        balanceDueAmountPerAppt.push({ apptTicketName: done[0][i]['apptName'] + drawerNumber, 'balanceDue': balancedue })
                    }
                }
                //CHANGE BACK LOGIC
                //need to create new DTO for change back line on every ticket iteration
                //to get the change back amount, add total tips to the balance due (which will always be negative or 0 on a completed ticket)
                // changeBackAmount = (done[0][i]['balancedue'] + totalTips);
            }
            index++;
            sendDailyCashDetails(index, paymentObj, tipsObj, cashInOutData, cashDrawerData, drawerData, balanceDueAmountPerAppt, callback)
        });
        getDailyCashDrawerDateRange(cashDrawer, startDatetime, endDatetime, dbName, function (err, data) {
            cashDrawerData = data[1];
            cashInOutData = data[2]
            drawerData = JSON.parse(data[3][0].JSON__c);
            index++;
            sendDailyCashDetails(index, paymentObj, tipsObj, cashInOutData, cashDrawerData, drawerData, balanceDueAmountPerAppt, callback)
        });
    },
    getTicketDetails: function (req, callback) {
        var dbName = req.headers['db'];
        var userId = req.headers['id'];
        var index = 0;
        var cashInOutdata = [];
        var ticketsBalnceDue;
        getAllTicketsForDateRangeOrderedByTicketNumber(req.params.stdate, req.params.eddate, dbName, function (err, done) {
            for (var i = 0; i < done[0].length; i++) {
                done[0][i]['balancedue'] = 0;
                done[0][i]['Ticket_Total__c'] = done[0][i]['Included_Ticket_Amount__c'];
                done[0][i]['Other_Sales__c'] = 0;
                done[0][i]['Product_Sales__c'] = 0;
                done[0][i]['Service_Sales__c'] = 0;
                var filter1 = done[1].filter(function (a) { return a['Id'] === done[0][i]['appId'] });
                var filter2 = done[2].filter(function (a) { return a['Id'] === done[0][i]['appId'] })
                var filter3 = done[3].filter(function (a) { return a['Id'] === done[0][i]['appId'] })
                var filter4 = done[4].filter(function (a) { return a['Id'] === done[0][i]['appId'] })
                if (filter1.length > 0) {
                    done[0][i]['balancedue'] += filter1[0]['tsNet_Price__c'];
                    done[0][i]['Ticket_Total__c'] += filter1[0]['tsNet_Price__c'];
                    done[0][i]['Service_Sales__c'] += filter1[0]['Net_Price__c'];
                }
                if (filter2.length > 0) {
                    done[0][i]['balancedue'] += filter2[0]['tpNet_Price__c'];
                    done[0][i]['Ticket_Total__c'] += filter2[0]['tpNet_Price__c'];
                    done[0][i]['Product_Sales__c'] += filter2[0]['Net_Price__c'];
                }
                if (filter3.length > 0) {
                    done[0][i]['balancedue'] += filter3[0]['oAmount__c'];
                    done[0][i]['Ticket_Total__c'] += filter3[0]['oAmount__c'];
                    done[0][i]['Other_Sales__c'] += filter3[0]['oAmount__c'];
                }
                if (filter4.length > 0) {
                    if (done[0][i]['isRefund__c'] == 1) {
                        done[0][i]['Abbreviation__c'] = 'Refund';
                    } else {
                        done[0][i]['Abbreviation__c'] = filter4[0]['Abbreviation__c'];
                    }
                    done[0][i]['balancedue'] -= filter4[0]['tpyAmount_Paid__c'];
                }

            }
            ticketsBalnceDue = done[0];
            index++;
            sendTicketDetails(index, cashInOutdata, ticketsBalnceDue, callback)
        });
        getCashInOutTicketsForDateRange(userId, req.params.stdate, req.params.eddate, dbName, function (err, data) {
            cashInOutdata = data;
            index++;
            sendTicketDetails(index, cashInOutdata, ticketsBalnceDue, callback)
        });
    },
    saveProcessCompensationReport: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var records = [];
        var compRunIds = [];
        var cmpSql = '';
        try {
            var cmpRunObj = req.body.archiveArrObj;
            for (var i = 0; i < cmpRunObj.length; i++) {
                cmpSql += `SELECT Steps__c FROM Compensation__c WHERE Id = '` + cmpRunObj[i]['compensationId'] + `' AND isDeleted = 0;`
            }
            execute.query(dbName, cmpSql, '', function (err, result) {
                if (err) {
                    logger.error('Error in - saveProcessCompensationReport:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    for (var i = 0; i < cmpRunObj.length; i++) {
                        const stepC = [];
                        if (result.length === 1) {
                            stepC.push(result[0]['Steps__c']);
                        } else {
                            stepC.push(result[i][0]['Steps__c'])
                        }
                        records.push([uniqid(), loginId,
                        config.booleanFalse,
                        dateFns.getUTCDatTmStr(new Date()), loginId,
                        dateFns.getUTCDatTmStr(new Date()), loginId,
                        dateFns.getUTCDatTmStr(new Date()),
                        req.body['sDate'],
                        req.body['eDate'],
                        cmpRunObj[i]['compensationName'],
                        cmpRunObj[i]['compensationAmount'],
                        cmpRunObj[i]['daysWorked'],
                        cmpRunObj[i]['deduction'],
                        cmpRunObj[i]['extraPay'],
                        cmpRunObj[i]['hourlyWage'],
                        cmpRunObj[i]['overtimeHours'],
                        cmpRunObj[i]['regularHours'],
                        cmpRunObj[i]['salary'],
                            stepC,
                        cmpRunObj[i]['workerId'],
                        cmpRunObj[i]['tipAmount']
                        ]);
                        compRunIds.push({
                            'workerId': cmpRunObj[i]['workerId'],
                            'Id': records[i][0]
                        })
                    }
                    var insertQuery = 'INSERT INTO ' + config.dbTables.compensationRunTBL
                        + ' (Id, OwnerId, IsDeleted,CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,'
                        + ' SystemModstamp, Begin_Date__c, End_Date__c, Compensation_Name__c,Compensation_Total__c,Days_Worked__c,'
                        + ' Deduction__c, Extra_Pay__c, Hourly_Wage__c,Overtime_Hours__c,Regular_Hours__c,Salary__c,'
                        + ' Steps__c, Worker__c, Tip_Amount__c) VALUES ?';
                    execute.query(dbName, insertQuery, [records], function (err, result) {
                        if (err) {
                            logger.error('Error in SetupSuppliersDAO - saveSetupSuppliers:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            done(err, compRunIds);
                        }

                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupSuppliersDAO - saveSetupSuppliers:', err);
            done(err, { statusCode: '9999' });
        }
    },
    deleteProcessCompensationReport: function (req, done) {
        var dbName = req.headers['db'];
        var queries = '';
        var deleteArray = req.body;
        try {
            var inStrPar = '(';
            for (var i = 0; i < deleteArray.length; i++) {
                inStrPar = inStrPar + '"' + deleteArray[i].Id + '",'
            }
            inStrPar = inStrPar.substr(0).slice(0, -2);
            inStrPar = inStrPar + '")';
            queries += 'DELETE from Compensation_Run__c'
                + ' WHERE Id IN ' + inStrPar + ';';
            execute.query(dbName, queries, '', function (err, result) {
                if (err) {
                    logger.error('Error in getting deleteProcessCompensationReport:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in getting deleteProcessCompensationReport:', err);
            done(err, null);
        }
    },
    getProcessCompensationRun: function (req, done) {
        var dbName = req.headers['db'];
        var queries = `SELECT cr.* FROM Compensation_Run__c cr
                LEFT JOIN Compensation__c as c on c.Name = cr.Compensation_Name__c
                WHERE cr.Id = '`+ req.params.id + `'`;
        var srvcqueries = `select Service__c serviceId, DATE(ts.Service_Date_Time__c) serviceDate,
                    sum( ts.Net_Price__c ) serviceTotal, count(ts.Id) numberOfServices,
                    sum( ts.Guest_Charge__c ) guestCharge
                    from Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c
                    where ts.Worker__c = '`+ req.headers.workerid + `' and at.isTicket__c = 1
                    and ts.Is_Booked_Out__c = 0 and ts.Is_Class__c = 0
                    and DATE(ts.Service_Date_Time__c) >= '`+ req.headers.sdate + `'
                    and DATE(ts.Service_Date_Time__c) <= '`+ req.headers.edate + `'                    
                    and ts.Do_Not_Deduct_From_Worker__c = 0 AND ts.IsDeleted=0
                    group by ts.Service__c`;
        var prdtqueries = `select DATE(a.Appt_Date_Time__c) productDate,
                sum( tp.Qty_Sold__c  *  tp.Net_Price__c ) productTotal,
                sum( tp.Qty_Sold__c ) numberOfProducts
                from Ticket_Product__c tp
                LEFT JOIN Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c
                where tp.Worker__c = '`+ req.headers.workerid + `' and a.isTicket__c = 1
                and DATE(a.Appt_Date_Time__c) >= '`+ req.headers.sdate + `'
                and DATE(a.Appt_Date_Time__c) <= '`+ req.headers.edate + `'
                and tp.Do_Not_Deduct_From_Worker__c = 0 AND tp.IsDeleted=0
                group by DATE(a.Appt_Date_Time__c)`;
        var clckqueries = `select DATE(Time_In__c) workDate,
                sum( IF(Time_Out__c = null, null, 
                time_to_sec(timediff(Time_Out__c, Time_In__c )) / 3600) ) hoursWorked
                from Time_Clock_Entry__c
                where Worker__c = '`+ req.headers.workerid + `' 
                and Time_In__c IS NOT null AND Time_Out__c IS NOT null
                and DATE(Time_In__c) >= '`+ req.headers.sdate + `'
                and DATE(Time_In__c) <= '`+ req.headers.edate + `'
                AND IsDeleted=0 group by DATE(Time_In__c);
                select Service__c wserviceId,Worker__c, IFNULL(Price__c,0) Price__c, IFNULL(Service_Fee_Amount__c, 0) Service_Fee_Amount__c, 
                IFNULL(Service_Fee_Percent__c,0) Service_Fee_Percent__c
                from Worker_Service__c where Worker__c ='`+ req.headers.workerid + `' `;
        // and ts.Do_Not_Deduct_From_Worker__c = 0
        execute.query(dbName, queries + ';' + srvcqueries
            + ';' + prdtqueries + ';' + clckqueries, '', function (err, result) {
                if (err) {
                    logger.error('Error in getting getProcessCompensationRunView:', err);
                    done(err, result);
                } else {
                    execute.query(dbName, srvcqueries
                        + ';' + prdtqueries + ';' + clckqueries, '', function (err, result1) {
                            if (err) {
                                logger.error('Error in getting getProcessCompensationRunView:', err);
                                done(err, result1);
                            } else {
                                done(err, { result, result1 });
                            }
                        });
                }
            });
    },
    getProcessCompensationDetails: function (req, callback) {
        var dbName = req.headers['db'];
        var totalCompensation = 0;
        var RunForDates = {};
        var CompensationMethods = {};
        var regularHours = {};
        var tipAmount = {};
        var indexParm = 0;
        var workerIds = '('
        var workerData = [];
        SetupWorkerDetailDAO.getWorkerDetail2(req, req.headers['today'], function (err, data) {
            for (var i = 0; i < data.length; i++) {
                workerIds += '\'' + data[i]['Id'] + '\',';
                workerData = workerData.concat(data[i]);
            }
            workerIds = workerIds.slice(0, -1);
            workerIds += ')';
            retrieveWorkerTimeClockHours(req.params.stdate, req.params.eddate, workerIds, dbName, function (err, hrsdone) {
                regularHours = hrsdone;
                indexParm++;
                sendPrsCompnRpt(indexParm, workerData, RunForDates, CompensationMethods, tipAmount, regularHours, callback);
            });
        });
        getRunForDates(req.params.stdate, req.params.eddate, dbName, function (err, done) {
            RunForDates = done;
            indexParm++;
            sendPrsCompnRpt(indexParm, workerData, RunForDates, CompensationMethods, tipAmount, regularHours, callback);
        });
        getAllCompensationMethods(dbName, function (err, allcdone) {
            CompensationMethods = allcdone;
            indexParm++;
            sendPrsCompnRpt(indexParm, workerData, RunForDates, CompensationMethods, tipAmount, regularHours, callback);
        });
        getTipsByWorker(req.params.stdate, req.params.eddate, dbName, function (err, tipdone) {
            tipAmount = tipdone;
            indexParm++;
            sendPrsCompnRpt(indexParm, workerData, RunForDates, CompensationMethods, tipAmount, regularHours, callback);
        });
    },
    getProcessCompensationGenerateDetails: function (req, callback) {
        var dbName = req.headers['db'];
        startDate = req.params.stdate;
        endDate = req.params.eddate;
        var workerIds = req.headers.workerids;
        var workerid = '(' + workerIds + ')';
        var sqlQuery = `select ts.Worker__c workerId, a.New_Client__c newClient, 
            a.Booked_Online__c bookedOnline,
            a.Is_Standing_Appointment__c standingAppt, ts.Rebooked__c rebooked,
            sum( ts.Net_Price__c ) serviceAmount, count(ts.Id) numberOfServices, 
            sum( ts.Guest_Charge__c ) guestCharge
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c
            where a.isTicket__c = 1
            and ts.Is_Booked_Out__c = 0 and ts.Is_Class__c = 0
            and DATE(ts.Service_Date_Time__c) >= '`+ startDate + `'
            and DATE(ts.Service_Date_Time__c) <= '`+ endDate + `'
            and ts.Do_Not_Deduct_From_Worker__c = 0 AND ts.IsDeleted=0
            group by ts.Worker__c, a.New_Client__c, a.Booked_Online__c,
            a.Is_Standing_Appointment__c, ts.Rebooked__c;
        select ts.Worker__c workerId, s.Service_Group__c serviceGroupName,
            sum( ts.Net_Price__c ) serviceAmount, count(ts.Id) numberOfServices, sum( ts.Guest_Charge__c ) guestCharge
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c
            LEFT JOIN Service__c as s on s.Id = ts.Service__c
            where a.isTicket__c = 1
            and ts.Is_Booked_Out__c = 0 and ts.Is_Class__c = 0
            and DATE(ts.Service_Date_Time__c) >= '` + startDate + `'
            and DATE(ts.Service_Date_Time__c) <= '` + endDate + `'
            and ts.Do_Not_Deduct_From_Worker__c = 0 AND ts.IsDeleted=0
            group by ts.Worker__c, s.Service_Group__c; 
        select tp.Worker__c workerId, pl.Name productLineName,
            sum( tp.Qty_Sold__c * tp.Net_Price__c ) productAmount, sum( tp.Qty_Sold__c ) numberOfProducts
            from Ticket_Product__c tp
            LEFT JOIN Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c
            LEFT JOIN Product__c p on p.Id = tp.Product__c
            LEFT JOIN Product_Line__c pl on pl.Id = p.Product_Line__c
            where a.isTicket__c = 1
            and DATE( a.Appt_Date_Time__c ) >= '` + startDate + `'
            and DATE( a.Appt_Date_Time__c ) <=  '` + endDate + `'
            and tp.Do_Not_Deduct_From_Worker__c = 0 AND tp.IsDeleted=0
            group by tp.Worker__c, pl.Name;
        select tp.Worker__c workerId, p.Inventory_Group__c inventoryGroupName,
            sum( tp.Qty_Sold__c * tp.Net_Price__c ) productAmount, sum( tp.Qty_Sold__c ) numberOfProducts
            from Ticket_Product__c tp
            LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
            LEFT JOIN Product__c p on p.Id = tp.Product__c
            where a.isTicket__c = 1
            and DATE( a.Appt_Date_Time__c) >= '` + startDate + `'
            and DATE( a.Appt_Date_Time__c) <= '` + endDate + `'
            and tp.Do_Not_Deduct_From_Worker__c = 0 AND tp.IsDeleted=0
            group by tp.Worker__c, p.Inventory_Group__c;
        select ts.Worker__c workerId, Count( ts.Appt_Ticket__c ) ticketCount
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
            where a.isTicket__c = 1 and a.isRefund__c = 0
            and a.Is_Booked_Out__c = 0 and ts.Is_Class__c = 0
            and DATE( a.Appt_Date_Time__c) >= '` + startDate + `'
            and DATE( a.Appt_Date_Time__c) <= '` + endDate + `'
            AND ts.IsDeleted=0
            group by ts.Worker__c;
        select count( ts.id ) rebookedServiceCount, 
            sum( ts.Net_Price__c ) rebookedServiceAmount,
            ts.Worker__c workerId,
            sum( ts.Guest_Charge__c ) rebookedGuestCharge
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
            where DATE( ts.CreatedDate ) >= '` + startDate + `'
            and DATE( ts.CreatedDate ) <= '` + endDate + `'
            AND ts.IsDeleted=0
            and (ts.Rebooked__c = 1 or a.Business_Rebook__c = 1)`;
        var srvcqueries = `select Service__c serviceId, ts.Worker__c, DATE(ts.Service_Date_Time__c) serviceDate,
                    sum( ts.Net_Price__c ) serviceTotal, count(ts.Id) numberOfServices,
                    sum( ts.Guest_Charge__c ) guestCharge
                    from Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c
                    where ts.Worker__c IN `+ workerid + ` and at.isTicket__c = 1
                    and ts.Is_Booked_Out__c = 0 and ts.Is_Class__c = 0
                    and DATE(ts.Service_Date_Time__c) >= '`+ startDate + `'
                    and DATE(ts.Service_Date_Time__c) <= '` + endDate + `'                  
                    and ts.Do_Not_Deduct_From_Worker__c = 0
                    AND ts.IsDeleted=0
                    group by ts.Service__c ,  ts.Worker__c`;

        var prdtqueries = `select tp.Worker__c, DATE(a.Appt_Date_Time__c) productDate,
                sum( tp.Qty_Sold__c  *  tp.Net_Price__c ) productTotal,
                sum( tp.Qty_Sold__c ) numberOfProducts
                from Ticket_Product__c tp
                LEFT JOIN Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c
                where tp.Worker__c IN `+ workerid + ` and a.isTicket__c = 1
                and DATE(a.Appt_Date_Time__c) >= '`+ startDate + `'
                and DATE(a.Appt_Date_Time__c) <= '` + endDate + `'
                and tp.Do_Not_Deduct_From_Worker__c = 0
                AND tp.IsDeleted=0
                group by DATE(a.Appt_Date_Time__c)`;
        var clckqueries = `select Worker__c, DATE(Time_In__c) workDate,
                sum( IF(Time_Out__c = null, null, 
                time_to_sec(timediff(Time_Out__c, Time_In__c )) / 3600) ) hoursWorked
                from Time_Clock_Entry__c
                where Worker__c IN `+ workerid + ` 
                and Time_In__c IS NOT null AND Time_Out__c IS NOT null
                and DATE(Time_In__c) >= '`+ startDate + `'
                and DATE(Time_In__c) <= '` + endDate + `'
                AND IsDeleted=0
                group by DATE(Time_In__c);
                select Service__c wserviceId,Worker__c, IFNULL(Price__c,0) Price__c, IFNULL(Service_Fee_Amount__c, 0) Service_Fee_Amount__c, 
                IFNULL(Service_Fee_Percent__c,0) Service_Fee_Percent__c
                from Worker_Service__c where Worker__c IN `+ workerid + ` `;
        var cmpsteps = `SELECT u.Id, c.Steps__c FROM Compensation__c c
                LEFT JOIN User__c as u on u.Compensation__c = c.Id
                WHERE u.Id IN `+ workerid + ``;
        execute.query(dbName, sqlQuery + ';' + srvcqueries
            + ';' + prdtqueries + ';' + clckqueries + ';' + cmpsteps, '', function (err, result) {
                if (err) {
                    logger.error('Error in Reports dao - getProcessCompensationGenerateDetails:', err);
                    callback(err, { statusCode: '9999' });
                } else {
                    callback(err, {
                        'serviceSalesByClientFlags': result[0],
                        'serviceSalesByServiceGroup': result[1],
                        'productSalesByProductLine': result[2],
                        'productSalesByInventoryGroup': result[3],
                        'ticketCounts': result[4],
                        'RebookedServicesByDateRange': result[5],
                        'srvcData': result[6],
                        'prctData': result[7],
                        'clientData': result[8],
                        'Fee_Amount__c': result[9],
                        'steps': result[10]
                    });
                }
            });
    },
    getGiftsListReport: function (req, callback) {
        var dbName = req.headers['db'];
        var giftIds = '';
        var sqlGoalQuery = `SELECT tot.Id, tot.Amount__c, DATE_FORMAt(tot.Expires__c, "%m/%d/%Y") Expires__c,
            tot.Gift_Number__c, DATE_FORMAt(tot.Issued__c, "%m/%d/%Y") Issued__c, tot.Recipient__c, 
            tot.Ticket__c, ap.Client__c 
            from Ticket_Other__c tot  
            LEFT JOIN Appt_Ticket__c as ap on ap.Id =tot.Ticket__c 
            where tot.Transaction_Type__c ="Gift" AND tot.IsDeleted = 0`
        if (req.body['type'] === 'No Issued Date') {
            sqlGoalQuery += ` AND tot.Issued__c IS NULL AND DATE(ap.Appt_Date_Time__c) >='` + req.body['begindate'] + `'
                AND DATE(ap.Appt_Date_Time__c) <='`+ req.body['enddate'] + `'`
        } else if (req.body['type'] === 'No Expires Date') {
            sqlGoalQuery += ` AND tot.Expires__c IS NULL AND DATE(ap.Appt_Date_Time__c) >='` + req.body['begindate'] + `'
                AND DATE(ap.Appt_Date_Time__c) <='`+ req.body['enddate'] + `'`
        } else if (req.body['type'] === 'Issued') {
            sqlGoalQuery += ` AND DATE(tot.Issued__c) >='` + req.body['begindate'] + `'
                AND DATE(tot.Issued__c) <='`+ req.body['enddate'] + `'`
        } else if (req.body['type'] === 'Expired') {
            sqlGoalQuery += ` AND DATE(tot.Expires__c) >='` + req.body['begindate'] + `'
                AND DATE(tot.Expires__c) <='`+ req.body['enddate'] + `'`
        }
        sqlGoalQuery += ` order by tot.Gift_Number__c asc LIMIT 500`;
        execute.query(dbName, sqlGoalQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getGiftsListReport:', err);
                callback(err, { statusCode: '9999' });
            } else if (result.length > 0) {
                for (var i = 0; i < result.length; i++) {
                    var temp = result[i]['Gift_Number__c'].replace("'", "\\'");
                    giftIds += '\'' + temp + '\',';
                }
                if (giftIds.length > 0) {
                    giftIds = giftIds.slice(0, -1);
                    giftIds = '(' + giftIds + ')';
                }
                var sql = 'select tp.Gift_Number__c, DATE_FORMAt(tp.LastModifiedDate, "%m/%d/%Y") LastModifiedDate, p.Name paymentType, sum(tp.Amount_Paid__c) giftRedeemedAmount, tp.Gift_Number__c giftNumber from Ticket_Payment__c '
                    + ' tp LEFT JOIN Payment_Types__c as p on p.Id = tp.Payment_Type__c where tp.Gift_Number__c in ' + giftIds + ' and tp.isDeleted = 0 group by p.Name, tp.Gift_Number__c';
                execute.query(dbName, sql, '', function (err, result1) {
                    if (err) {
                        callback(err, { statusCode: '9999' });
                    } else if (result1.length > 0) {
                        for (var i = 0; i < result.length; i++) {
                            for (var j = 0; j < result1.length; j++) {
                                if (result1[j]['Gift_Number__c'].toUpperCase() === result[i]['Gift_Number__c'].toUpperCase()) {
                                    result[i].currentBalnce = result1[j].giftRedeemedAmount;
                                    result[i].LastModifiedDate = result1[j].LastModifiedDate;
                                }
                            }
                        }
                        callback(err, result);
                    } else {
                        callback(err, result);
                    }
                });

            } else {
                callback(err, result);
            }
        });
    },
    getWorkerTipsReport: function (req, callback) {
        var dbName = req.headers['db'];
        var startDate = req.body.begindate;
        var endDate = req.body.enddate;
        var worker = req.body.workerId;
        var sqlGoalQuery = `select a.Name,DATE_FORMAT(a.Appt_Date_Time__c, "%Y-%m-%d") as aptDt,ts.Appt_Ticket__c, ts.id,
            sum(ts.Net_Price__c) Service_Sales__c, ts.Worker__c Worker__c,
            CONCAT(c.FirstName, ' ' , c.LastName) clientName,
            CONCAT(u.FirstName,' ', u.LastName) userName
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c
            LEFT JOIN Contact__c c on c.Id = a.Client__c
            LEFT JOIN User__c u on u.Id=ts.Worker__c
            WHERE ts.IsDeleted=0
            AND a.Status__c IN ('Complete', 'Checked In')
            AND a.isRefund__c = 0
            AND DATE(a.Appt_Date_Time__c) >= '`+ startDate + `'
            and DATE(a.Appt_Date_Time__c) <= '`+ endDate + `'`
        if (worker) {
            sqlGoalQuery += ` AND ts.Worker__c='` + worker + `'`
        }
        sqlGoalQuery += ` group by ts.Id,a.id order by a.Name DESC;
            select a.Name,DATE_FORMAT(a.Appt_Date_Time__c, "%Y-%m-%d") as aptDt,
            tip.Appt_Ticket__c,tip.id,sum(tip.Tip_Amount__c) Tip_Amount__c,
            tip.Tip_Option__c, tip.Worker__c Worker__c,
            CONCAT(u.FirstName,' ', u.LastName) userName,
            CONCAT(c.FirstName, ' ' , c.LastName) clientName,
            IF(tip.Tip_Option__c = 'Tip Left in Drawer',tip.Tip_Amount__c, 0) Tip_Left_in_Drawer__c,
            IF(tip.Tip_Option__c = 'Tip Paid Out',tip.Tip_Amount__c, 0) Tip_Paid_Out__c
            from Ticket_Tip__c tip
            LEFT JOIN Appt_Ticket__c a on a.Id=tip.Appt_Ticket__c            
            LEFT JOIN Contact__c c on c.Id = a.Client__c
            LEFT JOIN User__c u on u.Id=tip.Worker__c
            WHERE tip.IsDeleted=0
            AND a.Status__c IN ('Complete', 'Checked In')
            AND a.isRefund__c = 0
            AND DATE(a.Appt_Date_Time__c) >= '`+ startDate + `'
            and DATE(a.Appt_Date_Time__c) <= '`+ endDate + `' `
        if (worker) {
            sqlGoalQuery += `AND tip.Worker__c='` + worker + `'`
        }
        sqlGoalQuery += ` group by tip.Id,a.id order by a.Name DESC`;
        execute.query(dbName, sqlGoalQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getWorkerTipsReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                callback(err, { data: result, dates: getDates(startDate, endDate) });
            }
        });
    },
    getVisitTypesReport: function (req, callback) {
        var dbName = req.headers['db'];
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        var recurringClientsQuery = `select CONCAT(u.FirstName,' ', u.LastName) workerName,
            ts.Worker__c workerId, COUNT(DISTINCT ts.Appt_Ticket__c) recurringClients
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c
            LEFT JOIN User__c u on u.Id=ts.Worker__c
            where DATE(a.Appt_Date_Time__c) >= '`+ startDate + `'
            and DATE(a.Appt_Date_Time__c) <= '`+ endDate + `'
            and a.IsTicket__c = 1 and a.Status__c = 'Complete'
            and a.New_Client__c = 0 and a.isRefund__c = 0
            group by ts.Worker__c`;

        var newClients = `select CONCAT(u.FirstName,' ', u.LastName) workerName,
            ts.Worker__c workerId, COUNT(DISTINCT ts.Appt_Ticket__c ) newClients
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c
            LEFT JOIN User__c u on u.Id=ts.Worker__c
              where DATE(a.Appt_Date_Time__c) >= '`+ startDate + `'
              and DATE(a.Appt_Date_Time__c)  <= '`+ endDate + `'
              and a.IsTicket__c = 1 and a.Status__c = 'Complete'
              and a.New_Client__c = 1
            group by ts.Worker__c`;
        var rebookedClients = `select CONCAT(u.FirstName,' ', u.LastName) workerName,
                ts.Worker__c workerId, COUNT(DISTINCT ts.Appt_Ticket__c ) rebookedClients
                from Ticket_Service__c ts
                LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c
                LEFT JOIN User__c u on u.Id=ts.Worker__c
				  where DATE(a.CreatedDate) >= '`+ startDate + `'
				  and DATE(a.CreatedDate) <='`+ endDate + `'
				  and ts.Rebooked__c = 1
                group by ts.Worker__c`
        var onlineClients = `select CONCAT(u.FirstName,' ', u.LastName) workerName,
                ts.Worker__c workerId, COUNT(DISTINCT ts.Appt_Ticket__c ) onlineClients
                from Ticket_Service__c ts
                LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c
                LEFT JOIN User__c u on u.Id=ts.Worker__c
				  where DATE(a.Appt_Date_Time__c) >= '`+ startDate + `'
				  and DATE(a.Appt_Date_Time__c) <= '`+ endDate + `'
				  and a.IsTicket__c = 1 and a.Status__c = 'Complete'
				  and a.Booked_Online__c = 1
                group by ts.Worker__c`
        var nonserviceClients = `select CONCAT(u.FirstName,' ', u.LastName) workerName,
                tp.Worker__c workerId, COUNT(DISTINCT tp.Appt_Ticket__c ) nonServiceClients
                from Ticket_Product__c tp
                LEFT JOIN Appt_Ticket__c a on a.Id=tp.Appt_Ticket__c
                LEFT JOIN User__c u on u.Id=tp.Worker__c
				  where DATE(a.Appt_Date_Time__c)  >= '`+ startDate + `'
				  and DATE(a.Appt_Date_Time__c) <= '`+ endDate + `'
				  and a.IsTicket__c = 1 and a.Status__c = 'Complete'
				  and a.isNoService__c = 1 and a.isRefund__c = 0
                group by tp.Worker__c`
        var serviceSales = `select CONCAT(u.FirstName,' ', u.LastName) workerName,
                ts.Worker__c workerId, sum( ts.Net_Price__c ) serviceSales
                from Ticket_Service__c ts
                LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c
                LEFT JOIN User__c u on u.Id=ts.Worker__c
				  where DATE(a.Appt_Date_Time__c) >= '`+ startDate + `'
				  and DATE(a.Appt_Date_Time__c) <= '`+ endDate + `' 
				  and a.IsTicket__c = 1 and a.Status__c = 'Complete'
                group by ts.Worker__c`
        var productSales = `select CONCAT(u.FirstName,' ', u.LastName) workerName,
                tp.Worker__c workerId, sum( tp.Qty_Sold__c  *  tp.Net_Price__c ) productSales
                from Ticket_Product__c tp
                LEFT JOIN Appt_Ticket__c a on a.Id=tp.Appt_Ticket__c
                LEFT JOIN User__c u on u.Id=tp.Worker__c
				  where DATE(a.Appt_Date_Time__c) >= '`+ startDate + `'
				  and DATE(a.Appt_Date_Time__c) <= '`+ endDate + `'
				  and a.IsTicket__c = 1 and a.Status__c = 'Complete'
                group by tp.Worker__c`
        var visitTypeData = `select CONCAT(u.FirstName,' ', u.LastName) workerName,
                ts.Worker__c workerId, a.Client_Type__c visitType, COUNT(DISTINCT ts.Appt_Ticket__c ) visitTypeClients
                from Ticket_Service__c ts
                LEFT JOIN Appt_Ticket__c a on a.Id=ts.Appt_Ticket__c 
                LEFT JOIN User__c u on u.Id=ts.Worker__c
				  where DATE(a.Appt_Date_Time__c) >= '`+ startDate + `'
				  and DATE(a.Appt_Date_Time__c) <= '`+ endDate + `' 
				  and a.IsTicket__c = 1 and a.Status__c = 'Complete'
				  and a.Client_Type__c IS NOT null
                group by ts.Worker__c, a.Client_Type__c`
        execute.query(dbName, recurringClientsQuery + ';' + newClients + ';' +
            rebookedClients + ';' + onlineClients
            + ';' + nonserviceClients + ';' + serviceSales
            + ';' + productSales + ';' + visitTypeData, '', function (err, result) {
                if (err) {
                    logger.error('Error in Reports dao - getVisitTypesReport:', err);
                    callback(err, { statusCode: '9999' });
                } else {
                    callback(err, {
                        'recurringClients': result[0], 'newClients': result[1], 'rebookedClients': result[2], 'onlineClients': result[3],
                        'nonserviceClients': result[4], 'serviceSales': result[5], 'productSales': result[6], 'visitTypeData': result[7]
                    });
                }
            });
    },
    getWorkerGoalsReport: function (req, callback) {
        var dbName = req.headers['db'];
        var sqlGoalQuery = `SELECT wg.Id as workerGoalId, wg.End_Date__c as endDate,
            wg.Start_Date__c as startDate ,wg.Goal_Target__c as target, 
            g.* FROM Worker_Goal__c as wg INNER JOIN Goal__c as g ON
            wg.Goal__c = g.Id And g.isDeleted = 0 And wg.isDeleted = 0
            And wg.Worker__c= '` + req.body.workerId + `'
            AND EXTRACT(YEAR_MONTH FROM wg.Start_Date__c) >= '` + req.body.startDate + `'
            AND EXTRACT(YEAR_MONTH FROM wg.End_Date__c) <= '` + req.body.endDate + `'`
        if (req.body.Goal__c) {
            sqlGoalQuery += ` AND wg.Goal__c = '` + req.body.Goal__c + `'`
        }
        sqlGoalQuery += ` order by wg.Start_Date__c desc`;
        execute.query(dbName, sqlGoalQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getWorkerGoalsReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                callback(err, result);
            }
        });
    },
    getElectronicPaymentsReport: function (req, callback) {
        var dbName = req.headers['db'];
        var sqlGoalQuery = `SELECT a.Appt_Date_Time__c, tp.Reference_Number__c, a.Name, 
            CONCAT(c.FirstName,' ', c.LastName) clientName, tp.Amount_Paid__c, 
            tp.Approval_Code__c
            from Ticket_Payment__c tp
            LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
            LEFT JOIN Contact__c c on c.Id = a.Client__c and c.IsDeleted =0
            LEFT JOIN Payment_Types__c p on p.Id =tp.Payment_Type__c and tp.IsDeleted =0
            WHERE DATE(a.Appt_Date_Time__c) >='` + req.body.startDate + `'
            AND DATE(a.Appt_Date_Time__c) <='` + req.body.endDate + `'
            AND (p.Process_Electronically_Online__c=1 OR p.Process_Electronically__c=1)`
        if (req.body.worker) {
            sqlGoalQuery += ` AND tp.Merchant_Account_Name__c = '` + req.body.worker + `'`
        }
        sqlGoalQuery += ` order by a.Appt_Date_Time__c desc`;
        execute.query(dbName, sqlGoalQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getElectronicPaymentsReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                callback(err, result);
            }
        });
    },
    getOnHandProductReport: function (req, callback) {
        var dbName = req.headers['db'];
        var sqlQuery = `SELECT Name,Size__c,Quantity_On_Hand__c,
            Minimum_Quantity__c,Average_Cost__c,Inventory_Group__c,
            (Quantity_On_Hand__c*Average_Cost__c) On_Hand_Cost__c
            FROM Product__c 
            WHERE Product_Line__c='`+ req.body.pdLine + `' and IsDeleted=0`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getOnHandProductReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                callback(err, result);
            }
        });
    },
    getProductChartReport: function (req, callback) {
        var dbName = req.headers['db'];
        var date = new Date();
        var lastMonth = date.getFullYear() + '' + ('0' + (date.getMonth())).slice(-2);
        var sqlQuery = `SELECT pl.Color__c, p.name, SUM((tp.Qty_Sold__c * tp.Net_Price__c)) price FROM 
            Ticket_Product__c tp
            LEFT JOIN Appt_Ticket__c a on a.Id =tp.Appt_Ticket__c
            LEFT JOIN Product__c p on p.Id =tp.Product__c
            LEFT JOIN Product_Line__c pl on pl.Id = p.Product_Line__c
            WHERE EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) >= '`+ lastMonth + `'
            AND 
            EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) <= '`+ lastMonth + `'
            AND 
            tp.IsDeleted=0
            GROUP by
            p.name order by price`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getOnHandProductReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                callback(err, result);
            }
        });
    },
    getTicketSalesChartReport: function (req, callback) {
        var dbName = req.headers['db'];
        var date = new Date();
        var lastMonth = date.getFullYear() + '' + ('0' + (date.getMonth())).slice(-2);
        var sqlQuery = `SELECT a.Id, DATE_FORMAT(a.Appt_Date_Time__c, "%Y-%m-%d") as aptDt,
            a.Client_Type__c, a.Status__c, concat(c.FirstName , " ", c.LastName) as ClientName,
            a.Name, (SUM(IFNULL(ts.Net_Price__c,0))) + 
            (SUM((IFNULL(tp.Qty_Sold__c,0)* IFNULL(tp.Net_Price__c,0)))) Payments__c
            FROM Appt_Ticket__c a 
            LEFT JOIN Ticket_Service__c ts on ts.Appt_Ticket__c = a.Id AND ts.IsDeleted =0
            LEFT JOIN Ticket_Product__c tp on tp.Appt_Ticket__c = a.Id AND tp.IsDeleted = 0 
            LEFT JOIN Contact__c c on c.Id = a.Client__c 
            WHERE EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c)  
            BETWEEN '`+ lastMonth + `' AND '` + lastMonth + `' 
            AND (a.Status__c = "Checked In" OR a.Status__c = "Complete")
            AND a.IsDeleted = 0 
            GROUP BY a.Id
            ORDER BY aptDt  DESC`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getOnHandProductReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                if (result && result.length > 0) {
                    var dataFilter = [];
                    for (var i = 0; i < result.length; i++) {
                        if (i === 0) {
                            dataFilter.push(result[i]);
                        } else {
                            const index = dataFilter.findIndex((data) => data.aptDt === result[i]['aptDt']);
                            if (index !== -1) {
                                dataFilter[index]['Payments__c'] = +dataFilter[index]['Payments__c'] + result[i]['Payments__c'];
                            } else {
                                dataFilter.push(result[i]);
                            }
                        }
                    }
                    result = dataFilter;
                }
                callback(err, result);
            }
        });
    },
    getTbpReport: function (req, callback) {
        var stDt = req.params.beginDate;
        var endDt = req.params.endDate;
        var sqlQuery = `
        SELECT
            ts.Appt_Ticket__c,
            a.Service_Sales__c,
            a.isRefund__c,
            a.New_Client__c,
            a.Rebooked_Rollup_Max__c,
            a.Business_Rebook__c,
            a.Booked_Online__c,
            a.isNoService__c,
            a.Appt_Date_Time__c,
            ts.Rebooked__c,
            ts.Is_Booked_Out__c,
            ts.Duration__c,
            ts.Guest_Charge__c
        FROM
            Ticket_Service__c ts
        LEFT JOIN Appt_Ticket__c a ON
            a.Id = ts.Appt_Ticket__c
        WHERE
            DATE(a.Appt_Date_Time__c) >= '`+ stDt + `' AND DATE(a.Appt_Date_Time__c) <= '` + endDt + `' AND a.Status__c = 'Complete';
        SELECT
            tp.Appt_Ticket__c,
            a.Product_Sales__c,
            a.isRefund__c,
            a.New_Client__c,
            a.Booked_Online__c,
            a.isNoService__c,
            a.Appt_Date_Time__c,
            tp.Qty_Sold__c,
            tp.Return_To_Inventory__c
        FROM
            Ticket_Product__c tp
        LEFT JOIN Appt_Ticket__c a ON
            a.Id = tp.Appt_Ticket__c
        WHERE
        DATE(a.Appt_Date_Time__c) >= '`+ stDt + `' AND DATE(a.Appt_Date_Time__c) <= '` + endDt + `' AND a.Status__c = 'Complete';
        SELECT
            ts.Appt_Ticket__c,
            ts.Service_Date_Time__c serviceDate,
            SUM(ts.Net_Price__c) serviceTotal,
            SUM(ts.Guest_Charge__c) guestCharge
        FROM
            Ticket_Service__c ts
        LEFT JOIN Appt_Ticket__c a ON
            a.Id = ts.Appt_Ticket__c
        WHERE
            a.isTicket__c = 1 AND a.Is_Class__c = 0 AND a.Status__c = 'Complete' AND DATE(ts.Service_Date_Time__c) >= '` + stDt + `' AND DATE(ts.Service_Date_Time__c) <= '` + endDt + `'
        GROUP BY
            ts.Appt_Ticket__c,
            ts.Service_Date_Time__c;
        SELECT
            tp.Id,
            tp.Appt_Ticket__c,
            a.Appt_Date_Time__c productDate,
            SUM(
                tp.Qty_Sold__c * tp.Net_Price__c
            ) productTotal,
            SUM(tp.Qty_Sold__c) numberOfProducts
        FROM
            Ticket_Product__c tp
        LEFT JOIN Appt_Ticket__c a ON
            a.Id = tp.Appt_Ticket__c
        WHERE
        DATE(a.Appt_Date_Time__c) >= '` + stDt + `' AND DATE(a.Appt_Date_Time__c) <= '` + endDt + `' AND a.Status__c = 'Complete'
        GROUP BY
            Id,
            a.Appt_Date_Time__c,
            tp.Appt_Ticket__c;
        SELECT
            Id,
            Appt_Date_Time__c apptDate,
            Ticket_Rating__c ticketRating
        FROM
            Appt_Ticket__c
        WHERE
        DATE(Appt_Date_Time__c) >= '`+ stDt + `' 
        AND DATE(Appt_Date_Time__c) <= '`+ endDt + `' AND Status__c = 'Complete'
        GROUP BY
            Id,
            Appt_Date_Time__c,
            Ticket_Rating__c;
        SELECT
            Id,
            Appt_Date_Time__c apptDate
        FROM
            Appt_Ticket__c
        WHERE
        DATE(Appt_Date_Time__c) >= '` + stDt + `' AND DATE(Appt_Date_Time__c) <= '` + endDt + `' AND Status__c = 'Complete' AND New_Client__c = 1
        GROUP BY
            Id,
            Appt_Date_Time__c;
        SELECT
            COUNT(ts.Service__c) serviceCount,
            s.Name serviceName,
            s.Id sId,
            DATE_FORMAT(Service_Date_Time__c, '%Y-%m-%d') serviceDate
        FROM
            Ticket_Service__c ts
        LEFT JOIN Service__c s ON
            s.Id = ts.Service__c
        LEFT JOIN Appt_Ticket__c a ON
            a.Id = ts.Appt_Ticket__c
        WHERE
            (
                a.isTicket__c = 1 OR a.Is_Class__c = 1
            ) AND a.Status__c != 'Canceled' AND a.Status__c != 'No Show' AND a.Is_Booked_Out__c != 1 AND DATE_FORMAT(ts.Service_Date_Time__c, '%Y-%m-%d') >= '` + stDt + `' AND DATE_FORMAT(ts.Service_Date_Time__c, '%Y-%m-%d') <= '` + endDt + `'
        GROUP BY
            Service_Date_Time__c,
            s.Name;
        SELECT
            Id,
            NAME,
            Active__c,
            ServiceName__c,
            Price__c,
            Taxable__c,
            Duration_1__c,
            Duration_2__c,
            Duration_3__c,
            (
                Duration_1__c + Duration_2__c + Duration_3__c + Buffer_After__c
            ) Total_Duration__c,
            Duration_1_Available_For_Other_Work__c,
            Duration_2_Available_For_Other_Work__c,
            Duration_3_Available_For_Other_Work__c,
            IFNULL(Buffer_After__c, 0) Buffer_After__c,
            Levels__c,
            Service_Group__c,
            Available_For_Client_To_Self_Book__c,
            Client_Facing_Name__c,
            Description__c,
            Resource_Filter__c,
            Deposit_Required__c,
            Deposit_Percent__c,
            Deposit_Amount__c,
            Guest_Charge__c,
            Max_Attendees__c,
            Price_per_Attendee__c,
            Is_Class__c
        FROM
            Service__c
        ORDER BY NAME ASC
        LIMIT 1000;
        SELECT
        Id,
        Appointment_Hours__c,
        StartDay
    FROM
        User__c
    WHERE
        id IN(
        SELECT
            Worker__c
        FROM
            Worker_Service__c
            )
    and
         StartDay <= '`+ endDt + `';
    SELECT u.Id, ch.Id as compHrsId,
         ch.SundayStartTime__c as SundayStartTime__c, 
         ch.SundayEndTime__c as SundayEndTime__c, 
         ch.MondayStartTime__c as MondayStartTime__c, 
         ch.MondayEndTime__c as MondayEndTime__c, 
         ch.TuesdayStartTime__c as TuesdayStartTime__c, 
         ch.TuesdayEndTime__c as TuesdayEndTime__c, 
         ch.WednesdayStartTime__c as WednesdayStartTime__c, 
         ch.WednesdayEndTime__c as WednesdayEndTime__c, 
         ch.ThursdayStartTime__c as ThursdayStartTime__c, 
         ch.ThursdayEndTime__c as ThursdayEndTime__c, 
         ch.FridayStartTime__c as FridayStartTime__c, 
         ch.FridayEndTime__c as FridayEndTime__c, 
         ch.SaturdayStartTime__c as SaturdayStartTime__c, 
         ch.SaturdayEndTime__c as SaturdayEndTime__c 
         FROM User__c as u
         LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c
         RIGHT JOIN Worker_Service__c as ws on ws.Worker__c = u.Id 
          WHERE u.IsActive=1
          GROUP BY u.Id;
    SELECT
        c.id,
        c.Name,
        c.Company_Hours__c,
        c.Date__c,
        c.StartTime__c,
        c.EndTime__c,
        c.All_Day_Off__c,
        c.UserId__c,
        u.FirstName,
        u.LastName,
        c.IsWorkerHours__c,
        'companyHours' Querytype
    FROM
        CustomHours__c c
    LEFT JOIN User__c u ON
        u.Appointment_Hours__c = c.Id
        WHERE c.IsWorkerHours__c = 0
        AND DATE(c.Date__c) BETWEEN  '`+ stDt + `' AND '` + endDt + `'
    ORDER BY
        Date__c ASC;
    SELECT
        a.Id,
        a.Rebooked_Rollup_Max__c,
        ts.Rebooked__c
    FROM
        Ticket_Service__c ts
        JOIN Appt_Ticket__c a WHERE
        DATE(a.CreatedDate) >= '`+ stDt + `' AND DATE(a.CreatedDate) <= '` + endDt + `' AND a.Business_Rebook__c = 1
    GROUP BY
        a.Id`;
        execute.query(req.headers['db'], sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getOnHandProductReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                var totalAppts = 0;
                var numOfNewClients = 0;
                var percntOfGoodAppts = 0;
                var numOfRebookAppts = 0;
                var serSales = 0;
                var prodSales = 0;
                var percntOfProd = 0;
                var numOfProdPerTckt = 0;
                var avgRevenuePerTckt = 0;
                var productRevenue = 0;
                var serviceRevenue = 0;
                var totalTicketCount = 0;
                // for avgRevenuePerTckt --start---
                if (result[0] && result[0].length > 0) {
                    totalTicketCount += result[0].length;
                    result[0].map((obj) => serviceRevenue += obj.Service_Sales__c);
                }
                if (result[1] && result[1].length > 0) {
                    totalTicketCount += result[1].length;
                    result[1].map((obj) => productRevenue += obj.Product_Sales__c);
                }
                if (totalTicketCount === 0) {
                    avgRevenuePerTckt = 0;
                } else {
                    avgRevenuePerTckt = (serviceRevenue + productRevenue) / totalTicketCount;
                }
                // for avgRevenuePerTckt --End---
                // for serviceSales --start---
                if (result[2] && result[2].length > 0) {
                    result[2].map((obj) => serSales += obj.serviceTotal);
                }
                // for serviceSales --End---
                // for productsales --start---
                if (result[3] && result[3].length > 0) {
                    result[3].map((obj) => prodSales += obj.productTotal);
                    // # of Products per Client Ticket
                    numOfProdPerTckt = result[3].length / (result[3].length + result[2].length)
                }
                if (serSales > 0) {
                    percntOfProd = (prodSales / (prodSales + serSales)) * 100;
                }
                // for productsales --End---
                // for goodticketper --start---
                if (result[4] && result[4].length > 0) {
                    percntOfGoodAppts = result[4].filter((obj) => obj.ticketRating === 'Good').length / result[4].length
                }
                if (result[5] && result[5].length > 0) {
                    numOfNewClients = result[5].length;
                }
                if (result[6] && result[6].length > 0) {
                    var dur = 0;
                    var totalProductivityVal = 0;
                    var temp = []
                    if (result[8] && result[8].length > 0) {
                        var stDay = getWeekStartDay(new Date(stDt).getDay());
                        var endDay = getWeekEndDay(new Date(endDt).getDay());
                        if (result[9] && result[9].length > 0) {
                            if (result[10] && result[10].length > 0) {
                                for (var i = 0; i < result[9].length; i++) {
                                    for (var j = 0; j < result[10].length; j++) {
                                        if (result[10][j].Company_Hours__c === result[9][i]['Id']) {
                                            temp.push(result[10][j]);
                                        }
                                    }
                                }
                            }
                        }
                        var openingTime = null;
                        var closingTime = null;
                        var customHour;
                            result[8].map((obj1, i) => {
                                if (stDt <= endDt) {
                                    customHour = temp.filter((obj) =>
                                        obj.Date__c === obj1['StartDay']
                                    );
                                }
                            })
                        for (var i = 0; i < result[9].length; i++) {
                            if (customHour && customHour.length > 0) {
                                openingTime = (customHour.All_Day_Off__c ? null : customHour[0].StartTime__c);
                                closingTime = (customHour.All_Day_Off__c ? null : customHour[0].EndTime__c);
                            } else {
                                var openingTime = result[9][i][stDay];
                                var closingTime = result[9][i][endDay];
                            }
                            if (openingTime && closingTime) {
                                const timeIn = timeConversionToDate(openingTime);
                                const timeOut = timeConversionToDate(closingTime);
                                dur += calculateDurations(timeIn, timeOut)
                            }
                        }
                        result[7].map((obj) => {
                            obj['Hours__c'] = 0;
                            obj['dur'] = 0;
                            if (obj.Duration_1_Available_For_Other_Work__c !== 1) { obj['dur'] += obj.Duration_1__c }
                            if (obj.Duration_2_Available_For_Other_Work__c !== 1) { obj['dur'] += obj.Duration_2__c }
                            if (obj.Duration_3_Available_For_Other_Work__c !== 1) { obj['dur'] += obj.Duration_3__c }
                            obj['dur'] += obj.Buffer_After__c
                            result[6].map((obj1) => {
                                if (obj1['serviceDate'] >= stDt && obj1['serviceDate'] <= endDt) {
                                    if (obj1['sId'] === obj['Id'])
                                        totalProductivityVal += obj1['serviceCount'] * obj['dur'];
                                }
                            });

                        });
                        // var totalProductivityVal = 0;
                        if (dur > 0) {
                            totalProductivityVal = (totalProductivityVal / (+dur * 60)) * 100;
                        }
                    }
                    // for numOfRebookAppts ---- start-----
                    if (result[11] && result[11].length > 0) {
                        numOfRebookAppts = (result[11].length / result[2].length) * 100;
                    }
                    // for numOfRebookAppts ---- End-----

                }
                // for goodticketper --End---
                var rtnArr = [{
                    totalProductivityVal: totalProductivityVal ? totalProductivityVal : 0,
                    numOfRebookAppts: numOfRebookAppts ? parseFloat(numOfRebookAppts) : 0,
                    numOfClients: numOfNewClients ? numOfNewClients : 0,
                    percntOfGoodAppts: percntOfGoodAppts ? parseFloat(percntOfGoodAppts) : 0,
                    serSales: serSales ? serSales : 0,
                    prodSales: prodSales ? prodSales : 0,
                    percntOfProd: percntOfProd ? parseFloat(percntOfProd) : 0,
                    avgRevenuePerTckt: avgRevenuePerTckt ? avgRevenuePerTckt : 0,
                    numOfProdPerTckt: numOfProdPerTckt ? numOfProdPerTckt : 0
                }];
                callback(err, rtnArr);
            }
        });
    },
    getInventoryAdjustmentReportList: function (req, callback) {
        var dbName = req.headers['db'];
        var sqlQuery = `select p.Inventory_Group__c, pl.name plName,DATE_FORMAT(br.CreatedDate,  "%m/%d/%Y") CreatedDate,
            p.Product_Code__c,p.Name prdName, 
            p.Size__c, br.Product__c, br.Type__c, br.ID__c, br.JSON__c 
            from Batch_Report__c br
            LEFT JOIN Product__c p on p.Id = br.Product__c and p.IsDeleted =0            
            LEFT JOIN Product_Line__c pl on pl.Id = p.Product_Line__c
            where br.Type__c = 'Inventory Adjustment Report'
            and Product__c is not null
            and br.IsDeleted = 0
            order by br.CreatedDate DESC`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getOnHandProductReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                callback(err, result);
            }
        });
    },
    getProductSalesByRankReport: function (req, callback) {
        var dbName = req.headers['db'];
        var mad = [];
        var sqlQuery = `SELECT p.Name, p.Unit_of_Measure__c,p.Size__c,p.Quantity_On_Hand__c,tp.Net_Price__c FROM 
            Ticket_Product__c tp
            LEFT JOIN Product__c p on p.Id =tp.Product__c and p.IsDeleted =0
            LEFT JOIN Appt_Ticket__c a on a.Id =tp.Appt_Ticket__c
            WHERE DATE(a.Appt_Date_Time__c) >='` + req.body.startDate + `'
            AND DATE(a.Appt_Date_Time__c) <='` + req.body.endDate + `'`
        if (req.body.worker) {
            sqlQuery += `AND tp.Worker__c ='` + req.body.worker + `' `
        }
        if (req.body.pdLine) {
            sqlQuery += ` and p.Product_Line__c = '` + req.body.pdLine + `' `
        }
        sqlQuery += ` GROUP BY p.Id ORDER by tp.Net_Price__c desc;`;
        if (req.body.unsold) {
            sqlQuery += `select id, Name, Size__c, Quantity_On_Hand__c, Unit_of_Measure__c from Product__c
                where`
            if (req.body.pdLine) {
                sqlQuery += ` Product_Line__c = '` + req.body.pdLine + `' and `
            }
            sqlQuery += ` id NOT IN
                (select Product__c from Ticket_Product__c tp, Appt_Ticket__c a
                where DATE(a.Appt_Date_Time__c) >= '` + req.body.startDate + `'
                and DATE(a.Appt_Date_Time__c) <= '` + req.body.endDate + `'
                and a.Status__c = 'Complete') and isDeleted = 0 ORDER by Price__c desc`
        }
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getOnHandProductReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                var retunObj = [[], []];
                if (!req.body.unsold) {
                    retunObj[0] = result;
                } else {
                    retunObj = result;
                }
                callback(err, retunObj);
            }
        });
    },
    getActivityComparisonReport: function (req, callback) {
        var searchDate = req.params.date;
        var sqlQuery = '';
        var workerId = req.params.workerid;
        var ticketList;
        var ticketServicesList;
        var ticketProductsList;
        var giftData;
        var miscData;
        var tcktRtngData;
        var clientData;
        var serviceTicketList;
        var rebookData;
        var ticketListQry;
        var ticketServicesQry;
        var ticketProductsQry;
        var giftSqlQry;
        var finalQry;
        var miscSqlQry;
        var ticketRatngQry;
        var clientQry;
        var serviceTicketListQry;
        var rebookQry;
        moment.suppressDeprecationWarnings = true;
        var last30DaysStartDate = moment(searchDate).add(-30, 'days').format('YYYY-MM-DD');
        var last30DaysEndDate = searchDate;
        var beginningOfPriorYear = moment(searchDate).add(-1, 'year').format('YYYY-MM-DD');
        var last90DaysStartDate = moment(searchDate).add(-90, 'days').format('YYYY-MM-DD');
        var last90DaysEndDate = searchDate;
        if (!workerId || workerId === null) {
            ticketListQry = `
        SELECT
            Id,
            Appt_Date_Time__c apptDate,
            COUNT(Client__c) clientCount
        FROM
            Appt_Ticket__c
        WHERE
            DATE(Appt_Date_Time__c) <= '`+ searchDate + `' AND Status__c = 'Complete'
        GROUP BY
            Id,
            Appt_Date_Time__c;`
            ticketServicesQry = `
            SELECT
                ts.Id,
                ts.Service_Date_Time__c serviceDate,
                ts.Appt_Ticket__c,
                SUM(ts.Net_Price__c) serviceTotal,
                SUM(ts.Guest_Charge__c) guestCharge
            FROM
                Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a ON
                a.Id = ts.Appt_Ticket__c
            WHERE
                a.isTicket__c = 1 AND a.Status__c = 'Complete' AND DATE(ts.Service_Date_Time__c) <= '`+ searchDate + `'
            GROUP BY
                ts.Id,
                ts.Service_Date_Time__c,
                ts.Appt_Ticket__c;`
            ticketProductsQry = `SELECT
            tp.Id,
            tp.Appt_Ticket__c,
            a.Appt_Date_Time__c productDate,
            SUM(
                tp.Qty_Sold__c * tp.Net_Price__c
            ) productTotal,
            SUM(Qty_Sold__c) numberOfProducts
        FROM
            Ticket_Product__c tp
        LEFT JOIN Appt_Ticket__c a ON
            a.Id = tp.Appt_Ticket__c
        WHERE
        DATE(a.Appt_Date_Time__c) <= '`+ searchDate + `' AND a.Status__c = 'Complete'
        GROUP BY
            tp.Id,
            a.Appt_Date_Time__c,
            tp.Appt_Ticket__c;`
            giftSqlQry = `
        SELECT
            tot.Id,
            tot.Ticket__c,
            a.Appt_Date_Time__c giftDate,
            tot.Transaction_Type__c,
            SUM(tot.Amount__c) giftSales
        FROM
            Ticket_Other__c tot
        LEFT JOIN Appt_Ticket__c a ON
            a.Id = tot.Ticket__c
        WHERE
        DATE(a.Appt_Date_Time__c) <= '`+ searchDate + `' AND tot.Transaction_Type__c = 'Gift' AND a.Status__c = 'Complete'
        GROUP BY
            tot.Id,
            a.Appt_Date_Time__c,
            tot.Ticket__c,
            tot.Transaction_Type__c;`
            miscSqlQry = `
        SELECT
            tot.Id,
            tot.Ticket__c,
            a.Appt_Date_Time__c ticketDate,
            tot.Transaction_Type__c,
            SUM(tot.Amount__c) otherSales
        FROM
            Ticket_Other__c tot
        LEFT JOIN Appt_Ticket__c a ON
            a.Id = tot.Ticket__c
        WHERE
        DATE(a.Appt_Date_Time__c) <= '`+ searchDate + `' AND Transaction_Type__c = 'Misc Sale' AND a.Status__c = 'Complete'
        GROUP BY
            tot.Id,
            a.Appt_Date_Time__c,
            tot.Ticket__c,
            tot.Transaction_Type__c;`
            ticketRatngQry = `
        SELECT
            Id,
            Appt_Date_Time__c apptDate,
            Ticket_Rating__c ticketRating
        FROM
            Appt_Ticket__c
        WHERE
        DATE(Appt_Date_Time__c) <= '`+ searchDate + `' AND Status__c = 'Complete'
        GROUP BY
            Id,
            Appt_Date_Time__c,
            Ticket_Rating__c;`
            clientQry = `
        SELECT
            Id,
            Appt_Date_Time__c apptDate
        FROM
            Appt_Ticket__c
        WHERE
        DATE(Appt_Date_Time__c) <= '`+ searchDate + `' AND Status__c = 'Complete' AND New_Client__c = 1
        GROUP BY
            Id,
            Appt_Date_Time__c;`
            serviceTicketListQry = `
        SELECT
            ts.Appt_Ticket__c,
            ts.Service_Date_Time__c serviceDate,
            SUM(ts.Net_Price__c) serviceTotal,
            SUM(ts.Guest_Charge__c) guestCharge
        FROM
            Ticket_Service__c ts
        LEFT JOIN Appt_Ticket__c a ON
            a.Id = ts.Appt_Ticket__c
        WHERE
            a.isTicket__c = 1 AND a.Is_Class__c = 0 AND a.Status__c = 'Complete' AND DATE(ts.Service_Date_Time__c) BETWEEN '`+ beginningOfPriorYear + `' AND '` + searchDate + `'
        GROUP BY
            ts.Appt_Ticket__c,
            ts.Service_Date_Time__c;`
            rebookQry = `
        SELECT
            id,
            CreatedDate serviceDate
        FROM
            Appt_Ticket__c
        WHERE
        DATE(CreatedDate) <= '`+ searchDate + `' AND Business_Rebook__c = 1
        GROUP BY
            id,
            CreatedDate;`
            finalQry = ticketListQry + ticketServicesQry + ticketProductsQry + giftSqlQry + miscSqlQry + ticketRatngQry + clientQry + serviceTicketListQry + rebookQry;
        } else {
            ticketListQry = `
                    SELECT
                        ts.Id,
                        ts.Appt_Ticket__c ticket,
                        ts.Service_Date_Time__c serviceDate
                    FROM
                        Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a ON
                        a.Id = ts.Appt_Ticket__c
                    WHERE
                    DATE(ts.Service_Date_Time__c) <= '`+ searchDate + `' AND a.Status__c = 'Complete' AND ts.Worker__c = '` + workerId + `'
                    GROUP BY
                        ts.Id,
                        ts.Appt_Ticket__c,
                        ts.Service_Date_Time__c;`
            ticketServicesQry = `
                    SELECT
                        ts.Id,
                        ts.Service_Date_Time__c serviceDate,
                        ts.Appt_Ticket__c,
                        SUM(ts.Net_Price__c) serviceTotal,
                        SUM(ts.Guest_Charge__c) guestCharge
                    FROM
                        Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a ON
                        a.Id = ts.Appt_Ticket__c
                    WHERE
                        a.Status__c = 'Complete'  AND DATE(ts.Service_Date_Time__c) <= '`+ searchDate + `' AND ts.Worker__c = '` + workerId + `'
                    GROUP BY
                        ts.Service_Date_Time__c,
                        ts.Appt_Ticket__c;`
            ticketProductsQry = `
                    SELECT
                        tp.Id,
                        tp.Appt_Ticket__c,
                        a.Appt_Date_Time__c productDate,
                        SUM(tp.Qty_Sold__c * tp.Net_Price__c) productTotal,
                        SUM(tp.Qty_Sold__c) numberOfProducts
                    FROM
                        Ticket_Product__c tp
                    LEFT JOIN Appt_Ticket__c a ON
                        a.Id = tp.Appt_Ticket__c
                    WHERE
                    DATE(a.Appt_Date_Time__c) <= '`+ searchDate + `' AND a.Status__c = 'Complete' AND tp.Worker__c = '` + workerId + `'
                    GROUP BY
                        tp.Id,
                        a.Appt_Date_Time__c,
                        tp.Appt_Ticket__c;`
            ticketRatngQry = `
                    SELECT
                        ts.Id,
                        ts.Appt_Ticket__c ticket,
                        ts.Service_Date_Time__c serviceDate,
                        a.Ticket_Rating__c ticketRating
                    FROM
                        Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a ON
                        a.Id = ts.Appt_Ticket__c
                    WHERE
                    DATE(ts.Service_Date_Time__c) <= '`+ searchDate + `' AND a.Status__c = 'Complete' AND ts.Worker__c = '` + workerId + `'
                    GROUP BY
                        ts.Id,
                        ts.Appt_Ticket__c,
                        ts.Service_Date_Time__c,
                        a.Ticket_Rating__c;`
            clientQry = `
                    SELECT
                        ts.Id,
                        ts.Appt_Ticket__c ticket,
                        ts.Service_Date_Time__c serviceDate
                    FROM
                        Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a ON
                        a.Id = ts.Appt_Ticket__c
                    WHERE
                    DATE(ts.Service_Date_Time__c) <= '`+ searchDate + `' 
                        AND a.Status__c = 'Complete'
                        AND ts.Worker__c = '` + workerId + `'
                        AND a.New_Client__c = 1
                    GROUP BY
                        ts.Id,
                        ts.Appt_Ticket__c,
                        ts.Service_Date_Time__c;`
            serviceTicketListQry = `
                    SELECT
                        ts.Appt_Ticket__c,
                        ts.Service_Date_Time__c serviceDate,
                        SUM(ts.Net_Price__c) serviceTotal,
                        SUM(ts.Guest_Charge__c) guestCharge
                    FROM
                        Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a ON
                        a.Id = ts.Appt_Ticket__c
                    WHERE
                        a.isTicket__c = 1 AND a.Is_Class__c = 0 AND a.Status__c = 'Complete' AND DATE(ts.Service_Date_Time__c) BETWEEN '`+ beginningOfPriorYear + `' AND '` + searchDate + `' AND ts.Worker__c = '` + workerId + `'
                    GROUP BY
                        ts.Appt_Ticket__c,
                        ts.Service_Date_Time__c;`
            rebookQry = `
                    SELECT
                        ts.Appt_Ticket__c,
                        a.CreatedDate serviceDate
                    FROM
                        Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a ON
                        a.Id = ts.Appt_Ticket__c
                    WHERE
                    DATE(a.CreatedDate) <= '`+ searchDate + `'  AND ts.Rebooked__c = 1 AND ts.Worker__c = '` + workerId + `'
                    GROUP BY
                        ts.Appt_Ticket__c,
                        a.CreatedDate;`
            finalQry = ticketListQry + ticketServicesQry + ticketProductsQry + ticketRatngQry + clientQry + serviceTicketListQry + rebookQry;
        }
        getServicesByReportDate(finalQry, req.headers['db'], function (err, rtnData) {
            if (err) {
                logger.error('Error in Reports dao - getActivityComparisonReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                var aline = {};
                if (!workerId || workerId === null) {
                    ticketList = rtnData[0];
                    ticketServicesList = rtnData[1];
                    ticketProductsList = rtnData[2];
                    if (rtnData[3] && rtnData[3].length > 0) {
                        giftData = rtnData[3];
                    } else {
                        giftData = [];
                    }
                    if (rtnData[4] && rtnData[4].length > 0) {
                        miscData = rtnData[4];
                    } else {
                        miscData = [];
                    }
                    if (rtnData[5] && rtnData[5].length > 0) {
                        tcktRtngData = rtnData[5];
                    } else {
                        tcktRtngData = [];
                    }
                    if (rtnData[6] && rtnData[6].length > 0) {
                        clientData = rtnData[6];
                    } else {
                        clientData = [];
                    }
                    if (rtnData[7] && rtnData[7].length > 0) {
                        serviceTicketList = rtnData[7];
                    } else {
                        serviceTicketList = [];
                    }
                    if (rtnData[8] && rtnData[8].length > 0) {
                        rebookData = rtnData[8];
                    } else {
                        rebookData = [];
                    }
                } else {
                    ticketList = rtnData[0];
                    ticketServicesList = rtnData[1];
                    ticketProductsList = rtnData[2];
                    if (rtnData[3] && rtnData[3].length > 0) {
                        tcktRtngData = rtnData[3];
                    } else {
                        tcktRtngData = [];
                    }
                    if (rtnData[4] && rtnData[4].length > 0) {
                        clientData = rtnData[4];
                    } else {
                        clientData = [];
                    }
                    if (rtnData[5] && rtnData[5].length > 0) {
                        serviceTicketList = rtnData[5];
                    } else {
                        serviceTicketList = [];
                    }
                    if (rtnData[6] && rtnData[6].length > 0) {
                        rebookData = rtnData[6];
                    } else {
                        rebookData = [];
                    }
                }
                var numberActivityList = [];
                var ticketRatingList = [];
                var amountActivityList = [];
                var countArray = [];
                var productNumberArray = [];
                var salesArray = [];
                var serviceTicketCounts = 0;
                var percentageActivityList = [];
                if (ticketList && ticketList.length > 0) {
                    if (!workerId || workerId === null) {
                        var reportData = generateAcitvityReportValues(ticketList, searchDate, 'apptDate', null, null)
                        countArray = reportData.countArray;
                        productNumberArray = reportData.productNumberArray;
                        salesArray = reportData.salesArray;
                        aline = populateActivityLine(aline, true, false, countArray, productNumberArray, salesArray);
                        aline.category = 'Client';
                        numberActivityList.push(aline)
                    } else {
                        var weekToDate = moment(searchDate).startOf('week').format('YYYY-MM-DD');
                        var monthToDate = moment(searchDate).startOf('month').format('YYYY-MM-DD');
                        todayDate = moment(searchDate).format('YYYY-MM-DD');
                        var day = parseInt(todayDate.split('-')[2]);
                        var month = parseInt(todayDate.split('-')[1]);
                        var year = parseInt(todayDate.split('-')[0]);
                        var lastYear = parseInt(year) - 1;
                        var quarterToDate;
                        if (month === 1 || month === 2 || month === 3)
                            quarterToDate = year + '-' + 1 + '-' + 1;
                        else if (month === 4 || month === 5 || month === 6)
                            quarterToDate = year + '-' + 4 + '-' + 1;
                        else if (month === 7 || month === 8 || month === 9)
                            quarterToDate = year + '-' + 7 + '-' + 1;
                        else
                            quarterToDate = new Date(year + '-' + 10 + '-' + 1);
                        yearToDate = year + '-' + 1 + '-' + 1;
                        mtdLastYearStartDate = lastYear + '-' + month + '-' + 1;
                        mtdLastYearEndDate = lastYear + '-' + month + '-' + day;
                        qtdLastYearStartDate = lastYear + '-' + (new Date(quarterToDate).getMonth() + 1) + '-' + 1;
                        qtdLastYearEndDate = lastYear + '-' + month + '-' + day;
                        ytdLastYearStartDate = lastYear + '-' + 1 + '-' + 1;
                        ytdLastYearEndDate = lastYear + '-' + month + '-' + day;
                        prevCalendarYearStartDate = lastYear + '-' + 1 + '-' + 1;
                        prevCalendarYearEndDate = lastYear + '-' + 12 + '-' + 31;
                        // populateCompensationSection
                        prior12MonthsStartDate = moment(searchDate).add(-12, 'months').format('YYYY-MM-DD');
                        prior12MonthsEndDate = searchDate;
                        last30DaysStartDate = moment(searchDate).add(-30, 'days').format('YYYY-MM-DD');
                        last30DaysEndDate = searchDate;
                        last90DaysStartDate = moment(searchDate).add(-90, 'days').format('YYYY-MM-DD');
                        last90DaysEndDate = searchDate;
                        var prev12MonthsCountSet = [];
                        var prev12MonthsCount = 0;
                        var last30DaysCountSet = [];
                        var last30DaysCount = 0;
                        var last90DaysCountSet = [];
                        var last90DaysCount = 0;
                        var todayCountSet = [];
                        var wtdCountSet = [];
                        var wtdCount = 0;
                        var mtdCountSet = [];
                        var mtdCount = 0;
                        var todayCount = 0;
                        var mtdLastYearCount = 0;
                        var qtdCount = 0
                        var qtdLastYearCount = 0;
                        var ytdCount = 0;
                        var ytdCountSet = [];
                        var ytdLastYearCount = 0;
                        var prevCalendarYearCount = 0;
                        ticketList.map((obj) => {
                            var ticketId = obj['ticket'];
                            var reportDate = obj['serviceDate'].split(' ')[0];
                            //	only want to count one appointment ticket for each service
                            //	if an appointment contains many services, it counts as 1
                            if (reportDate === todayDate) {
                                if (!(todayCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    todayCountSet.push(ticketId);
                                    todayCount++;
                                }
                            }
                            if (reportDate >= weekToDate) {
                                if (!(wtdCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    wtdCountSet.push(ticketId);
                                    wtdCount++;
                                }
                            }
                            if (reportDate >= monthToDate) {
                                if (!(mtdCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    mtdCountSet.push(ticketId);
                                    mtdCount++;
                                }
                            }
                            if (reportDate >= quarterToDate) {
                                if (!(qtdCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    qtdCountSet.push(ticketId);
                                    qtdCount++;
                                }
                            }
                            if (reportDate >= yearToDate) {
                                if (!(ytdCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    ytdCountSet.push(ticketId);
                                    ytdCount++;
                                }
                            }
                            if (reportDate >= mtdLastYearStartDate && reportDate <= mtdLastYearEndDate) {
                                if (!(mtdLastYearCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    mtdLastYearCountSet.push(ticketId);
                                    mtdLastYearCount++;
                                }
                            }
                            if (reportDate >= qtdLastYearStartDate && reportDate <= qtdLastYearEndDate) {
                                if (!(qtdLastYearCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    qtdLastYearCountSet.push(ticketId);
                                    qtdLastYearCount++;
                                }
                            }
                            if (reportDate >= ytdLastYearStartDate && reportDate <= ytdLastYearEndDate) {
                                if (!(ytdLastYearCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    ytdLastYearCountSet.push(ticketId);
                                    ytdLastYearCount++;
                                }
                            }
                            if (reportDate >= prevCalendarYearStartDate && reportDate <= prevCalendarYearEndDate) {
                                if (!(prevCalendarYearCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    prevCalendarYearCountSet.push(ticketId);
                                    prevCalendarYearCount++;
                                }
                            }
                            if (reportDate >= prior12MonthsStartDate && reportDate <= prior12MonthsEndDate) {
                                if (!(prev12MonthsCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    prev12MonthsCountSet.push(ticketId);
                                    prev12MonthsCount++;
                                }
                            }
                            if (reportDate >= last30DaysStartDate && reportDate <= last30DaysEndDate) {
                                if (!(last30DaysCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    last30DaysCountSet.push(ticketId);
                                    last30DaysCount++;
                                }
                            }
                            if (reportDate >= last90DaysStartDate && reportDate <= last90DaysEndDate) {
                                if (!(last90DaysCountSet.filter((obj) => obj === ticketId).length > 0)) {
                                    last90DaysCountSet.push(ticketId);
                                    last90DaysCount++;
                                }
                            }
                        });
                        var aline = {};
                        aline.todayDate = todayCount;
                        aline.weekToDate = wtdCount;
                        aline.monthToDate = mtdCount;
                        aline.monthToDateLastYear = mtdLastYearCount;
                        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
                        aline.quarterToDate = qtdCount;
                        aline.quarterToDateLastYear = qtdLastYearCount;
                        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
                        aline.yearToDate = ytdCount;
                        aline.yearToDateLastYear = ytdLastYearCount;
                        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
                        aline.priorCalendarYear = prevCalendarYearCount;
                        aline.priorTwelveMonths = prev12MonthsCount;
                        aline.last30Days = last30DaysCount;
                        aline.last90Days = last90DaysCount;
                        aline.category = 'Client';
                        numberActivityList.push(aline);
                    }
                }
                // Services
                if (ticketServicesList && ticketServicesList.length > 0) {
                    var reportData = generateAcitvityReportValues(ticketServicesList, searchDate, 'serviceDate', 'serviceTotal', null)
                    countArray = reportData.countArray;
                    productNumberArray = reportData.productNumberArray;
                    salesArray = reportData.salesArray;
                    aline = populateActivityLine({}, true, false, countArray, productNumberArray, salesArray);
                    aline.category = 'Services';
                    numberActivityList.push(aline);
                    aline = populateActivityLine({}, false, false, countArray, productNumberArray, salesArray);
                    aline.category = 'Services';
                    amountActivityList.push(aline);
                }
                // Products
                if (ticketProductsList && ticketProductsList.length > 0) {
                    var reportData = generateAcitvityReportValues(ticketProductsList, searchDate, 'productDate', 'productTotal', 'numberOfProducts')
                    countArray = reportData.countArray;
                    productNumberArray = reportData.productNumberArray;
                    salesArray = reportData.salesArray;
                    aline = populateActivityLine({}, true, false, countArray, productNumberArray, salesArray);
                    aline.category = 'Products';
                    numberActivityList.push(aline);
                    aline = populateActivityLine({}, false, false, countArray, productNumberArray, salesArray);
                    aline.category = 'Products';
                    numberActivityList.push(aline);
                } else {
                    var pro;
                    for (const obj in aline) {
                        pro = [];
                        aline[obj] = 0;
                        aline.category = 'Products';
                        if (pro.indexOf(aline.category) === -1) {
                            pro.push(aline);
                        }
                    }
                    numberActivityList.push(pro[0]);
                }
                // calculation for services, products and tickets ------Start
                var numClients = numberActivityList[0];
                var numProducts = numberActivityList[3];
                var aline = {};
                if (numClients && numProducts && numClients.length > 0 && numProducts.length > 0) {
                    // aline.category = System.Label.Label_Products_per_Client;
                    aline.todayDate = calculateAverage(numProducts.todayDate, numClients.todayDate);
                    aline.weekToDate = calculateAverage(numProducts.weekToDate, numClients.weekToDate);
                    aline.monthToDate = calculateAverage(numProducts.monthToDate, numClients.monthToDate);
                    aline.monthToDateLastYear = calculateAverage(numProducts.monthToDateLastYear, numClients.monthToDateLastYear);
                    aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
                    aline.quarterToDate = calculateAverage(numProducts.quarterToDate, numClients.quarterToDate);
                    aline.quarterToDateLastYear = calculateAverage(numProducts.quarterToDateLastYear, numClients.quarterToDateLastYear);
                    aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
                    aline.yearToDate = calculateAverage(numProducts.yearToDate, numClients.yearToDate);
                    aline.yearToDateLastYear = calculateAverage(numProducts.yearToDateLastYear, numClients.yearToDateLastYear);
                    aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
                    aline.priorCalendarYear = calculateAverage(numProducts.priorCalendarYear, numClients.priorCalendarYear);
                    aline.priorTwelveMonths = calculateAverage(numProducts.priorTwelveMonths, numClients.priorTwelveMonths);
                    aline.last30Days = calculateAverage(numProducts.last30Days, numClients.last30Days);
                    aline.last90Days = calculateAverage(numProducts.last90Days, numClients.last90Days);
                    aline.category = 'PerClient';
                    numberActivityList.push(aline)
                    var aline = {};
                    aline = populateActivityLine(aline, false, false, countArray, productNumberArray, salesArray);
                    aline.category = 'Products';
                    amountActivityList.push(aline);
                }
                // calculation for services, products and tickets ------End
                // Gifts
                if (giftData && giftData.length > 0) {
                    var reportData = generateAcitvityReportValues(giftData, searchDate, 'giftDate', 'giftSales', null)
                    countArray = reportData.countArray;
                    productNumberArray = reportData.productNumberArray;
                    salesArray = reportData.salesArray;
                    aline = populateActivityLine({}, true, false, countArray, productNumberArray, salesArray);
                    aline.category = 'Gift';
                    numberActivityList.push(aline);
                    aline = populateActivityLine({}, false, false, countArray, productNumberArray, salesArray);
                    aline.category = 'Gift';
                    amountActivityList.push(aline);
                }
                // Misc
                if (miscData && miscData.length > 0) {
                    var reportData = generateAcitvityReportValues(miscData, searchDate, 'ticketDate', 'otherSales', null)
                    countArray = reportData.countArray;
                    productNumberArray = reportData.productNumberArray;
                    salesArray = reportData.salesArray;
                    aline = populateActivityLine({}, true, false, countArray, productNumberArray, salesArray);
                    aline.category = 'Misc';
                    numberActivityList.push(aline);
                    aline = populateActivityLine({}, false, false, countArray, productNumberArray, salesArray);
                    aline.category = 'Misc';
                    amountActivityList.push(aline);
                }
                // amountActivityList 
                if (amountActivityList && amountActivityList.length > 0) {
                    amountActivityList = populateAmountActivityList(amountActivityList, numberActivityList)
                }
                // callback('err', ticketList);
                // ticket Rating
                if (tcktRtngData && tcktRtngData.length > 0) {
                    ticketRatingList = populateTicketRatingList(tcktRtngData, searchDate, workerId, numberActivityList)
                }
                // Clients
                if (clientData && clientData.length > 0) {
                    clientData = populateClientData(clientData, searchDate, workerId, numberActivityList)
                }
                if (serviceTicketList && serviceTicketList.length > 0) {
                    serviceTicketCounts = countArray;
                }
                if (rebookData && rebookData.length > 0) {
                    var reportData = generateAcitvityReportValues(rebookData, searchDate, 'serviceDate', null, null)
                    countArray = reportData.countArray;
                    productNumberArray = reportData.productNumberArray;
                    salesArray = reportData.salesArray;
                    var aline = {};
                    aline.category = 'Rebook_percent';
                    aline.todayDate = calculateAverage(countArray[0], serviceTicketCounts[0]) * 100;
                    aline.weekToDate = calculateAverage(countArray[1], serviceTicketCounts[1]) * 100;
                    aline.monthToDate = calculateAverage(countArray[2], serviceTicketCounts[2]) * 100;
                    aline.monthToDateLastYear = calculateAverage(countArray[3], serviceTicketCounts[3]) * 100;
                    aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
                    aline.quarterToDate = calculateAverage(countArray[4], serviceTicketCounts[4]) * 100;
                    aline.quarterToDateLastYear = calculateAverage(countArray[5], serviceTicketCounts[5]) * 100;
                    aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
                    aline.yearToDate = calculateAverage(countArray[6], serviceTicketCounts[6]) * 100;
                    aline.yearToDateLastYear = calculateAverage(countArray[7], serviceTicketCounts[7]) * 100;
                    aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
                    aline.priorCalendarYear = calculateAverage(countArray[8], serviceTicketCounts[8]) * 100;
                    aline.priorTwelveMonths = calculateAverage(countArray[9], serviceTicketCounts[9]) * 100;
                    aline.last30Days = 0;
                    aline.last90Days = 0;
                    clientData.push(aline);
                }
                // Compensation section
                // total sales %
                if (amountActivityList && amountActivityList.length > 0) {
                    populatePercentageSection(amountActivityList, searchDate, workerId, req.headers['db'], function (data) {
                        percentageActivityList = data;
                        populateCompensationSection(req.headers['db'], workerId, last30DaysStartDate, last30DaysEndDate, last90DaysStartDate, last90DaysEndDate,
                            countArray, productNumberArray, salesArray, amountActivityList, numberActivityList, searchDate, function (err, data1) {
                                callback(err, {
                                    numberActivityList: numberActivityList, amountActivityList: amountActivityList,
                                    tcktRtngData: ticketRatingList, clientData: clientData, percentageActivityList: percentageActivityList,
                                    populateCompensationSection: data1
                                })
                            })
                    });
                }
            }
        })
    },
    deleteInventoryAdjustmens: function (req, callback) {
        var dbName = req.headers['db'];
        var sqlQuery = `DELETE FROM Batch_Report__c
            WHERE DATE(CreatedDate) = '`+ req.params.date + `'`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getOnHandProductReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                callback(err, result);
            }
        });
    },
    getTicketAnalysisReport: function (req, callback) {
        var dbName = req.headers['db'];
        var sqlQuery = `select count(ts.Id) servcCount, ts.Appt_Ticket__c, SUM(ts.Net_Price__c) Service_Sales__c,
                a.isRefund__c, a.New_Client__c,
                a.Rebooked_Rollup_Max__c, a.Business_Rebook__c,
                a.Booked_Online__c, a.isNoService__c,
                a.Appt_Date_Time__c, ts.Rebooked__c, ts.Is_Booked_Out__c,
                ts.Duration__c, SUM(IFNULL(ts.Guest_Charge__c,0)) Guest_Charge__c
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
            where DATE(a.Appt_Date_Time__c) >= '` + req.body.startDate.split(' ')[0] + `'
            and DATE(a.Appt_Date_Time__c) <= '` + req.body.endDate.split(' ')[0] + `'`
        if (req.body.worker) {
            sqlQuery += ` and ts.Worker__c = '` + req.body.worker + `'`
        }
        sqlQuery += ` and a.Status__c = 'Complete' and ts.IsDeleted = 0 GROUP BY a.Id;
            select count(tp.Id) prdtCount, tp.Appt_Ticket__c,
                (tp.Qty_Sold__c * tp.Net_Price__c) Product_Sales__c,
                    a.isRefund__c, a.New_Client__c,
                    a.Booked_Online__c, a.isNoService__c,
                    a.Appt_Date_Time__c, tp.Qty_Sold__c, tp.Return_To_Inventory__c
            from Ticket_Product__c tp
            LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
            where DATE(a.Appt_Date_Time__c) >= '` + req.body.startDate.split(' ')[0] + `'
            and DATE(a.Appt_Date_Time__c) <= '` + req.body.endDate.split(' ')[0] + `' `
        if (req.body.worker) {
            sqlQuery += ` and tp.Worker__c = '` + req.body.worker + `'`
        }
        sqlQuery += ` and a.Status__c = 'Complete' and tp.IsDeleted = 0 GROUP BY a.Id;
            select SUM(IFNULL(ts.Net_Price__c,0)) Service_Sales__c, SUM(IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) Product_Sales__c
            from Appt_Ticket__c a
            left join Ticket_Service__c ts on ts.Appt_Ticket__c=a.Id and ts.IsDeleted= 0
            left join Ticket_Product__c tp on tp.Appt_Ticket__c=a.Id and tp.IsDeleted= 0
            where DATE(a.Appt_Date_Time__c) >= '` + req.body.startDate.split(' ')[0] + `'
            and DATE(a.Appt_Date_Time__c) <= '` + req.body.endDate.split(' ')[0] + `'
            and a.Status__c = 'Complete' GROUP BY a.Id`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getTicketAnalysisReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                callback(err, {
                    'serviceTxList': result[0],
                    'productTxList': result[1],
                    'zeroOrRefundCount': result[2].filter((obj) => obj.Service_Sales__c === 0 && obj.Product_Sales__c === 0).length
                });
            }
        });
    },
    getMonthlyBussinessAnalysisReport: function (req, callback) {
        var dbName = req.headers['db'];
        var sqlQuery = `select ts.Appt_Ticket__c, ts.Service_Date_Time__c serviceDate,
                sum(ts.Net_Price__c) serviceTotal, sum(IFNULL(ts.Guest_Charge__c, 0)) guestCharge
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
            where a.isTicket__c = 1
            and a.Is_Class__c = 0
            and a.Status__c = 'Complete'
            and EXTRACT(YEAR_MONTH FROM ts.Service_Date_Time__c) >= '` + req.body.lastyear + `'
            and EXTRACT(YEAR_MONTH FROM ts.Service_Date_Time__c) <= '` + req.body.startDate + `' `
        if (req.body.worker) {
            sqlQuery += ` and ts.Worker__c = '` + req.body.worker + `'`
        }
        sqlQuery += ` GROUP BY ts.Appt_Ticket__c, DATE(Service_Date_Time__c);
            select tp.Id, tp.Appt_Ticket__c, DATE(a.Appt_Date_Time__c) productDate,
                sum(tp.Qty_Sold__c * tp.Net_Price__c) productTotal, sum(tp.Qty_Sold__c) numberOfProducts
            from Ticket_Product__c tp
            LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
            where EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) >= '` + req.body.lastyear + `'
            and EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) <= '` + req.body.startDate + `' `

        if (req.body.worker) {
            sqlQuery += ` and tp.Worker__c = '` + req.body.worker + `'`
        }
        sqlQuery += ` GROUP BY tp.Id, DATE(a.Appt_Date_Time__c), tp.Appt_Ticket__c; `
        if (req.body.serviceGroup) {
            sqlQuery += ` select ts.Id, DATE(ts.Service_Date_Time__c) serviceDate,
                ts.Appt_Ticket__c, sum(ts.Net_Price__c) serviceTotal, sum(IFNULL(ts.Guest_Charge__c, 0)) guestCharge
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
            LEFT JOIN Service__c s on s.Id = ts.Service__c
            where a.isTicket__c = 1
            and a.Status__c = 'Complete'
            and s.Service_Group__c = '` + req.body.serviceGroup + `'
            and EXTRACT(YEAR_MONTH FROM ts.Service_Date_Time__c) >= '` + req.body.lastyear + `'
            and EXTRACT(YEAR_MONTH FROM ts.Service_Date_Time__c) <= '` + req.body.startDate + `' `
            if (req.body.worker) {
                sqlQuery += ` and ts.Worker__c = '` + req.body.worker + `'`
            }
            sqlQuery += ` GROUP BY ts.Id, DATE(ts.Service_Date_Time__c), ts.Appt_Ticket__c; `
        }
        sqlQuery += `select ts.Appt_Ticket__c, a.Service_Sales__c, a.Product_Sales__c,
                a.isRefund__c, a.New_Client__c,
                a.isRefund__c, a.Business_Rebook__c, a.Booked_Online__c, a.isNoService__c,
                a.Appt_Date_Time__c, ts.Rebooked__c, ts.Is_Booked_Out__c, ts.Duration__c, ts.Guest_Charge__c
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
            where EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) >= '` + req.body.lastyear + `'
            and EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) <= '` + req.body.startDate + `'
            and a.Status__c = 'Complete' `
        if (req.body.worker) {
            sqlQuery += ` and ts.Worker__c = '` + req.body.worker + `'`
        }
        sqlQuery += ` GROUP BY ts.Id, DATE(a.Appt_Date_Time__c);
            select tp.Appt_Ticket__c, a.Service_Sales__c, a.Product_Sales__c,
                a.isRefund__c, a.New_Client__c,
                a.Booked_Online__c, a.isNoService__c,
                a.Appt_Date_Time__c, tp.Qty_Sold__c, tp.Return_To_Inventory__c
            from Ticket_Product__c tp
            LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
            where EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) >= '` + req.body.lastyear + `'
            and EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) <= '` + req.body.startDate + `'
            and a.Status__c = 'Complete'`
        if (req.body.worker) {
            sqlQuery += ` and tp.Worker__c = '` + req.body.worker + `'`
        }
        sqlQuery += ` GROUP BY tp.Id, DATE(a.Appt_Date_Time__c); `
        if (req.body.worker) {
            sqlQuery += ` select ts.Appt_Ticket__c, DATE(a.CreatedDate) bookedDate
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
            where EXTRACT(YEAR_MONTH FROM a.CreatedDate) >= '` + req.body.lastyear + `'
            and EXTRACT(YEAR_MONTH FROM a.CreatedDate) <= '` + req.body.startDate + `'
            and ts.Rebooked__c = 1 and ts.Worker__c = '` + req.body.worker + `';`
        } else {
            sqlQuery += `select Id, CreatedDate bookedDate
            from Appt_Ticket__c
            where  EXTRACT(YEAR_MONTH FROM CreatedDate) >= '` + req.body.lastyear + `'
            and EXTRACT(YEAR_MONTH FROM CreatedDate) <= '` + req.body.startDate + `'
            and Business_Rebook__c = 1
            GROUP BY Id, CreatedDate;`
        }
        if (req.body.pdLine) {
            sqlQuery += ` select tp.Id, tp.Appt_Ticket__c, DATE(a.Appt_Date_Time__c) productDate,
                sum(tp.Qty_Sold__c * tp.Net_Price__c) productTotal, sum(tp.Qty_Sold__c) numberOfProducts
            from Ticket_Product__c tp
            LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c
            LEFT JOIN Product__c p on p.Id = tp.Product__c
            where EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) >= '` + req.body.lastyear + `'
            and EXTRACT(YEAR_MONTH FROM a.Appt_Date_Time__c) <= '` + req.body.startDate + `'
            and p.Product_Line__c = '` + req.body.pdLine + `'
            and a.Status__c = 'Complete'`
            if (req.body.worker) {
                sqlQuery += ` and tp.Worker__c = '` + req.body.worker + `'`
            }
            sqlQuery += ` GROUP BY tp.Id, DATE(a.Appt_Date_Time__c), tp.Appt_Ticket__c`
        }
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Reports dao - getMonthlyBussinessAnalysisReport:', err);
                callback(err, { statusCode: '9999' });
            } else {
                callback(err, {
                    'srvcTickets': result[0],
                    'prdctTickets': result[1],
                    'srvcServiceGroup': result[2],
                    'serviceTicketStatistics': result[3],
                    'productTicketStatistics': result[4],
                    'rebooked': result[5],
                    'productLine': result[6],
                });
            }
        });
    },
    getClientRetentionReport: function (req, callback) {
        var dbName = req.headers['db'];
        var clientIds = '('
        var clientData = [];
        var clientWithWorkerId = {};
        getNewClientServiceTicketsByDateRange(req.body.startDate, req.body.endDate,
            req.body.worker, req.body.retentionType, dbName, function (err, data) {
                if (data.length > 0) {
                    var ClientWithWorkerIdMap = new Map();
                    for (var i = 0; i < data.length; i++) {
                        if (!ClientWithWorkerIdMap.has(data[i]['clientId'])) {
                            clientIds += '\'' + data[i]['clientId'] + '\',';
                            ClientWithWorkerIdMap.set(data[i]['clientId'], [data[i]['worker']]);

                        } else {
                            var workers = ClientWithWorkerIdMap.get(data[i]['clientId']);
                            if (workers.indexOf(data[i]['worker']) === -1) {
                                workers.push(data[i]['worker']);
                                ClientWithWorkerIdMap.set(data[i]['clientId'], workers);
                            }
                        }
                    }
                    ClientWithWorkerIdMap.forEach((value, key) => {
                        // var clientData = [];
                        clientData.push(key);
                        clientWithWorkerId[key] = value;
                    });
                    clientIds = clientIds.slice(0, -1);
                    clientIds += ')';
                    getNewClientTicketsByClientIds(req.body.startDate, clientIds, req.body.retentionType, dbName, function (err, done) {
                        callback(null, { clientWithWorkerId, done })
                    });
                } else {
                    getNewClientTicketsByClientIds(req.body.startDate, undefined, req.body.retentionType, dbName, function (err, done) {
                        callback(null, { clientWithWorkerId, done })
                    });
                }
            });
    },
    getServiceSalesRecords: function (req, done) {
        var dbName = req.headers['db'];
        var serviceSalesObj = [];
        var serviceSalesRefundObj = [];
        var index = 0;
        try {
            getServicesCountAndSalesByDateRange(req.body.begindate, req.body.enddate, req.body.worker, dbName, function (err, data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i]['serviceCount'] > 0 && !req.body.worker) {
                        var averageSales = data[i]['serviceTotal'] / data[i]['serviceCount'];
                    } else if (data[i]['serviceCount'] > 0 && req.body.worker) {
                        var averageSales = data[i]['workerPrice'] / data[i]['serviceCount'];
                    } else {
                        averageSales = 0;
                    }
                    serviceSalesObj.push({
                        'serviceCount': data[i]['serviceCount'],
                        'totalSales': data[i]['serviceTotal'],
                        'guestCharge': data[i]['guestCharge'],
                        'serviceGroup': data[i]['serviceGroup'],
                        'serviceName': data[i]['serviceName'],
                        'averageSales': averageSales,
                        'workerPrice': data[i]['workerPrice']
                    });
                }
                index++;
                sendSalesReportDetails(index, serviceSalesObj, serviceSalesRefundObj, done)
            });
            getRefundServicesCountAndSalesByDateRange(req.body.begindate, req.body.enddate, req.body.worker, dbName, function (err, data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i]['serviceCount'] > 0) {
                        var averageSales = data[i]['serviceTotal'] / data[i]['serviceCount'];
                    } else {
                        averageSales = 0;
                    }
                    serviceSalesRefundObj.push({
                        'serviceCount': data[i]['serviceCount'],
                        'totalSales': data[i]['serviceTotal'],
                        'guestCharge': data[i]['guestCharge'],
                        'serviceGroup': data[i]['serviceGroup'],
                        'serviceName': data[i]['serviceName'],
                        'averageSales': averageSales
                    });
                }
                index++;
                sendSalesReportDetails(index, serviceSalesObj, serviceSalesRefundObj, done)
            });
        } catch (err) {
            logger.error('Unknown error in reports dao - getDailyCashDrawerRecords:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getTicketSalesReport: function (req, callback) {
        var stDt = req.params.begindate;
        var endDt = req.params.enddate;
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT a.Id, DATE_FORMAT(a.Appt_Date_Time__c, "%Y-%m-%d") as aptDt, a.Client_Type__c, a.Status__c, concat(c.FirstName , " ", c.LastName) as ClientName, '
                + ' Name, Payments__c FROM `Appt_Ticket__c` a LEFT JOIN Contact__c c on c.Id = a.Client__c WHERE DATE(Appt_Date_Time__c) '
                + ' BETWEEN "' + stDt.split(' ')[0] + '" AND "' + endDt.split(' ')[0] + '" AND (a.Status__c = "Checked In" OR a.Status__c = "Complete") '
                + ' AND a.IsDeleted = 0 ORDER BY a.Appt_Date_Time__c';
            execute.query(dbName, sqlQuery, function (error, result) {
                if (error) {
                    logger.error('Error in getting getWorkerDetail: ', error);
                    callback(error, result);
                } else {
                    callback(error, { salesData: result, dates: getDates(stDt.split(' ')[0], endDt.split(' ')[0]) });
                }
            });
        } catch (err) {
            logger.error('Unknown error in reports dao - getTicketSalesReport:', err);
            return (err, { statusCode: '9999' });

        }
    },
    getInventoryUsageReport: function (req, callback) {
        var stDt = req.params.begindate;
        var endDt = req.params.enddate;
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT p.Id as PrdocutId, iu.Id as invUsageId, p.Name, DATE_FORMAT(iu.CreatedDate, "%Y-%m-%d") as date, '
                + ' SUM(iu.Qty_c) Qty_c, SUM(p.Standard_Cost__c) as costEach, SUM((p.Standard_Cost__c * iu.Qty_c)) as totalCost, iu.Used_By_c , '
                + ' IF(iu.Used_By_c = "Worker", CONCAT(u.FirstName, " ", u.LastName), "-") as workerName FROM `Inventory_Usage__c` as iu '
                + ' LEFT JOIN Product__c p on p.Id = iu.Product__c  LEFT JOIN User__c as u on u.Id = iu.Used_By_Worker_c '
                + ' WHERE DATE(iu.CreatedDate) BETWEEN "' + stDt.split(' ')[0] + '" AND "' + endDt.split(' ')[0] + '" GROUP BY iu.Id ORDER BY iu.CreatedDate';
            // var sqlQuery = 'SELECT a.Id as aptId, DATE_FORMAT(a.Appt_Date_Time__c, "%Y-%m-%d") as aptDt, p.Name, tp.Qty_Sold__c, p.Standard_Cost__c as costEach, '
            //     + ' (p.Standard_Cost__c * tp.Qty_Sold__c) as totalCost,  CONCAT(u.FirstName, " ", u.LastName) as workerName '
            //     + ' FROM Ticket_Product__c tp LEFT JOIN Appt_Ticket__c a on a.Id = tp.Appt_Ticket__c LEFT JOIN Product__c p on p.Id = tp.Product__c '
            //     + ' LEFT JOIN User__c u on u.Id = tp.Worker__c WHERE a.Appt_Date_Time__c BETWEEN "' + stDt.split(' ')[0] + '" AND "' + endDt.split(' ')[0] + '" AND a.IsDeleted = 0 '
            //     + ' GROUP BY a.Id ORDER BY a.Appt_Date_Time__c';
            execute.query(dbName, sqlQuery, function (error, result) {
                if (error) {
                    logger.error('Error in getting getWorkerDetail: ', error);
                    callback(error, result);
                } else {
                    callback(error, { usageData: result, dates: getDates(stDt.split(' ')[0], endDt.split(' ')[0]) });
                }
            });
        } catch (err) {
            logger.error('Unknown error in reports dao - getTicketSalesReport:', err);
            return (err, { statusCode: '9999' });

        }
    },
    getProductSalesRecords: function (req, done) {
        var dbName = req.headers['db'];
        var productSalesObj = [];
        try {
            getProductsCountAndSalesByDateRange(req.body.begindate, req.body.enddate, req.body.worker, req.body.pdLine, dbName, function (err, data) {
                for (var i = 0; i < data.length; i++) {
                    productSalesObj.push({
                        'productName': data[i]['productName'],
                        'sku': data[i]['sku'],
                        'size': data[i]['size'],
                        'Unit_of_Measure__c': data[i]['Unit_of_Measure__c'],
                        'apptNumber': data[i]['apptNumber'],
                        'Appt_Date_Time__c': data[i]['Appt_Date_Time__c'],
                        'Qty_Sold__c': data[i]['Qty_Sold__c'],
                        'Net_Price__c': data[i]['Net_Price__c']
                    });
                }
                done(err, productSalesObj)
            });
        } catch (err) {
            logger.error('Unknown error in reports dao - getDailyCashDrawerRecords:', err);
            return (err, { statusCode: '9999' });
        }
    },
    getDailyTotalSheetRecords: function (req, done) {
        var dbName = req.headers['db'];
        var index1 = 0;
        var index2 = 0;
        var workerSalesObj = [];
        var companySalesObj = [];
        var accountBalanceObj = {
            'deposit': 0,
            'depositOnline': 0,
            'prepayment': 0,
            'receivedOnAccount': 0,
            'totals': 0
        };
        var salesType = ['Service Sales', 'Service Refunds', 'Product Sales', 'Product Refunds', 'Gift Sales'
            , 'Gift Sales Online', 'Gift Redeem', 'Membership Sales', 'Package Sales', 'Prepaid Package Usage'
            , 'Misc Sales', 'Totals'];
        for (var i = 0; i < salesType.length; i++) {
            companySalesObj.push({
                'salesType': salesType[i],
                'fullPrice': 0,
                'discounts': 0,
                'discountedSales': 0,
                'taxTotal': 0
            });
        }
        var prdData = [];
        var tipsData = [];
        var paymentsData = [];
        var changeBackData = {
            'paymentType': 'Change Back',
            'electronic': 0,
            'number': 0,
            'amountPaid': 0
        };
        var cashPaidData = [{
            'paymentType': 'Cash Paid In',
            'electronic': 0,
            'number': 0,
            'amountPaid': 0
        },
        {
            'paymentType': 'Cash Paid Out',
            'electronic': 0,
            'number': 0,
            'amountPaid': 0
        }];
        var paymentRecords = [];
        var electronicRefunds = [];
        var grandTotalObj = [];
        var grandTotalType = ['Service Sales', 'Product Sales', 'Other', 'Tips Left In Drawer', 'Cash Paid In',
            'Cash Paid Out', 'Electronic Payment Refunds', 'Total', 'Tips Paid Out'];
        for (var i = 0; i < grandTotalType.length; i++) {
            grandTotalObj.push({
                'allSales': grandTotalType[i],
                'amount': 0,
                'tax': 0,
                'amountTax': 0
            });
        }
        getServiceSales(req.params.begindate, req.params.enddate, dbName, function (err, data) {
            var srvcTempCount = 0;
            for (var i = 0; i < data.length; i++) {
                var duplicate = checkWorker(workerSalesObj, data[i]['workerId']);
                if (duplicate[0]) {
                    workerSalesObj[duplicate[1]]['serviceSales'] += data[i]['servicesales'];
                    workerSalesObj[duplicate[1]]['guestCharge'] += data[i]['guestCharge'];
                    workerSalesObj[duplicate[1]]['serviceTax'] += data[i]['servicetax'];
                    workerSalesObj[duplicate[1]]['totalSales'] += data[i]['totalSales'];
                } else {
                    workerSalesObj.push({
                        'workerName': data[i]['workerName'],
                        'serviceSales': data[i]['servicesales'],
                        'guestCharge': data[i]['guestCharge'],
                        'serviceTax': data[i]['servicetax'],
                        'displayOrder': data[i]['Display_Order__c'],
                        'workerId': data[i]['workerId'],
                        'productSales': 0,
                        'totalSales': data[i]['servicesales'],
                        'tipLeftInDrawer': 0,
                        'tipPaidOut': 0,
                        'totalTips': 0,
                        'image': data[i]['image']
                    });
                }
                if (data[i]['isRefund']) {
                    companySalesObj[1]['fullPrice'] += data[i]['servicesales'];
                    companySalesObj[1]['discountedSales'] += data[i]['servicesales'];
                    companySalesObj[1]['discounts'] += 0;
                    companySalesObj[1]['taxTotal'] += data[i]['servicetax'];
                } else {
                    companySalesObj[0]['fullPrice'] += data[i]['serviceprice'];
                    companySalesObj[0]['discountedSales'] += data[i]['servicesales'];
                    companySalesObj[0]['discounts'] += (data[i]['serviceprice'] - data[i]['servicesales']);
                    companySalesObj[0]['taxTotal'] += data[i]['servicetax'];
                    srvcTempCount += data[i]['srvcCount'];
                }
                if (data[i]['clientPackage']) {
                    companySalesObj[9]['fullPrice'] += data[i]['serviceprice'] * -1;
                    companySalesObj[9]['discountedSales'] += data[i]['servicesales'] * -1;
                    companySalesObj[9]['discounts'] += (data[i]['serviceprice'] - data[i]['servicesales']) * -1;
                    companySalesObj[9]['taxTotal'] += data[i]['servicetax'] * -1;
                }
            }
            companySalesObj[0]['srvcCount'] = srvcTempCount;
            index1++
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done)
        });
        getProductSales(req.params.begindate, req.params.enddate, dbName, function (err, data) {
            var prdTempCount = 0;
            for (var i = 0; i < data[0].length; i++) {
                if (data[0][i]['isRefund']) {
                    companySalesObj[3]['fullPrice'] += data[0][i]['productsales'];
                    companySalesObj[3]['discountedSales'] += data[0][i]['productsales'];
                    companySalesObj[3]['discounts'] += 0;
                    companySalesObj[3]['taxTotal'] += data[0][i]['producttax'];

                } else {
                    companySalesObj[2]['fullPrice'] += data[0][i]['productprice'];
                    companySalesObj[2]['discountedSales'] += data[0][i]['productsales'];
                    companySalesObj[2]['discounts'] += data[0][i]['qtySold'] * (data[0][i]['productprice'] - data[0][i]['netprice']);
                    companySalesObj[2]['taxTotal'] += data[0][i]['producttax'];
                    prdTempCount += data[0][i]['prdCount'];

                }

            }
            companySalesObj[2]['prdCount'] = prdTempCount;
            prdData = data[1];
            index1++;
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done);
        });
        getTips(req.params.begindate, req.params.enddate, dbName, function (err, data) {
            tipsData = data;
            index1++;
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done);
        });
        getOtherSales(req.params.begindate, req.params.enddate, dbName, function (err, data) {
            for (i = 0; i < data.length; i++) {
                if (data[i]['transType'] === 'Membership') {
                    companySalesObj[7]['fullPrice'] += data[i]['amount'];
                    companySalesObj[7]['discountedSales'] += data[i]['amount'];
                } else if (data[i]['transType'] === 'Package') {
                    companySalesObj[8]['fullPrice'] += data[i]['packageprice'];
                    companySalesObj[8]['discountedSales'] += data[i]['packageprice'];
                    companySalesObj[8]['taxTotal'] += data[i]['servicetax'];
                } else if (data[i]['transType'] === 'Pre Payment') {
                    companySalesObj[9]['fullPrice'] += data[i]['amount'];
                    companySalesObj[9]['discountedSales'] += data[i]['amount'];
                } else if (data[i]['transType'] === 'Misc Sale') {
                    companySalesObj[10]['fullPrice'] += data[i]['amount'];
                    companySalesObj[10]['discountedSales'] += data[i]['amount'];
                } else if (data[i]['transType'] === 'Gift' && !data[i]['onlineField']) {
                    companySalesObj[4]['fullPrice'] += data[i]['amount'];
                    companySalesObj[4]['discountedSales'] += data[i]['amount'];
                } else if (data[i]['transType'] === 'Gift' && data[i]['onlineField']) {
                    companySalesObj[5]['fullPrice'] += data[i]['amount'];
                    companySalesObj[5]['discountedSales'] += data[i]['amount'];
                }
            }
            index1++;
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done);
        });

        getAccountBalances(req.params.begindate, req.params.enddate, dbName, function (err, data) {
            for (i = 0; i < data.length; i++) {
                if (!data[i]['onlineField'] && data[i]['transType'] === 'Deposit') {
                    accountBalanceObj['deposit'] += data[i]['amount'];
                } else if (data[i]['onlineField'] && data[i]['transType'] === 'Deposit') {
                    accountBalanceObj['depositOnline'] = data[i]['amount'];
                } else if (data[i]['transType'] === 'Received on Account') {
                    accountBalanceObj['receivedOnAccount'] = data[i]['amount'];
                } else if (data[i]['transType'] === 'Pre Payment') {
                    accountBalanceObj['prepayment'] = data[i]['amount'];
                }
            }
            index1++;
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done);

        });
        getPaymentTypeList(dbName, function (err, data) {
            for (i = 0; i < data.length; i++) {
                paymentsData.push({
                    'paymentType': data[i]['Name'],
                    'electronic': data[i]['Process_Electronically__c'],
                    'number': 0,
                    'amountPaid': 0
                });
            }
            index1++;
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done);
        });
        getPayments(req.params.begindate, req.params.enddate, dbName, function (err, data) {
            paymentRecords = data;
            for (i = 0; i < data.length; i++) {
                if (data[i]['paymentType'] === 'Gift Redeem') {
                    companySalesObj[6]['fullPrice'] += data[i]['amount'] * -1;
                    companySalesObj[6]['discountedSales'] += data[i]['amount'] * -1;
                }
                // else if (data[i]['paymentType'] === 'Prepaid Package') {
                //     companySalesObj[9]['fullPrice'] += data[i]['amount'] * -1;
                //     companySalesObj[9]['discountedSales'] += data[i]['amount'] * -1;
                // }
            }
            index1++;
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done);
        });
        getPaymentOverchargeRefundPayments(req.params.begindate, req.params.enddate, dbName, function (err, data) {
            electronicRefunds = data;
            index1++;
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done);

        });
        getBalanceDueApptTicketsByReportDate(req.params.begindate, req.params.enddate, dbName, function (err, bdone) {
            var numTotal = 0;
            var amtTotal = 0;
            for (var i = 0; i < bdone[0].length; i++) {
                bdone[0][i]['balancedue'] = bdone[0][i]['Included_Ticket_Amount__c'];
                var filter1 = bdone[1].filter(function (a) { return a['Id'] === bdone[0][i]['appId'] });
                var filter2 = bdone[2].filter(function (a) { return a['Id'] === bdone[0][i]['appId'] })
                var filter3 = bdone[3].filter(function (a) { return a['Id'] === bdone[0][i]['appId'] })
                var filter4 = bdone[4].filter(function (a) { return a['Id'] === bdone[0][i]['appId'] })
                var filter5 = bdone[5].filter(function (a) { return a['Id'] === bdone[0][i]['appId'] })
                if (filter1.length > 0) {
                    bdone[0][i]['balancedue'] += filter1[0]['tsNet_Price__c'];
                }
                if (filter2.length > 0) {
                    bdone[0][i]['balancedue'] += filter2[0]['tpNet_Price__c'];
                }
                if (filter3.length > 0) {
                    bdone[0][i]['balancedue'] += filter3[0]['oAmount__c'];
                }
                if (filter4.length > 0) {
                    bdone[0][i]['balancedue'] -= filter4[0]['tpyAmount_Paid__c'];
                }
                if (filter5.length > 0) {
                    bdone[0][i]['balancedue'] += parseFloat(filter5[0]['tipTip_Amount__c']);
                }
                if (bdone[0][i]['balancedue'] < 0) {
                    numTotal += bdone[0][i]['countId'];
                    amtTotal += bdone[0][i]['balancedue'];
                }
            }
            changeBackData['number'] = numTotal;
            changeBackData['amountPaid'] = amtTotal;
            index1++;
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done);
        });
        getCashInOutRecordsByDate(req.params.begindate, req.params.enddate, dbName, function (err, data) {
            for (i = 0; i < data.length; i++) {
                if (data[i]['Type__c'] === 'Cash Paid In') {
                    cashPaidData[0]['number'] += 1;
                    cashPaidData[0]['amountPaid'] += data[i]['Amount__c'];
                } else {
                    cashPaidData[1]['number'] += 1;
                    cashPaidData[1]['amountPaid'] += data[i]['Amount__c'];
                }
            }
            index1++;
            addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
                paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done);
        });
    }
}

function getServiceSales(beginDate, endDate, dbName, callback) {
    var sqlQuery = 'select u.image, COUNT(u.Id) srvcCount,u.Id workerId, IFNULL(u.Display_Order__c, 0) Display_Order__c, CONCAT(u.FirstName," ", u.LastName) workerName, sum(ts.Net_Price__c) servicesales, sum(ts.Price__c) serviceprice, '
        + ' IFNULL(sum(ts.Service_Tax__c), 0) servicetax, at.isRefund__c as isRefund, ts.Client_Package__c as clientPackage, IFNULL(sum(ts.Guest_Charge__c), 0) guestCharge '
        + ' from Ticket_Service__c as ts '
        + ' LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c '
        + ' LEFT JOIN User__c as u on u.Id = ts.Worker__c '
        + ' where DATE(at.Appt_Date_Time__c) >= "' + beginDate + '" '
        + ' and DATE(at.Appt_Date_Time__c) <= "' + endDate + '" '
        + ' and at.IsTicket__c = 1 and ts.Isdeleted = 0 '
        + ' group by u.Id, u.Username, at.isRefund__c, ts.Client_Package__c';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}

function getProductSales(beginDate, endDate, dbName, callback) {
    var sqlQuery = 'select COUNT(u.Id) prdCount,p.Id productId, p.Name productName, u.Id workerId,u.Display_Order__c,CONCAT(u.FirstName,"",u.LastName) workerName,'
        + ' IFNULL(sum(tp.Qty_Sold__c  *  tp.Net_Price__c), 0) productsales, IFNULL(avg(tp.Net_Price__c), 0) netprice, IFNULL(avg(tp.Price__c), 0) productprice, '
        + ' IFNULL(sum(tp.Product_Tax__c), 0) producttax, IFNULL(sum(tp.Qty_Sold__c), 0) qtySold, at.isRefund__c isRefund '
        + ' from Ticket_Product__c as tp '
        + ' LEFT JOIN Appt_Ticket__c as at on at.Id= tp.Appt_Ticket__c '
        + ' left JOIN Product__c as p on p.Id =tp.Product__c '
        + ' LEFT JOIN User__c as u on u.Id = tp.Worker__c '
        + ' where DATE(at.Appt_Date_Time__c)  >= "' + beginDate + '" '
        + ' and DATE(at.Appt_Date_Time__c)  <= "' + endDate + '" '
        + ' and at.IsTicket__c = 1 '
        + ' and tp.IsDeleted = 0 group by p.id, u.Id, at.isRefund__c;';
    sqlQuery += 'select COUNT(u.Id) prdCount,p.Id productId, p.Name productName, u.Id workerId,u.Display_Order__c,CONCAT(u.FirstName,"",u.LastName) workerName,'
        + ' IFNULL(sum(tp.Qty_Sold__c  *  tp.Net_Price__c), 0) productsales, IFNULL(avg(tp.Net_Price__c), 0) netprice, IFNULL(avg(tp.Price__c), 0) productprice, '
        + ' IFNULL(sum(tp.Product_Tax__c), 0) producttax, IFNULL(sum(tp.Qty_Sold__c), 0) qtySold, at.isRefund__c isRefund '
        + ' from Ticket_Product__c as tp '
        + ' LEFT JOIN Appt_Ticket__c as at on at.Id= tp.Appt_Ticket__c '
        + ' left JOIN Product__c as p on p.Id =tp.Product__c '
        + ' LEFT JOIN User__c as u on u.Id = tp.Worker__c '
        + ' where DATE(at.Appt_Date_Time__c)  >= "' + beginDate + '" '
        + ' and DATE(at.Appt_Date_Time__c)  <= "' + endDate + '" '
        + ' and at.IsTicket__c = 1 '
        + ' and tp.IsDeleted = 0 group by u.Id, at.isRefund__c';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getTips(beginDate, endDate, dbName, callback) {
    var sqlQuery = 'select at.Id, u.Id workerId, IFNULL(u.Display_Order__c,0) Display_Order__c,CONCAT(u.FirstName,"",u.LastName) workerName, t.Tip_Option__c tipOption,'
        + ' IFNULL(sum(t.Tip_Amount__c), 0) tips from Ticket_Tip__c as t left JOIN Appt_Ticket__c as at on at.Id = t.Appt_Ticket__c '
        + ' LEFT JOIN User__c as u on u.Id = t.Worker__c'
        + ' where DATE(at.Appt_Date_Time__c) >= "' + beginDate + '" '
        + ' and DATE(at.Appt_Date_Time__c) <= "' + endDate + '" '
        + ' and at.IsTicket__c = 1 '
        + ' and t.isDeleted= 0 group by at.Id, u.Id, t.Tip_Option__c';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getOtherSales(beginDate, endDate, dbName, callback) {
    var sqlQuery = 'select IFNULL(sum(t.Amount__c), 0) amount, IFNULL(sum(t.Package_Price__c), 0) packageprice, IFNULL(sum(t.Service_Tax__c), 0) servicetax, '
        + ' t.Transaction_Type__c transType, IFNULL(t.Online__c, 0) onlineField from Ticket_Other__c as t '
        + ' LEFT JOIN Appt_Ticket__c as at on at.Id = t.Ticket__c  where DATE(at.Appt_Date_Time__c) >= "' + beginDate + '"'
        + ' and DATE(at.Appt_Date_Time__c) <= "' + endDate + '" and t.isDeleted= 0 group by t.Transaction_Type__c, t.Online__c';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getAccountBalances(beginDate, endDate, dbName, callback) {
    var sqlQuery = 'select t.Transaction_Type__c transType, IFNULL(sum(t.Amount__c), 0) amount, t.Online__c onlineField '
        + ' from Ticket_Other__c as t LEFT JOIN Appt_Ticket__c as at on at.Id =t.Ticket__c '
        + ' where DATE(at.Appt_Date_Time__c) >= "' + beginDate + '" and DATE(at.Appt_Date_Time__c)  <= "' + endDate + '" '
        + ' and t.Transaction_Type__c in ("Deposit","Pre Payment","Received on Account") '
        + ' and t.isDeleted= 0 group by t.Transaction_Type__c, t.Online__c ';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
/*
	 * Return a list of all Payment type records only for Total Sheet
	 */
function getPaymentTypeList(dbName, callback) {
    var sqlQuery = 'Select p.Reads_Only_Name__c, p.Read_Only_Active_Flag__c, p.Transaction_Fee_Percentage__c, p.Transaction_Fee_Per_Transaction__c,'
        + ' p.SystemModstamp, p.Sort_Order__c, p.Process_Electronically__c, p.OwnerId, p.Name, p.Minimum_Purchase_Amount__c, '
        + ' p.LastModifiedDate, p.Process_Electronically_Online__c, p.LastModifiedById, p.IsDeleted, p.Id, p.CreatedDate, p.CreatedById,'
        + ' p.Active__c, p.Abbreviation__c, Icon_Document_Name__c from Payment_Types__c p where p.Name != "Prepaid Package" and '
        + ' p.Name != "Gift Redeem" and p.IsDeleted = 0 order by Sort_Order__c asc limit 50';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getPayments(beginDate, endDate, dbName, callback) {
    var sqlQuery = 'select p.Name paymentType, p.Process_Electronically__c, IFNULL(sum(tp.Amount_Paid__c), 0) amount, COUNT(DISTINCT(tp.Id)) countId '
        + ' from Ticket_Payment__c as tp LEFT JOIN Payment_Types__c as p on p.Id = tp.Payment_Type__c '
        + ' LEFT JOIN Appt_Ticket__c as at on at.Id = tp.Appt_Ticket__c where DATE(at.Appt_Date_Time__c) >= "' + beginDate + '" '
        + ' and DATE(at.Appt_Date_Time__c) <= "' + endDate + '" and at.IsTicket__c = 1 and p.Name IS NOT NULL '
        + ' and tp.Original_Ticket_Payment__c IS NULL group by p.Name';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getPaymentOverchargeRefundPayments(beginDate, endDate, dbName, callback) {
    var sqlQuery = 'select p.Name paymentType,p.Process_Electronically__c, IFNULL(sum(tp.Amount_Paid__c), 0) amount, COUNT(DISTINCT(tp.Id)) countId '
        + ' from Ticket_Payment__c as tp LEFT JOIN Payment_Types__c as p on p.Id = tp.Payment_Type__c '
        + ' left JOIN Appt_Ticket__c as at on at.Id = tp.Appt_Ticket__c where DATE(at.Appt_Date_Time__c) >= "' + beginDate + '" '
        + ' and DATE(at.Appt_Date_Time__c)  <= "' + endDate + '" and at.IsTicket__c = 1  and p.Name IS NOT null '
        + ' and tp.Original_Ticket_Payment__c IS NOT NULL and at.isRefund__c = 1 group by p.Name';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getBalanceDueApptTicketsByReportDate(startDatetime, endDatetime, dbName, callback) {
    var sqlQuery = 'select COUNT(DISTINCT(at.Id)) countId, at.id appId, IFNULL(at.Included_Ticket_Amount__c,0) Included_Ticket_Amount__c from Appt_Ticket__c as at'
        + ' where at.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND at.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
        + ' and at.isRefund__c = 0 and at.IsDeleted = 0 '
        + ' GROUP by at.Id order by at.Name asc';
    var sqlQuery1 = 'select a.Id, sum(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c from Ticket_Service__c as ts'
        + ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and ts.Is_Class__c = 0 and ts.IsDeleted = 0 GROUP by a.Id';
    var sqlQuery2 = 'select a.Id, SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))'
        + ' tpNet_Price__c from Ticket_Product__c as tp'
        + ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and tp.IsDeleted = 0 GROUP by a.Id';

    var sqlQuery3 = 'select a.Id, SUM(IFNULL(o.Amount__c, 0)) oAmount__c,o.Transaction_Type__c from Ticket_Other__c as o '
        + ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and o.Transaction_Type__c != "Membership" and o.IsDeleted = 0 GROUP by a.Id';
    var sqlQuery4 = 'select a.Id, SUM(IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c from Ticket_Payment__c as tpy'
        + ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and tpy.IsDeleted = 0 GROUP by a.Id';
    var sqlQuery5 = 'select a.Id, IFNULL(tip.Tip_Amount__c, 0) tipTip_Amount__c from Ticket_Tip__c as tip'
        + ' left join Appt_Ticket__c as a on a.Id = tip.Appt_Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and a.Status__c ="Complete"  and tip.isDeleted=0 ';
    execute.query(dbName, sqlQuery + ';' + sqlQuery1 + ';' + sqlQuery2
        + ';' + sqlQuery3 + ';' + sqlQuery4 + ';' + sqlQuery5, function (error, result) {
            if (error) {
                logger.error('Error in getting getWorkerDetail: ', error);
                callback(error, result);
            } else {
                callback(error, result);
            }
        });
}
function getCashInOutRecordsByDate(beginDate, endDate, dbName, callback) {
    var sqlQuery = 'select Id, Amount__c, Type__c, CreatedDate  from Cash_In_Out__c  where CreatedDate >="' + beginDate + ' ' + config.startTimeOfDay + '" '
        + ' and CreatedDate  <= "' + endDate + ' ' + config.endtTimeOfDay + '" order by Name asc';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getServicesCountAndSalesByDateRange(beginDate, endDate, worker, dbName, callback) {
    var sqlQuery = 'select count(ts.Service__c) serviceCount, s.Name serviceName, s.Service_Group__c serviceGroup,'
        + ' IFNULL(sum(ts.Net_Price__c), 0) serviceTotal, IFNULL(sum(ts.Guest_Charge__c),0) guestCharge, '
        + '(IFNULL(sum(ts.Net_Price__c), 0) - IFNULL(sum(ts.Guest_Charge__c),0)) workerPrice'
        + ' from Ticket_Service__c as ts LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c '
        + ' LEFT JOIN Service__c as s on s.Id = ts.Service__c where at.isTicket__c = 1 '
        + ' and at.Status__c ="Complete"  and at.Is_Booked_Out__c != 1 and at.IsRefund__c != 1 '
        + ' and ts.Service_Date_Time__c >= "' + beginDate + ' ' + config.startTimeOfDay + '" and ts.Service_Date_Time__c  <= "' + endDate + ' ' + config.endtTimeOfDay + '"'
    if (worker) {
        sqlQuery += ' and ts.Worker__c ="' + worker + '" '
    }
    sqlQuery += ' and ts.IsDeleted = 0 GROUP BY s.Name, s.Service_Group__c ORDER BY s.Service_Group__c, s.Name';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getProductsCountAndSalesByDateRange(beginDate, endDate, worker, pdLine, dbName, callback) {
    var sqlQuery = `select
            count(tp.Product__c) productCount,
                p.Name productName,
                    IFNULL(sum(tp.Net_Price__c), 0) productTotal,
                        p.Product_Code__c as sku,
                        at.Name as apptNumber,
                        p.Size__c size,
                            p.Unit_of_Measure__c,
                            at.Appt_Date_Time__c,
                            tp.Qty_Sold__c,
                            (tp.Qty_Sold__c * tp.Net_Price__c) Net_Price__c
            from Ticket_Product__c as tp
            LEFT JOIN Appt_Ticket__c as at on at.Id = tp.Appt_Ticket__c
            LEFT JOIN Product__c as p on p.Id = tp.Product__c
            where at.isTicket__c = 1 and
            at.Status__c = "Complete"
            and at.Is_Booked_Out__c != 1
            and at.Appt_Date_Time__c >= '` + beginDate + ' ' + config.startTimeOfDay + `'
            and at.Appt_Date_Time__c <= '` + endDate + ' ' + config.endtTimeOfDay + `'`
    if (worker) {
        sqlQuery += ' and tp.Worker__c ="' + worker + '" '
    }
    if (pdLine != 'All') {
        sqlQuery += ' and p.Product_Line__c ="' + pdLine + '" '
    }
    sqlQuery += ' and tp.IsDeleted = 0 GROUP BY tp.Id ORDER BY p.Name';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getRefundServicesCountAndSalesByDateRange(beginDate, endDate, worker, dbName, callback) {
    var sqlQuery = 'select count(ts.Service__c) serviceCount, s.Name serviceName, s.Service_Group__c serviceGroup,'
        + ' sum(ts.Net_Price__c) serviceTotal from Ticket_Service__c as ts '
        + ' LEFT JOIN Appt_Ticket__c as at on at.Id =ts.Appt_Ticket__c'
        + ' LEFT JOIN Service__c as s on s.Id = ts.Service__c'
        + ' where at.isTicket__c = 1 and at.Status__c ="Complete" '
        + ' and at.Is_Booked_Out__c != 1 and at.IsRefund__c = 1 '
        + ' and ts.Service_Date_Time__c >= "' + beginDate + ' ' + config.startTimeOfDay + '"'
        + ' and ts.Service_Date_Time__c <= "' + endDate + ' ' + config.endtTimeOfDay + '" '
    if (worker) {
        sqlQuery += ' and ts.Worker__c = "' + worker + '" '
    }
    sqlQuery += ' GROUP BY s.Name, s.Service_Group__c '
        + ' ORDER BY s.Service_Group__c, s.Name';
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getAllTicketsForDateRangeOrderedByTicketNumber(startDatetime, endDatetime, dbName, callback) {
    var sqlQuery = 'select at.id appId, at.Name,CONCAT(c.FirstName, " ", c.LastName) as FullName, at.Client__c, at.Client_Type__c, c.System_Client__c, at.Appt_Date_Time__c, at.Status__c, at.Status_Color__c,'
        + ' at.isTicket__c, at.Notes__c, ts.Rebooked__c, at.Business_Rebook__c, at.Booked_Online__c, at.Is_Standing_Appointment__c, '
        + ' IFNULL(at.Included_Ticket_Amount__c,0) Included_Ticket_Amount__c'
        + ' , at.isNoService__c, at.isRefund__c, at.Is_Class__c, tp.Amount_Paid__c '
        + ' from Appt_Ticket__c as at'
        + ' LEFT JOIN Ticket_Payment__c as tp on tp.Appt_Ticket__c=at.Id'
        + ' LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = at.Id'
        + ' LEFT JOIN Payment_Types__c as p on p.Id =tp.Payment_Type__c'
        + ' LEFT JOIN Contact__c as c on c.Id = at.Client__c AND c.IsDeleted = 0'
        + ' where at.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" AND at.Appt_Date_Time__c < "' + endDatetime + ' ' + config.endtTimeOfDay + '"'
        + ' and at.Is_Class__c = 0 and at.IsDeleted = 0 and ( at.Status__c = "Checked In" or at.Status__c = "Complete" or at.Status__c = "Pending Deposit" )'
        + ' GROUP by at.Id order by at.Name asc';
    var sqlQuery1 = 'select a.Id, SUM(IFNULL(ts.Net_Price__c,0)) Net_Price__c, SUM(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c from Ticket_Service__c as ts'
        + ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and ts.Is_Class__c = 0 and ts.IsDeleted = 0 GROUP by a.Id';
    var sqlQuery2 = 'select a.Id, SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0))) Net_Price__c,SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))'
        + ' tpNet_Price__c from Ticket_Product__c as tp'
        + ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and tp.IsDeleted = 0 GROUP by a.Id';
    var sqlQuery3 = 'select a.Id, SUM(IFNULL(o.Amount__c, 0)) oAmount__c,o.Transaction_Type__c from Ticket_Other__c as o '
        + ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and o.IsDeleted = 0 GROUP by a.Id';
    var sqlQuery4 = 'select a.Id, SUM(IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c ,'
        + ' GROUP_CONCAT(p.Abbreviation__c) Abbreviation__c from Ticket_Payment__c as tpy'
        + ' LEFT JOIN Payment_Types__c p on p.Id =tpy.Payment_Type__c '
        + ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and tpy.IsDeleted = 0 GROUP by a.Id';
    execute.query(dbName, sqlQuery + ';' + sqlQuery1 + ';' + sqlQuery2 + ';' + sqlQuery3 + ';' + sqlQuery4, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getCashInOutTicketsForDateRange(userId, startDatetime, endDatetime, dbName, callback) {
    var sqlQuery = 'select Id, Name, Amount__c, Drawer_Name__c, Drawer_Number__c, From__c, Reason__c, To__c,'
        + ' Transaction_By__c, Type__c, CreatedDate from Cash_In_Out__c'
        + ' where CreatedDate  >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and CreatedDate <= "' + endDatetime + ' ' + config.endtTimeOfDay + '" order by Name asc;';
    sqlQuery += 'SELECT Can_View_Appt_Values_Totals__c FROM User__c where Id="' + userId + '"'
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}

function getCashInOutTicketsBalnceDue(startDatetime, endDatetime, dbName, callback) {
    var sqlQuery1 = 'select at.Id appId,at.Name apptName, at.Appt_Date_Time__c, at.Client__c, at.Client_Type__c, CONCAT(c.FirstName," ",  c.LastName) FullName,'
        + ' at.Payments__c, at.Tips__c, IFNULL(at.Included_Ticket_Amount__c,0) includAmt,at.isRefund__c , at.LastModifiedById '
        + ' from Appt_Ticket__c as at '
        + ' LEFT JOIN Contact__c as c on c.Id = at.Client__c AND c.IsDeleted = 0'
        + ' where at.Status__c ="Complete" '
        + ' and at.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and at.Appt_Date_Time__c <= "' + endDatetime + ' ' + config.endtTimeOfDay + '" '
        + ' and at.isDeleted = 0 GROUP by at.Id'
        + ' order by at.Appt_Date_Time__c desc';
    var sqlQuery2 = 'select a.Id, sum(IFNULL(ts.Net_Price__c,0)+ IFNULL(ts.Service_Tax__c,0)) tsNet_Price__c from Ticket_Service__c as ts'
        + ' left join Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and a.Status__c ="Complete" and ts.Is_Class__c = 0 and ts.isDeleted = 0 GROUP by a.Id';
    var sqlQuery3 = 'select a.Id, SUM((IFNULL(tp.Qty_Sold__c,0) * IFNULL(tp.Net_Price__c,0)) + IFNULL(tp.Product_Tax__c, 0))'
        + ' tpNet_Price__c from Ticket_Product__c as tp'
        + ' left join Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and a.Status__c ="Complete" and tp.isDeleted = 0 GROUP by a.Id';

    var sqlQuery4 = 'select a.Id, SUM(IFNULL(o.Amount__c, 0)) oAmount__c,o.Transaction_Type__c from Ticket_Other__c as o '
        + ' left join Appt_Ticket__c as a on a.Id = o.Ticket__c'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and a.Status__c ="Complete" and o.isDeleted = 0 GROUP by a.Id';
    var sqlQuery5 = 'select a.Id, CONCAT(u.FirstName, "", u.LastName) lastModify,tpy.LastModifiedDate, tpy.Payment_Type__c, p.Name paymentType,tpy.Drawer_Number__c, tpy.LastModifiedById, (IFNULL(tpy.Amount_Paid__c, 0))  tpyAmount_Paid__c from Ticket_Payment__c as tpy'
        + ' left join Appt_Ticket__c as a on a.Id = tpy.Appt_Ticket__c'
        + ' LEFT JOIN Payment_Types__c as p on p.Id=tpy.Payment_Type__c '
        + ' LEFT JOIN User__c as u on u.Id= tpy.LastModifiedById'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '"'
        + ' and a.Status__c ="Complete" and tpy.isDeleted = 0 order by tpy.Drawer_Number__c desc';
    /* and p.Name !="Gift Redeem" */
    var sqlQuery6 = 'select a.Id, CONCAT(u.FirstName, "", u.LastName) lastModify,tip.Tip_Option__c, tip.Drawer_Number__c, tip.LastModifiedById, IFNULL(tip.Tip_Amount__c, 0) tipTip_Amount__c from Ticket_Tip__c as tip'
        + ' left join Appt_Ticket__c as a on a.Id = tip.Appt_Ticket__c'
        + ' LEFT JOIN User__c as u on u.Id= tip.LastModifiedById'
        + ' where a.Appt_Date_Time__c >= "' + startDatetime + ' ' + config.startTimeOfDay + '" and a.Appt_Date_Time__c <"' + endDatetime + ' ' + config.endtTimeOfDay + '" and a.Status__c ="Complete"  and tip.isDeleted=0 ';
    execute.query(dbName, sqlQuery1 + ';' + sqlQuery2 + ';' + sqlQuery3 + ';' + sqlQuery4 + ';' + sqlQuery5 + ';' + sqlQuery6, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getDailyCashDrawerDateRange(cashDrawer, startDatetime, endDatetime, dbName, callback) {
    var sqlQuery1 = 'select Name, Checkbox__c, Date__c, Number__c, Text__c, Encrypted__c, JSON__c from Preference__c where Name = "' + cashDrawer + '" ';

    var sqlQuery2 = 'select Id, Cash_Drawer__c, Cash_Drawer_Number__c, Date__c, Status__c, Cash_In_Out_Total__c, Cash_Over_Under__c,'
        + ' Close_1__c, Close_10__c, Close_100__c, Close_10_cent__c, Close_1_cent__c, Close_20__c, Close_25_cent__c, Close_5__c, Close_50__c,'
        + ' Close_50_cent__c, Close_5_cent__c, Closing_Cash__c, Open_1__c, Open_10__c, Open_100__c, Open_10_cent__c, Open_1_cent__c, Open_20__c,'
        + ' Open_25_cent__c, Open_5__c, Open_50__c, Open_50_cent__c, Open_5_cent__c, Opening_Cash__c, Total_Close__c, Total_Open__c, '
        + ' Transaction_Total__c from Daily_Cash__c where Date__c ="' + startDatetime + '" ';

    var sqlQuery3 = 'select Id, Name, Amount__c, Drawer_Name__c, Drawer_Number__c, From__c, '
        + ' Reason__c, To__c, Transaction_By__c, Type__c, CreatedDate'
        + ' from Cash_In_Out__c'
        + ' where  CreatedDate >= "' + startDatetime + ' ' + config.startTimeOfDay + '"'
        + ' and CreatedDate <= "' + endDatetime + ' ' + config.endtTimeOfDay + '" '
        + ' and (Drawer_Number__c IS NOT NULL AND Drawer_Number__c !="") ORDER BY Cash_In_Out__c.Drawer_Number__c ASC';
    var sqlQuery4 = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + " WHERE Name = 'Cash Drawers' ORDER BY Name";
    execute.query(dbName, sqlQuery1 + '; ' + sqlQuery2 + ';' + sqlQuery3 + ';' + sqlQuery4, '', function (err, result) {
        if (err)
            logger.error('Error getting getDailyCashDrawer : ', err);
        callback(err, result);
    });
}
function finalResponse(index, workerSalesObj, companySalesObj, accountBalanceObj, paymentsData, grandTotalObj, done) {
    if (index === 1) {
        done(null, {
            'workerSalesObj': workerSalesObj,
            'companySalesObj': companySalesObj,
            'accountBalanceObj': accountBalanceObj,
            'paymentsData': paymentsData,
            'grandTotalObj': grandTotalObj
        });
    }
}

function addProductSales(index1, workerSalesObj, prdData, tipsData, companySalesObj, accountBalanceObj,
    paymentsData, paymentRecords, changeBackData, cashPaidData, electronicRefunds, grandTotalObj, index2, done) {
    var amountTotal = 0;
    var numberTotal = 0;
    var tipPOut = {
        'paymentType': 'Less Tip Paid Out',
        'electronic': 0,
        'number': 0,
        'amountPaid': 0
    };
    if (index1 === 10) {
        for (var i = 0; i < workerSalesObj.length; i++) {
            for (var j = 0; j < prdData.length; j++) {
                if (workerSalesObj[i]['workerId'] == prdData[j]['workerId']) {
                    workerSalesObj[i]['productSales'] += prdData[j]['productsales'];
                    prdData[j]['addParam'] = true;
                }
            }
            workerSalesObj[i]['totalSales'] = workerSalesObj[i]['serviceSales'] + workerSalesObj[i]['productSales'];
            for (var j = 0; j < tipsData.length; j++) {
                if (workerSalesObj[i]['workerId'] == tipsData[j]['workerId']) {
                    if (tipsData[j]['tipOption'] === 'Tip Paid Out') {
                        workerSalesObj[i]['tipPaidOut'] += tipsData[j]['tips'];
                        tipPOut['number'] += 1;
                        tipPOut['amountPaid'] -= tipsData[j]['tips'];
                    } else {
                        workerSalesObj[i]['tipLeftInDrawer'] += tipsData[j]['tips'];
                    }
                    tipsData[j]['addParam'] = true;
                }
                workerSalesObj[i]['totalTips'] = workerSalesObj[i]['tipPaidOut'] + workerSalesObj[i]['tipLeftInDrawer'];
            }
        }
        var prdfilData = prdData.filter(function (obj) { return !obj['addParam'] });
        var tipfilData = tipsData.filter(function (obj) { return !obj['addParam'] });
        prdfilData.forEach((obj) => {
            workerSalesObj.push({
                'workerName': obj['workerName'],
                'serviceSales': 0,
                'guestCharge': 0,
                'serviceTax': 0,
                'displayOrder': obj['Display_Order__c'],
                'workerId': obj['workerId'],
                'productSales': obj['productsales'],
                'totalSales': obj['productsales'],
                'tipLeftInDrawer': 0,
                'tipPaidOut': 0,
                'totalTips': 0,
                'image': ''
            });
        })
        tipfilData.forEach((obj) => {
            workerSalesObj.push({
                'workerName': obj['workerName'],
                'serviceSales': 0,
                'guestCharge': 0,
                'serviceTax': 0,
                'displayOrder': obj['Display_Order__c'],
                'workerId': obj['workerId'],
                'productSales': 0,
                'totalSales': 0,
                'tipLeftInDrawer': 0,
                'tipPaidOut': 0,
                'totalTips': obj['tips'],
                'image': ''
            });
        })
        var nullVal = workerSalesObj.filter(function (a) { return a['displayOrder'] === 0 });
        var notNullVal = workerSalesObj.filter(function (a) { return a['displayOrder'] !== 0 });
        notNullVal.sort(function (a, b) {
            return parseInt(a.displayOrder) - parseInt(b.displayOrder);
        });
        workerSalesObj = notNullVal.concat(nullVal);
        var totalObj = {
            'workerName': 'Totals',
            'serviceSales': workerSalesObj.reduce(function (sum, wrkObj) { return sum + wrkObj.serviceSales; }, 0),
            'productSales': workerSalesObj.reduce(function (sum, wrkObj) { return sum + wrkObj.productSales; }, 0),
            'totalSales': workerSalesObj.reduce(function (sum, wrkObj) { return sum + wrkObj.totalSales; }, 0),
            'totalTips': workerSalesObj.reduce(function (sum, wrkObj) { return sum + wrkObj.totalTips; }, 0),
            'guestCharge': workerSalesObj.reduce(function (sum, wrkObj) { return sum + wrkObj.guestCharge; }, 0),

        }
        workerSalesObj.push(totalObj);
        var cmpySaleslength = companySalesObj.length;
        for (var i = 0; i < cmpySaleslength; i++) {
            if (i !== cmpySaleslength - 1) {
                companySalesObj[cmpySaleslength - 1]['fullPrice'] += companySalesObj[i]['fullPrice'];
                companySalesObj[cmpySaleslength - 1]['discounts'] += companySalesObj[i]['discounts'];
                companySalesObj[cmpySaleslength - 1]['discountedSales'] += companySalesObj[i]['discountedSales'];
                companySalesObj[cmpySaleslength - 1]['taxTotal'] += companySalesObj[i]['taxTotal'];
            }
        }
        accountBalanceObj['totals'] = accountBalanceObj['deposit'] + accountBalanceObj['depositOnline'] +
            accountBalanceObj['prepayment'] + accountBalanceObj['receivedOnAccount'];
        for (var i = 0; i < paymentRecords.length; i++) {
            for (var j = 0; j < paymentsData.length; j++) {
                if (paymentRecords[i]['paymentType'] === paymentsData[j]['paymentType']) {
                    paymentsData[j]['number'] += paymentRecords[i]['countId'];
                    paymentsData[j]['amountPaid'] += paymentRecords[i]['amount'];
                    numberTotal += paymentRecords[i]['countId'];
                    amountTotal += paymentRecords[i]['amount'];
                }
            }
        }
        var totalElectronicNumber = 0;
        var totalElectronicAmount = 0;
        for (var i = 0; i < electronicRefunds.length; i++) {
            if (electronicRefunds[i]['Process_Electronically__c']) {
                totalElectronicNumber += electronicRefunds[i]['countId'];
                totalElectronicAmount += electronicRefunds[i]['amount'];
            }
        }
        numberTotal += totalElectronicNumber;
        amountTotal += totalElectronicAmount;
        paymentsData.push({
            'paymentType': 'Electronic Payment Refunds',
            'electronic': 0,
            'number': totalElectronicNumber,
            'amountPaid': totalElectronicAmount
        });
        paymentsData.push({
            'paymentType': 'Payment Total',
            'electronic': 0,
            'number': numberTotal,
            'amountPaid': amountTotal
        });
        paymentsData.push(changeBackData);
        paymentsData.push(tipPOut);
        paymentsData.push(cashPaidData[0]);
        paymentsData.push(cashPaidData[1]);
        paymentsData.push({
            'paymentType': 'Total',
            'electronic': 0,
            'number': '',
            'amountPaid': amountTotal + changeBackData['amountPaid'] + tipPOut['amountPaid'] +
                cashPaidData[0]['amountPaid'] + cashPaidData[1]['amountPaid']
        });
        grandTotalObj[0]['amount'] = workerSalesObj[workerSalesObj.length - 1]['serviceSales'];
        grandTotalObj[0]['tax'] = companySalesObj[0]['taxTotal'] + companySalesObj[1]['taxTotal'];
        grandTotalObj[0]['amountTax'] = grandTotalObj[0]['amount'] + grandTotalObj[0]['tax'];
        grandTotalObj[1]['amount'] = workerSalesObj[workerSalesObj.length - 1]['productSales'];
        grandTotalObj[1]['tax'] = companySalesObj[2]['taxTotal'] + companySalesObj[3]['taxTotal'];
        grandTotalObj[1]['amountTax'] = grandTotalObj[1]['amount'] + grandTotalObj[1]['tax'];
        grandTotalObj[2]['amount'] = accountBalanceObj['totals'] + companySalesObj[4]['fullPrice']
            + companySalesObj[5]['fullPrice'] + companySalesObj[7]['fullPrice'] + companySalesObj[8]['fullPrice']
            + companySalesObj[6]['discountedSales'] + companySalesObj[9]['discountedSales']
            + companySalesObj[10]['fullPrice'];
        grandTotalObj[2]['amountTax'] = grandTotalObj[2]['amount'];
        grandTotalObj[3]['amount'] = workerSalesObj[workerSalesObj.length - 1]['totalTips'] + tipPOut['amountPaid'];
        grandTotalObj[3]['amountTax'] = grandTotalObj[3]['amount'];
        grandTotalObj[4]['amount'] = cashPaidData[0]['amountPaid'];
        grandTotalObj[4]['amountTax'] = grandTotalObj[4]['amount'];
        grandTotalObj[5]['amount'] = cashPaidData[1]['amountPaid'];
        grandTotalObj[5]['amountTax'] = grandTotalObj[5]['amount'];
        grandTotalObj[6]['amount'] = totalElectronicAmount;
        grandTotalObj[6]['amountTax'] = grandTotalObj[6]['amount'];
        grandTotalObj[7]['amount'] = grandTotalObj.reduce(function (sum, gtObj) { return sum + gtObj['amount']; }, 0);
        grandTotalObj[7]['tax'] = grandTotalObj.reduce(function (sum, gtObj) { return sum + gtObj['tax']; }, 0);
        grandTotalObj[7]['amountTax'] = grandTotalObj[7]['amount'] + grandTotalObj[7]['tax'];
        grandTotalObj[8]['amount'] = tipPOut['amountPaid'];
        grandTotalObj[8]['amountTax'] = grandTotalObj[8]['amount'];
        index2++;
        finalResponse(index2, workerSalesObj, companySalesObj, accountBalanceObj, paymentsData, grandTotalObj, done);
    }
}

function checkWorker(workerSalesObj, wrkId) {
    for (var i = 0; i < workerSalesObj.length; i++) {
        if (workerSalesObj[i]['workerId'] === wrkId) {
            return [true, i];
        }
    }
    return [false, 0];
}
function sendTicketDetails(index, cashInOutdata, apptData, done) {
    if (index === 2) {
        done(null, {
            'cashInOutdata': cashInOutdata,
            'apptData': apptData
        });
    }
}
function sendDailyCashDetails(index, paymentObj, tipsObj, cashInOutData, cashDrawerData, drawerData, balanceDueAmountPerAppt, done) {
    if (index === 2) {
        done(null, {
            'paymentObjdata': paymentObj,
            'tipsObjData': tipsObj,
            'cashInOutData': cashInOutData,
            'cashDrawerData': cashDrawerData,
            'balanceDueAmountPerAppt': balanceDueAmountPerAppt,
            'drawerData': drawerData
        });
    }
}
function sendSalesReportDetails(index, serviceSalesObj, serviceSalesRefundObj, done) {
    if (index === 2) {
        done(null, {
            'serviceSalesObj': serviceSalesObj,
            'serviceSalesRefundObj': serviceSalesRefundObj
        });
    }
}
function getRunForDates(startDate, endDate, dbName, callback) {
    var sqlQuery = `select Id, Worker__c, Begin_Date__c, End_Date__c
                , Compensation_Name__c, Hourly_Wage__c, Salary__c,
                Regular_Hours__c, Overtime_Hours__c, Days_Worked__c
                , Tip_Amount__c, Extra_Pay__c, Deduction__c,
                Steps__c, Compensation_Total__c
            FROM Compensation_Run__c where
            Begin_Date__c = '` + startDate + `' and End_Date__c = '` + endDate + `' AND IsDeleted = 0`;
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getServiceSalesByServiceGroup(startDate, endDate, dbName, callback) {
    var sqlQuery = `select ts.Worker__c workerId, s.Service_Group__c serviceGroupName,
                sum(ts.Net_Price__c) serviceAmount, count(ts.Id) numberOfServices, sum(ts.Guest_Charge__c) guestCharge
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c
            LEFT JOIN Service__c as s on s.Id = ts.Service__c
            where a.isTicket__c = 1
            and ts.Is_Booked_Out__c = 0 and ts.Is_Class__c = 0
            and DATE(ts.Service_Date_Time__c) >= '` + startDate + `'
            and DATE(ts.Service_Date_Time__c) <= '` + endDate + `'
            and ts.Do_Not_Deduct_From_Worker__c = 0
            group by ts.Worker__c, s.Service_Group__c`;
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function sendPrsCompnRpt(indexParm, workerData, RunForDates, CompensationMethods, tipAmount, regularHours, done) {
    var finalObj = [];
    if (indexParm === 4) {
        var totalCompensation = 0;
        for (var i = 0; i < workerData.length; i++) {
            var runfilDat = RunForDates.filter(function (a) { return a['Worker__c'] === workerData[i].Id });
            var workerObj = {
                'include': 1,
                'workerId': workerData[i].Id,
                'workerName': workerData[i].FullName,
                'hourlyWage': workerData[i].Hourly_Wage__c,
                'salary': workerData[i].Salary__c,
                'regularHours': '',
                'overtimeHours': 0,
                'daysWorked': null,
                'extraPay': 0.00,
                'deduction': 0.00,
                'compensationId': workerData[i].Compensation__c,
                'compensationName': '',
                'compensationAmount': 0,
                'tipAmount': '',
                'id': ''
            };
            if (runfilDat.length === 0) {
                var tempComp = CompensationMethods.filter(function (a) { return a['Id'] === workerData[i].Compensation__c });
                if (tempComp.length > 0) {
                    workerObj['compensationName'] = tempComp[0]['Name'];
                }
                var tempRgHrsAry = regularHours.filter(function (a) { return a['Worker__c'] === workerData[i].Id });
                workerObj['daysWorked'] = tempRgHrsAry.length;
                var tempRgHrs = 0;
                for (var j = 0; j < tempRgHrsAry.length; j++) {
                    tempRgHrs += tempRgHrsAry[j]['Hours_Worked__c'];
                }
                workerObj['regularHours'] = tempRgHrs;
                var tempTipsAry = tipAmount.filter(function (a) { return a['workerId'] === workerData[i].Id });
                if (tempTipsAry && tempTipsAry.length > 0) {
                    workerObj['tipAmount'] = tempTipsAry[0]['tips'];
                }
            } else {
                workerObj['id'] = runfilDat[0]['Id'];
                workerObj['hourlyWage'] = runfilDat[0]['Hourly_Wage__c'];
                workerObj['salary'] = runfilDat[0]['Salary__c'];
                // workerObj['compensationId'] = runfilDat[0]['Id'];
                workerObj['compensationName'] = runfilDat[0]['Compensation_Name__c'];
                workerObj['regularHours'] = runfilDat[0]['Regular_Hours__c'];
                workerObj['overtimeHours'] = runfilDat[0]['Overtime_Hours__c'];
                if (runfilDat[0]['Days_Worked__c']) {
                    workerObj['daysWorked'] = runfilDat[0]['Days_Worked__c'];
                }
                workerObj['extraPay'] = runfilDat[0]['Extra_Pay__c'];
                workerObj['deduction'] = runfilDat[0]['Deduction__c'];
                workerObj['compensationAmount'] = runfilDat[0]['Compensation_Total__c'];
                workerObj['tipAmount'] = runfilDat[0]['Tip_Amount__c'];
                totalCompensation += runfilDat[0]['Compensation_Total__c'];
            }

            finalObj.push(workerObj);
        }
        done(null, [finalObj, totalCompensation]);
    }
}
function getProductSalesByProductLine(dbName, callback) {
    var sqlQuery = `select tp.Worker__c workerId, pl.Name productLineName,
                sum(tp.Qty_Sold__c * tp.Net_Price__c) productAmount, sum(tp.Qty_Sold__c) numberOfProducts
            from Ticket_Product__c tp
            LEFT JOIN Appt_Ticket__c as a on a.Id = tp.Appt_Ticket__c
            LEFT JOIN Product__c p on p.Id = tp.Product__c
            LEFT JOIN Product_Line__c pl on pl.Id = p.Product_Line__c
            where a.isTicket__c = 1
            and DATE(a.Appt_Date_Time__c) >= '` + startDate + `'
            and DATE(a.Appt_Date_Time__c) <= '` + endDate + `'
            and tp.Do_Not_Deduct_From_Worker__c = 0
            group by tp.Worker__c, pl.Name`;
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getAllCompensationMethods(dbName, callback) {
    var sqlQuery = `select Id, Name, Active__c, Steps__c
            from Compensation__c where isScale__c = 0 AND IsDeleted = 0 order by Name limit 1000`;
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function retrieveWorkerTimeClockHours(startDate, endDate, workerId, dbName, callback) {
    var sqlQuery = `SELECT Id, Worker__c, Name, Time_In__c, Time_Out__c,
                IF(Time_Out__c = null, null, time_to_sec(timediff(Time_Out__c, Time_In__c)) / 3600)
            Hours_Worked__c, Worker__c, Worker__c
            FROM Time_Clock_Entry__c
            WHERE Time_In__c IS NOT null AND Time_Out__c IS NOT null
            AND DATE(Time_In__c) >= '` + startDate + `'
            AND DATE(Time_In__c) <= '` + endDate + `'`
    if (workerId) {
        sqlQuery += ` AND Worker__c IN` + workerId + ``
    }
    sqlQuery += ` AND IsDeleted = 0 ORDER BY Worker__c, Time_In__c`;
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getNewClientServiceTicketsByDateRange(startDate, endDate, workerId, retentionType, dbName, callback) {
    if (retentionType === 'New') {
        var sqlGoalQuery = `select ts.Id, ts.Worker__c, ts.Appt_Ticket__c ticket,
                a.Appt_Date_Time__c  apptDate,
                    c.Id clientId, ts.Worker__c worker, a.New_Client__c newClient
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
            LEFT JOIN Contact__c c on c.Id = ts.Client__c
            JOIN (select at.Client__c from Appt_Ticket__c at 
                left join Ticket_Service__c tis on at.Id = tis.Appt_Ticket__c 
                where
                at.Status__c = 'Complete' and
                DATE(Appt_Date_Time__c) >= '` + startDate.split(' ')[0] + `'
            and DATE(Appt_Date_Time__c) <= '` + endDate.split(' ')[0] + `'
                and at.New_Client__c = 1) as clt on clt.Client__c = a.Client__c
            where DATE(a.Appt_Date_Time__c) >= '` + startDate.split(' ')[0] + `'
            and DATE(a.Appt_Date_Time__c) <= '` + endDate.split(' ')[0] + `'
            and c.System_Client__c = 0
            and a.Status__c = 'Complete'`
        if (workerId) {
            sqlGoalQuery += ` and ts.Worker__c = '` + workerId + `'`
        }
        sqlGoalQuery += ` and ts.IsDeleted = 0
            and c.IsDeleted = 0
            GROUP BY ts.Id, ts.Appt_Ticket__c, a.Appt_Date_Time__c,
                c.Id, ts.Worker__c, a.New_Client__c`;
    } else {
        var sqlGoalQuery = `select ts.Id, ts.Appt_Ticket__c ticket,
                a.Appt_Date_Time__c apptDate,
                    c.Id clientId, ts.Worker__c worker
            from Ticket_Service__c ts
            LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
            LEFT JOIN Contact__c c on c.Id = ts.Client__c
            where DATE(a.Appt_Date_Time__c) >= '` + startDate.split(' ')[0] + `'
            and DATE(a.Appt_Date_Time__c) <= '` + endDate.split(' ')[0] + `'
            and a.Status__c = 'Complete' and c.System_Client__c = 0`
        if (workerId) {
            sqlGoalQuery += ` and ts.Worker__c = '` + workerId + `'`
        }
        sqlGoalQuery += ` and ts.IsDeleted = 0
            and c.IsDeleted = 0
            GROUP BY ts.Id, ts.Appt_Ticket__c, DATE(a.Appt_Date_Time__c), c.Id, ts.Worker__c`;

    }
    execute.query(dbName, sqlGoalQuery, '', function (err, result) {
        if (err) {
            logger.error('Error in Reports dao - getNewClientServiceTicketsByDateRange:', err);
            callback(err, { statusCode: '9999' });
        } else {
            callback(err, result);
        }
    });
}
function getNewClientTicketsByClientIds(startDate, clientIds, retentionType, dbName, callback) {
    if (clientIds) {
        var sqlQuery = `select a.Id, a.Appt_Date_Time__c, a.Client__c, c.Id,
                CONCAT(c.FirstName, ' ', c.LastName) clientName
            from Appt_Ticket__c a
            LEFT JOIN Contact__c c on c.Id = a.Client__c and c.IsDeleted = 0
            where  a.Status__c = 'Complete'
            and c.Id in `+ clientIds + `
            and a.Service_Sales__c > 0 `
        if (retentionType === 'New') {
            sqlQuery += ` and DATE(a.Appt_Date_Time__c) >= '` + startDate.split(' ')[0] + `'`
        }
        sqlQuery += ` order by a.Appt_Date_Time__c Asc`;
        execute.query(dbName, sqlQuery, function (error, result) {
            if (error) {
                logger.error('Error in getting getWorkerDetail: ', error);
                callback(error, result);
            } else {
                callback(error, result);
            }
        });
    } else {
        callback(null, []);
    }

}
function getTipsByWorker(startDate, endDate, dbName, callback) {
    var sqlQuery = `select t.Worker__c workerId, sum(t.Tip_Amount__c) tips
            from Ticket_Tip__c as t
            LEFT JOIN User__c as u on u.Id = t.Worker__c
            LEFT JOIN Appt_Ticket__c as a on a.Id = t.Appt_Ticket__c
            where date(a.Appt_Date_Time__c) >= '` + startDate + `'
            and date(a.Appt_Date_Time__c) <= '` + endDate + `'
            and a.IsTicket__c = 1 and a.Status__c = 'Complete'
            AND t.IsDeleted = 0 group by u.Id`;
    execute.query(dbName, sqlQuery, function (error, result) {
        if (error) {
            logger.error('Error in getting getWorkerDetail: ', error);
            callback(error, result);
        } else {
            callback(error, result);
        }
    });
}
function getDates(startDate, stopDate) {
    var dateArray = [];

    moment.suppressDeprecationWarnings = true;
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push(moment(currentDate).format('YYYY-MM-DD'))
        dateArray.push(moment(currentDate).format('MM/DD/YYYY'))
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}
function timeConversionToDate(time) {
    let date;
    let hours;
    const timeInDate = new Date();
    let minutes = time.split(' ')[0].split(':')[1];
    if (time.split(' ')[1] === 'AM') {
        hours = time.split(' ')[0].split(':')[0];
        if (+hours === 12) {
            hours = 0;
        }
    } else if (time.split(' ')[1] === 'PM') {
        hours = time.split(' ')[0].split(':')[0];
        if (parseInt(hours, 10) !== 12) {
            hours = parseInt(hours, 10) + 12;
        }
    }
    minutes = parseInt(minutes, 10);
    date = new Date(timeInDate.getFullYear(), timeInDate.getMonth(), timeInDate.getDate(), hours, minutes);
    return date;
}
function calculateDurations(inTime, outTime) {
    const hours = (outTime.getTime() - inTime.getTime()) / (1000 * 60 * 60);
    return +hours;
}
function calculateAverage(firstValue, secondValue) {
    if (secondValue == 0)
        return 0;

    try {
        return firstValue / secondValue;
    } catch (err) {
        return 0;
    }
}
function getWeekStartDay(val) {
    switch (val) {
        case 0: return 'SundayStartTime__c'
            break;
        case 1: return 'MondayStartTime__c'
            break;
        case 2: return 'TuesdayStartTime__c'
            break;
        case 3: return 'WednesdayStartTime__c'
            break;
        case 4: return 'ThursdayStartTime__c'
            break;
        case 5: return 'FridayStartTime__c'
            break;
        case 6: return 'SaturdayStartTime__c'
            break;
    }
}
function getWeekEndDay(val) {
    switch (val) {
        case 0: return 'SundayEndTime__c'
            break;
        case 1: return 'MondayEndTime__c'
            break;
        case 2: return 'TuesdayEndTime__c'
            break;
        case 3: return 'WednesdayEndTime__c'
            break;
        case 4: return 'ThursdayEndTime__c'
            break;
        case 5: return 'FridayEndTime__c'
            break;
        case 6: return 'SaturdayEndTime__c'
            break;
    }
}

function calculatePercentChange(firstValue, secondValue) {
    if (secondValue == 0 || (firstValue == 0 && secondValue == 0))
        return 0;

    try {
        return ((firstValue - secondValue) / secondValue) * 100;
    } catch (err) {
        return 0;
    }
}

/** */
function getServicesByReportDate(qry, db, callback) {
    execute.query(db, qry, '', function (err, result) {
        if (err) {
            logger.error('Error in Reports dao - getActivityComparisonReport:', err);
            callback(err, { statusCode: '9999' });
        } else {
            callback(err, result);
        }
    });
}
function generateAcitvityReportValues(list, searchDate, countField, salesField, productQtyField) {
    var countArray = [];
    var salesArray = [];
    var productNumberArray = [];
    var todayCount = 0;
    var wtdCount = 0;
    var mtdCount = 0;
    var mtdLastYearCount = 0;
    var qtdCount = 0;
    var qtdLastYearCount = 0;
    var ytdCount = 0;
    var ytdLastYearCount = 0;
    var prevCalendarYearCount = 0;
    var prev12MonthsCount = 0;
    var last30DaysCount = 0;
    var last90DaysCount = 0;
    var todaySales = 0;
    var wtdSales = 0;
    var mtdSales = 0;
    var mtdLastYearSales = 0;
    var qtdSales = 0;
    var qtdLastYearSales = 0;
    var ytdSales = 0;
    var ytdLastYearSales = 0;
    var prevCalendarYearSales = 0;
    var prev12MonthsSales = 0;
    var last30DaysSales = 0;
    var last90DaysSales = 0;
    var todayQty = 0;
    var wtdQty = 0;
    var mtdQty = 0;
    var mtdLastYearQty = 0;
    var qtdQty = 0;
    var qtdLastYearQty = 0;
    var ytdQty = 0;
    var ytdLastYearQty = 0;
    var prevCalendarYearQty = 0;
    var prev12MonthsQty = 0;
    var last30DaysQty = 0;
    var last90DaysQty = 0;
    moment.suppressDeprecationWarnings = true;
    var weekToDate = moment(searchDate).startOf('week').format('YYYY-MM-DD');
    var monthToDate = moment(searchDate).startOf('month').format('YYYY-MM-DD');
    todayDate = moment(searchDate).format('YYYY-MM-DD');
    var day = parseInt(todayDate.split('-')[2]);
    var month = parseInt(todayDate.split('-')[1]);
    var year = parseInt(todayDate.split('-')[0]);
    var lastYear = parseInt(year) - 1;
    var quarterToDate;
    if (month === 1 || month === 2 || month === 3)
        quarterToDate = year + '-' + 1 + '-' + 1;
    else if (month === 4 || month === 5 || month === 6)
        quarterToDate = year + '-' + 4 + '-' + 1;
    else if (month === 7 || month === 8 || month === 9)
        quarterToDate = year + '-' + 7 + '-' + 1;
    else
        quarterToDate = new Date(year + '-' + 10 + '-' + 1);
    yearToDate = year + '-' + 1 + '-' + 1;
    mtdLastYearStartDate = lastYear + '-' + month + '-' + 1;
    mtdLastYearEndDate = lastYear + '-' + month + '-' + day;
    qtdLastYearStartDate = lastYear + '-' + (new Date(quarterToDate).getMonth() + 1) + '-' + 1;
    qtdLastYearEndDate = lastYear + '-' + month + '-' + day;
    ytdLastYearStartDate = lastYear + '-' + 1 + '-' + 1;
    ytdLastYearEndDate = lastYear + '-' + month + '-' + day;
    prevCalendarYearStartDate = lastYear + '-' + 1 + '-' + 1;
    prevCalendarYearEndDate = lastYear + '-' + 12 + '-' + 31;
    prior12MonthsStartDate = moment(searchDate).add(-12, 'months').format('YYYY-MM-DD');
    prior12MonthsEndDate = searchDate;
    last30DaysStartDate = moment(searchDate).add(-30, 'days').format('YYYY-MM-DD');
    last30DaysEndDate = searchDate;
    last90DaysStartDate = moment(searchDate).add(-90, 'days').format('YYYY-MM-DD');
    last90DaysEndDate = searchDate;
    var todayCountSet = [];
    var wtdCountSet = [];
    var mtdCountSet = [];
    var qtdCountSet = [];
    var ytdCountSet = [];
    var mtdLastYearCountSet = [];
    var qtdLastYearCountSet = [];
    var ytdLastYearCountSet = [];
    var prevCalendarYearCountSet = [];
    var prev12MonthsCountSet = [];
    var last30DaysCountSet = [];
    var last90DaysCountSet = [];
    var numberActivityList = [];
    var amountActivityList = [];
    var aline = {};
    subtractGuestCharge = true;
    list.map((obj) => {
        var reportDate = obj[countField];
        reportDate = moment(reportDate).format('YYYY-MM-DD');
        var sales;
        var qty;
        salesField = 'serviceTotal'
        if (salesField != null)
            sales = obj[salesField];
        if (subtractGuestCharge) {
            guestCharge = obj['guestCharge'];
            if (guestCharge != null)
                sales -= guestCharge;
        }
        if (productQtyField != null)
            qty = obj[productQtyField];
        if (sales == null)
            sales = 0;
        if (qty == null)
            qty = 0;
        if (reportDate === todayDate) {
            todayCount++;
            if (salesField != null)
                todaySales += sales;
            if (productQtyField != null)
                todayQty += qty;
        }
        if (reportDate >= weekToDate) {
            wtdCount++;
            if (salesField != null)
                wtdSales += sales;
            if (productQtyField != null)
                wtdQty += qty;
        }
        if (reportDate >= monthToDate) {
            mtdCount++;
            if (salesField != null)
                mtdSales += sales;
            if (productQtyField != null)
                mtdQty += qty;
        }
        if (reportDate >= quarterToDate) {
            qtdCount++;
            if (salesField != null)
                qtdSales += sales;
            if (productQtyField != null)
                qtdQty += qty;
        }
        if (reportDate >= yearToDate) {
            ytdCount++;
            if (salesField != null)
                ytdSales += sales;
            if (productQtyField != null)
                ytdQty += qty;
        }
        if (reportDate >= mtdLastYearStartDate && reportDate <= mtdLastYearEndDate) {
            mtdLastYearCount++;
            if (salesField != null)
                mtdLastYearSales += sales;
            if (productQtyField != null)
                mtdLastYearQty += qty;
        }
        if (reportDate >= qtdLastYearStartDate && reportDate <= qtdLastYearEndDate) {
            qtdLastYearCount++;
            if (salesField != null)
                qtdLastYearSales += sales;
            if (productQtyField != null)
                qtdLastYearQty += qty;
        }
        if (reportDate >= ytdLastYearStartDate && reportDate <= ytdLastYearEndDate) {
            ytdLastYearCount++;
            if (salesField != null)
                ytdLastYearSales += sales;
            if (productQtyField != null)
                ytdLastYearQty += qty;
        }
        if (reportDate >= prevCalendarYearStartDate && reportDate <= prevCalendarYearEndDate) {
            prevCalendarYearCount++;
            if (salesField != null)
                prevCalendarYearSales += sales;
            if (productQtyField != null)
                prevCalendarYearQty += qty;
        }
        if (reportDate >= prior12MonthsStartDate && reportDate <= prior12MonthsEndDate) {
            prev12MonthsCount++;
            if (salesField != null)
                prev12MonthsSales += sales;
            if (productQtyField != null)
                prev12MonthsQty += qty;
        }
        if (reportDate >= last30DaysStartDate && reportDate <= last30DaysEndDate) {
            last30DaysCount++;
            if (salesField != null)
                last30DaysSales += sales;
            if (productQtyField != null)
                last30DaysQty += qty;
        }
        if (reportDate >= last90DaysStartDate && reportDate <= last90DaysEndDate) {
            last90DaysCount++;
            if (salesField != null)
                last90DaysSales += sales;
            if (productQtyField != null)
                last90DaysQty += qty;
        }
    })
    countArray.push(todayCount, wtdCount, mtdCount, mtdLastYearCount, qtdCount, qtdLastYearCount, ytdCount, ytdLastYearCount, prevCalendarYearCount,
        prev12MonthsCount, last30DaysCount, last90DaysCount)
    if (salesField != null) {
        salesArray.push(todaySales, wtdSales, mtdSales, mtdLastYearSales, qtdSales, qtdLastYearSales, ytdSales, ytdLastYearSales,
            prevCalendarYearSales, prev12MonthsSales, last30DaysSales, last90DaysSales);
    }
    if (productQtyField != null) {
        productNumberArray.push(todayQty, wtdQty, mtdQty, mtdLastYearQty, qtdQty, qtdLastYearQty, ytdQty, ytdLastYearQty,
            prevCalendarYearQty, prev12MonthsQty, last30DaysQty, last90DaysQty);
    }
    return { countArray: countArray, salesArray: salesArray, productNumberArray: productNumberArray };
}
function populateActivityLine(aline, isCount, isQTY, countArray, productNumberArray, salesArray) {
    if (isQTY) {
        aline.todayDate = productNumberArray[0]// todayQty;
        aline.weekToDate = productNumberArray[1]// wtdQty;
        aline.monthToDate = productNumberArray[2]// mtdQty;
        aline.monthToDateLastYear = productNumberArray[3]// mtdLastYearQty;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = productNumberArray[4]// qtdQty;
        aline.quarterToDateLastYear = productNumberArray[5]// qtdLastYearQty;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = productNumberArray[6]// ytdQty;
        aline.yearToDateLastYear = productNumberArray[7]// ytdLastYearQty;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = productNumberArray[8]// prevCalendarYearQty;
        aline.priorTwelveMonths = productNumberArray[9]// prev12MonthsQty;
        aline.last30Days = productNumberArray[10]// last 30 days Qty
        aline.last90Days = productNumberArray[11]// last 90 days Qty
    } else if (isCount) {
        aline.todayDate = countArray[0];
        aline.weekToDate = countArray[1];
        aline.monthToDate = countArray[2];
        aline.monthToDateLastYear = countArray[3];
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = countArray[4];
        aline.quarterToDateLastYear = countArray[5];
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = countArray[6];
        aline.yearToDateLastYear = countArray[7];
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = countArray[8];
        aline.priorTwelveMonths = countArray[9];
        aline.last30Days = countArray[10];
        aline.last90Days = countArray[11];
    } else {
        aline.todayDate = salesArray[0];
        aline.weekToDate = salesArray[1];
        aline.monthToDate = salesArray[2];
        aline.monthToDateLastYear = salesArray[3];
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = salesArray[4];
        aline.quarterToDateLastYear = salesArray[5];
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = salesArray[6];
        aline.yearToDateLastYear = salesArray[7];
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = salesArray[8];
        aline.priorTwelveMonths = salesArray[9];
        aline.last30Days = salesArray[10];
        aline.last90Days = salesArray[11];
    }
    return aline;
}
function populateTicketRatingList(tcktRtngData, searchDate, workerId, numberActivityList) {
    var ticketRatingList = [];
    if (tcktRtngData && tcktRtngData.length > 0) {
        // GOOD TICKET RATINGS COUNT
        var goodtodayCount = 0;
        var goodwtdCount = 0;
        var goodmtdCount = 0;
        var goodmtdLastYearCount = 0;
        var goodqtdCount = 0;
        var goodqtdLastYearCount = 0;
        var goodytdCount = 0;
        var goodytdLastYearCount = 0;
        var goodprevCalendarYearCount = 0;
        var goodprev12MonthsCount = 0;
        var goodlast30DaysCount = 0;
        var goodlast90DaysCount = 0;
        // FAIR TICKET RATINGS COUNT
        var fairtodayCount = 0;
        var fairwtdCount = 0;
        var fairmtdCount = 0;
        var fairmtdLastYearCount = 0;
        var fairqtdCount = 0;
        var fairqtdLastYearCount = 0;
        var fairytdCount = 0;
        var fairytdLastYearCount = 0;
        var fairprevCalendarYearCount = 0;
        var fairprev12MonthsCount = 0;
        var fairlast30DaysCount = 0;
        var fairlast90DaysCount = 0;
        // POOR TICKET RATINGS COUNT
        var poortodayCount = 0;
        var poorwtdCount = 0;
        var poormtdCount = 0;
        var poormtdLastYearCount = 0;
        var poorqtdCount = 0;
        var poorqtdLastYearCount = 0;
        var poorytdCount = 0;
        var poorytdLastYearCount = 0;
        var poorprevCalendarYearCount = 0;
        var poorprev12MonthsCount = 0;
        var poorlast30DaysCount = 0;
        var poorlast90DaysCount = 0;
        // UNSPECIFIED TICKET RATINGS COUNT
        var unspecifiedtodayCount = 0;
        var unspecifiedwtdCount = 0;
        var unspecifiedmtdCount = 0;
        var unspecifiedmtdLastYearCount = 0;
        var unspecifiedqtdCount = 0;
        var unspecifiedqtdLastYearCount = 0;
        var unspecifiedytdCount = 0;
        var unspecifiedytdLastYearCount = 0;
        var unspecifiedprevCalendarYearCount = 0;
        var unspecifiedprev12MonthsCount = 0;
        var unspecifiedlast30DaysCount = 0;
        var unspecifiedlast90DaysCount = 0;
        var weekToDate = moment(searchDate).startOf('week').format('YYYY-MM-DD');
        var monthToDate = moment(searchDate).startOf('month').format('YYYY-MM-DD');
        todayDate = moment(searchDate).format('YYYY-MM-DD');
        var day = parseInt(todayDate.split('-')[2]);
        var month = parseInt(todayDate.split('-')[1]);
        var year = parseInt(todayDate.split('-')[0]);
        var lastYear = parseInt(year) - 1;
        var quarterToDate;
        if (month === 1 || month === 2 || month === 3)
            quarterToDate = year + '-' + 1 + '-' + 1;
        else if (month === 4 || month === 5 || month === 6)
            quarterToDate = year + '-' + 4 + '-' + 1;
        else if (month === 7 || month === 8 || month === 9)
            quarterToDate = year + '-' + 7 + '-' + 1;
        else
            quarterToDate = new Date(year + '-' + 10 + '-' + 1);
        yearToDate = year + '-' + 1 + '-' + 1;
        mtdLastYearStartDate = lastYear + '-' + month + '-' + 1;
        mtdLastYearEndDate = lastYear + '-' + month + '-' + day;
        qtdLastYearStartDate = lastYear + '-' + (new Date(quarterToDate).getMonth() + 1) + '-' + 1;
        qtdLastYearEndDate = lastYear + '-' + month + '-' + day;
        ytdLastYearStartDate = lastYear + '-' + 1 + '-' + 1;
        ytdLastYearEndDate = lastYear + '-' + month + '-' + day;
        prevCalendarYearStartDate = lastYear + '-' + 1 + '-' + 1;
        prevCalendarYearEndDate = lastYear + '-' + 12 + '-' + 31;
        prior12MonthsStartDate = moment(searchDate).add(-12, 'months').format('YYYY-MM-DD');
        prior12MonthsEndDate = searchDate;
        last30DaysStartDate = moment(searchDate).add(-30, 'days').format('YYYY-MM-DD');
        last30DaysEndDate = searchDate;
        last90DaysStartDate = moment(searchDate).add(-90, 'days').format('YYYY-MM-DD');
        last90DaysEndDate = searchDate;
        if (!workerId || workerId === null) {
            // populateArrays(ticketList, 'apptDate', null, null);
            var reportData = generateAcitvityReportValues(tcktRtngData, searchDate, 'apptDate', null, null)
            countArray = reportData.countArray;
            productNumberArray = reportData.productNumberArray;
            salesArray = reportData.salesArray;
            tcktRtngData.map((obj) => {
                var reportDate = obj.apptDate;
                var ticketRating = obj.ticketRating;
                //	only want to count one appointment ticket for each service
                //	if an appointment contains many services, it counts as 1
                if (reportDate == todayDate) {
                    if (ticketRating == 'Good')
                        goodtodayCount++;
                    else if (ticketRating == 'Fair')
                        fairtodayCount++;
                    else if (ticketRating == 'Poor')
                        poortodayCount++;
                    else
                        unspecifiedtodayCount++;
                }
                if (reportDate >= weekToDate) {
                    if (ticketRating == 'Good')
                        goodwtdCount++;
                    else if (ticketRating == 'Fair')
                        fairwtdCount++;
                    else if (ticketRating == 'Poor')
                        poorwtdCount++;
                    else
                        unspecifiedwtdCount++;
                }
                if (reportDate >= monthToDate) {
                    if (ticketRating == 'Good')
                        goodmtdCount++;
                    else if (ticketRating == 'Fair')
                        fairmtdCount++;
                    else if (ticketRating == 'Poor')
                        poormtdCount++;
                    else
                        unspecifiedmtdCount++;
                }
                if (reportDate >= quarterToDate) {
                    if (ticketRating == 'Good')
                        goodqtdCount++;
                    else if (ticketRating == 'Fair')
                        fairqtdCount++;
                    else if (ticketRating == 'Poor')
                        poorqtdCount++;
                    else
                        unspecifiedqtdCount++;
                }
                if (reportDate >= yearToDate) {
                    if (ticketRating == 'Good')
                        goodytdCount++;
                    else if (ticketRating == 'Fair')
                        fairytdCount++;
                    else if (ticketRating == 'Poor')
                        poorytdCount++;
                    else
                        unspecifiedytdCount++;
                }
                if (reportDate >= mtdLastYearStartDate && reportDate <= mtdLastYearEndDate) {
                    if (ticketRating == 'Good')
                        goodmtdLastYearCount++;
                    else if (ticketRating == 'Fair')
                        fairmtdLastYearCount++;
                    else if (ticketRating == 'Poor')
                        poormtdLastYearCount++;
                    else
                        unspecifiedmtdLastYearCount++;
                }
                if (reportDate >= qtdLastYearStartDate && reportDate <= qtdLastYearEndDate) {
                    if (ticketRating == 'Good')
                        goodqtdLastYearCount++;
                    else if (ticketRating == 'Fair')
                        fairqtdLastYearCount++;
                    else if (ticketRating == 'Poor')
                        poorqtdLastYearCount++;
                    else
                        unspecifiedqtdLastYearCount++;
                }
                if (reportDate >= ytdLastYearStartDate && reportDate <= ytdLastYearEndDate) {
                    if (ticketRating == 'Good')
                        goodytdLastYearCount++;
                    else if (ticketRating == 'Fair')
                        fairytdLastYearCount++;
                    else if (ticketRating == 'Poor')
                        poorytdLastYearCount++;
                    else
                        unspecifiedytdLastYearCount++;
                }
                if (reportDate >= prevCalendarYearStartDate && reportDate <= prevCalendarYearEndDate) {
                    if (ticketRating == 'Good')
                        goodprevCalendarYearCount++;
                    else if (ticketRating == 'Fair')
                        fairprevCalendarYearCount++;
                    else if (ticketRating == 'Poor')
                        poorprevCalendarYearCount++;
                    else
                        unspecifiedprevCalendarYearCount++;
                }
                if (reportDate >= prior12MonthsStartDate && reportDate <= prior12MonthsEndDate) {
                    if (ticketRating == 'Good')
                        goodprev12MonthsCount++;
                    else if (ticketRating == 'Fair')
                        fairprev12MonthsCount++;
                    else if (ticketRating == 'Poor')
                        poorprev12MonthsCount++;
                    else
                        unspecifiedprev12MonthsCount++;
                }
                if (reportDate >= last30DaysStartDate && reportDate <= last30DaysEndDate) {
                    if (ticketRating == 'Good')
                        goodlast30DaysCount++;
                    else if (ticketRating == 'Fair')
                        fairlast30DaysCount++;
                    else if (ticketRating == 'Poor')
                        poorlast30DaysCount++;
                    else
                        unspecifiedlast30DaysCount++;
                }
                if (reportDate >= last90DaysStartDate && reportDate <= last90DaysEndDate) {
                    if (ticketRating == 'Good')
                        goodlast90DaysCount++;
                    else if (ticketRating == 'Fair')
                        fairlast90DaysCount++;
                    else if (ticketRating == 'Poor')
                        poorlast90DaysCount++;
                    else
                        unspecifiedlast90DaysCount++;
                }
            })
        } else {
            var todayCountSet = [];
            var wtdCountSet = [];
            var mtdCountSet = [];
            var mtdLastYearCountSet = [];
            var qtdCountSet = [];
            var qtdLastYearCountSet = [];
            var ytdCountSet = [];
            var ytdLastYearCountSet = [];
            var prevCalendarYearCountSet = [];
            var prev12MonthsCountSet = [];
            var last30DaysCountSet = [];
            var last90DaysCountSet = [];
            var todayCount = 0;
            var wtdCount = 0;
            var mtdCount = 0;
            var mtdLastYearCount = 0;
            var qtdCount = 0;
            var qtdLastYearCount = 0;
            var ytdCount = 0;
            var ytdLastYearCount = 0;
            var prevCalendarYearCount = 0;
            var prev12MonthsCount = 0;
            var last30DaysCount = 0;
            var last90DaysCount = 0;
            tcktRtngData.map((obj) => {
                var ticketId = obj.ticket;
                var reportDate = obj.serviceDate;
                var ticketRating = ticketRating;
                //	only want to count one appointment ticket for each service
                //	if an appointment contains many services, it counts as 1
                if (reportDate == todayDate) {
                    if (!todayCountSet.filter((obj) => obj === ticketId).length > 0) {
                        todayCountSet.push(ticketId);
                        todayCount++;
                        if (ticketRating == 'Good')
                            goodtodayCount++;
                        else if (ticketRating == 'Fair')
                            fairtodayCount++;
                        else if (ticketRating == 'Poor')
                            poortodayCount++;
                        else
                            unspecifiedtodayCount++;
                    }
                }
                if (reportDate >= weekToDate) {
                    if (!wtdCountSet.filter((obj) => obj === ticketId).length > 0) {
                        wtdCountSet.push(ticketId);
                        wtdCount++;
                        if (ticketRating == 'Good')
                            goodwtdCount++;
                        else if (ticketRating == 'Fair')
                            fairwtdCount++;
                        else if (ticketRating == 'Poor')
                            poorwtdCount++;
                        else
                            unspecifiedwtdCount++;
                    }
                }
                if (reportDate >= monthToDate) {
                    if (!mtdCountSet.filter((obj) => obj === ticketId).length > 0) {
                        mtdCountSet.push(ticketId);
                        mtdCount++;
                        if (ticketRating == 'Good')
                            goodmtdCount++;
                        else if (ticketRating == 'Fair')
                            fairmtdCount++;
                        else if (ticketRating == 'Poor')
                            poormtdCount++;
                        else
                            unspecifiedmtdCount++;
                    }
                }
                if (reportDate >= quarterToDate) {
                    if (!qtdCountSet.filter((obj) => obj === ticketId).length > 0) {
                        qtdCountSet.push(ticketId);
                        qtdCount++;
                        if (ticketRating == 'Good')
                            goodqtdCount++;
                        else if (ticketRating == 'Fair')
                            fairqtdCount++;
                        else if (ticketRating == 'Poor')
                            poorqtdCount++;
                        else
                            unspecifiedqtdCount++;
                    }
                }
                if (reportDate >= yearToDate) {
                    if (!ytdCountSet.filter((obj) => obj === ticketId).length > 0) {
                        ytdCountSet.push(ticketId);
                        ytdCount++;
                        if (ticketRating == 'Good')
                            goodytdCount++;
                        else if (ticketRating == 'Fair')
                            fairytdCount++;
                        else if (ticketRating == 'Poor')
                            poorytdCount++;
                        else
                            unspecifiedytdCount++;
                    }
                }
                if (reportDate >= mtdLastYearStartDate && reportDate <= mtdLastYearEndDate) {
                    if (!mtdLastYearCountSet.filter((obj) => obj === ticketId).length > 0) {
                        mtdLastYearCountSet.push(ticketId);
                        mtdLastYearCount++;
                        if (ticketRating == 'Good')
                            goodmtdLastYearCount++;
                        else if (ticketRating == 'Fair')
                            fairmtdLastYearCount++;
                        else if (ticketRating == 'Poor')
                            poormtdLastYearCount++;
                        else
                            unspecifiedmtdLastYearCount++;
                    }
                }
                if (reportDate >= qtdLastYearStartDate && reportDate <= qtdLastYearEndDate) {
                    if (!qtdLastYearCountSet.filter((obj) => obj === ticketId).length > 0) {
                        qtdLastYearCountSet.push(ticketId);
                        qtdLastYearCount++;
                        if (ticketRating == 'Good')
                            goodqtdLastYearCount++;
                        else if (ticketRating == 'Fair')
                            fairqtdLastYearCount++;
                        else if (ticketRating == 'Poor')
                            poorqtdLastYearCount++;
                        else
                            unspecifiedqtdLastYearCount++;
                    }
                }
                if (reportDate >= ytdLastYearStartDate && reportDate <= ytdLastYearEndDate) {
                    if (!ytdLastYearCountSet.filter((obj) => obj === ticketId).length > 0) {
                        ytdLastYearCountSet.push(ticketId);
                        ytdLastYearCount++;
                        if (ticketRating == 'Good')
                            goodytdLastYearCount++;
                        else if (ticketRating == 'Fair')
                            fairytdLastYearCount++;
                        else if (ticketRating == 'Poor')
                            poorytdLastYearCount++;
                        else
                            unspecifiedytdLastYearCount++;
                    }
                }
                if (reportDate >= prevCalendarYearStartDate && reportDate <= prevCalendarYearEndDate) {
                    if (!prevCalendarYearCountSet.filter((obj) => obj === ticketId).length > 0) {
                        prevCalendarYearCountSet.push(ticketId);
                        prevCalendarYearCount++;
                        if (ticketRating == 'Good')
                            goodprevCalendarYearCount++;
                        else if (ticketRating == 'Fair')
                            fairprevCalendarYearCount++;
                        else if (ticketRating == 'Poor')
                            poorprevCalendarYearCount++;
                        else
                            unspecifiedprevCalendarYearCount++;
                    }
                }
                if (reportDate >= prior12MonthsStartDate && reportDate <= prior12MonthsEndDate) {
                    if (!prev12MonthsCountSet.filter((obj) => obj === ticketId).length > 0) {
                        prev12MonthsCountSet.push(ticketId);
                        prev12MonthsCount++;
                        if (ticketRating == 'Good')
                            goodprev12MonthsCount++;
                        else if (ticketRating == 'Fair')
                            fairprev12MonthsCount++;
                        else if (ticketRating == 'Poor')
                            poorprev12MonthsCount++;
                        else
                            unspecifiedprev12MonthsCount++;
                    }
                }
                if (reportDate >= last30DaysStartDate && reportDate <= last30DaysEndDate) {
                    if (!last30DaysCountSet.filter((obj) => obj === ticketId).length > 0) {
                        last30DaysCountSet.push(ticketId);
                        last30DaysCount++;
                        if (ticketRating == 'Good')
                            goodlast30DaysCount++;
                        else if (ticketRating == 'Fair')
                            fairlast30DaysCount++;
                        else if (ticketRating == 'Poor')
                            poorlast30DaysCount++;
                        else
                            unspecifiedlast30DaysCount++;
                    }
                }
                if (reportDate >= last90DaysStartDate && reportDate <= last90DaysEndDate) {
                    if (!last90DaysCountSet.filter((obj) => obj === ticketId).length > 0) {
                        last90DaysCountSet.push(ticketId);
                        last90DaysCount++;
                        if (ticketRating == 'Good')
                            goodlast90DaysCount++;
                        else if (ticketRating == 'Fair')
                            fairlast90DaysCount++;
                        else if (ticketRating == 'Poor')
                            poorlast90DaysCount++;
                        else
                            unspecifiedlast90DaysCount++;
                    }
                }
            })
        }
        var aline = {};
        aline.category = 'NumOf_ticketRating_Good';
        aline.todayDate = goodtodayCount;
        aline.weekToDate = goodwtdCount;
        aline.monthToDate = goodmtdCount;
        aline.monthToDateLastYear = goodmtdLastYearCount;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = goodqtdCount;
        aline.quarterToDateLastYear = goodqtdLastYearCount;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = goodytdCount;
        aline.yearToDateLastYear = goodytdLastYearCount;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = goodprevCalendarYearCount;
        aline.priorTwelveMonths = goodprev12MonthsCount;
        aline.last30Days = goodlast30DaysCount;
        aline.last90Days = goodlast90DaysCount;
        ticketRatingList.push(aline);
        //	Can use existing rows to calculate the percentage rows, no SOQL needed
        var totalTickets = numberActivityList[0];
        aline = {};
        aline.category = 'Perc_ticketRating_Good';
        aline.todayDate = calculateAverage(goodtodayCount, totalTickets.todayDate) * 100;
        aline.weekToDate = calculateAverage(goodwtdCount, totalTickets.weekToDate) * 100;
        aline.monthToDate = calculateAverage(goodmtdCount, totalTickets.monthToDate) * 100;
        aline.monthToDateLastYear = calculateAverage(goodmtdLastYearCount, totalTickets.monthToDateLastYear) * 100;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = calculateAverage(goodqtdCount, totalTickets.quarterToDate) * 100;
        aline.quarterToDateLastYear = calculateAverage(goodqtdLastYearCount, totalTickets.quarterToDateLastYear) * 100;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = calculateAverage(goodytdCount, totalTickets.yearToDate) * 100;
        aline.yearToDateLastYear = calculateAverage(goodytdLastYearCount, totalTickets.yearToDateLastYear) * 100;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = calculateAverage(goodprevCalendarYearCount, totalTickets.priorCalendarYear) * 100;
        aline.priorTwelveMonths = calculateAverage(goodprev12MonthsCount, totalTickets.priorTwelveMonths) * 100;
        aline.last30Days = 0;
        aline.last90Days = 0;
        ticketRatingList.push(aline);
        var aline = {};
        aline.category = 'NumOf_ticketRating_Fair';
        aline.todayDate = fairtodayCount;
        aline.weekToDate = fairwtdCount;
        aline.monthToDate = fairmtdCount;
        aline.monthToDateLastYear = fairmtdLastYearCount;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = fairqtdCount;
        aline.quarterToDateLastYear = fairqtdLastYearCount;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = fairytdCount;
        aline.yearToDateLastYear = fairytdLastYearCount;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = fairprevCalendarYearCount;
        aline.priorTwelveMonths = fairprev12MonthsCount;
        aline.last30Days = fairlast30DaysCount;
        aline.last90Days = fairlast90DaysCount;
        ticketRatingList.push(aline);
        var aline = {};
        aline.category = 'Perc_ticketRating_Fair';
        aline.todayDate = calculateAverage(fairtodayCount, totalTickets.todayDate) * 100;
        aline.weekToDate = calculateAverage(fairwtdCount, totalTickets.weekToDate) * 100;
        aline.monthToDate = calculateAverage(fairmtdCount, totalTickets.monthToDate) * 100;
        aline.monthToDateLastYear = calculateAverage(fairmtdLastYearCount, totalTickets.monthToDateLastYear) * 100;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = calculateAverage(fairqtdCount, totalTickets.quarterToDate) * 100;
        aline.quarterToDateLastYear = calculateAverage(fairqtdLastYearCount, totalTickets.quarterToDateLastYear) * 100;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = calculateAverage(fairytdCount, totalTickets.yearToDate) * 100;
        aline.yearToDateLastYear = calculateAverage(fairytdLastYearCount, totalTickets.yearToDateLastYear) * 100;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = calculateAverage(fairprevCalendarYearCount, totalTickets.priorCalendarYear) * 100;
        aline.priorTwelveMonths = calculateAverage(fairprev12MonthsCount, totalTickets.priorTwelveMonths) * 100;
        aline.last30Days = 0;
        aline.last90Days = 0;
        ticketRatingList.push(aline);
        var aline = {};
        aline.category = 'NumOf_ticketRating_Poor';
        aline.todayDate = poortodayCount;
        aline.weekToDate = poorwtdCount;
        aline.monthToDate = poormtdCount;
        aline.monthToDateLastYear = poormtdLastYearCount;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = poorqtdCount;
        aline.quarterToDateLastYear = poorqtdLastYearCount;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = poorytdCount;
        aline.yearToDateLastYear = poorytdLastYearCount;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = poorprevCalendarYearCount;
        aline.priorTwelveMonths = poorprev12MonthsCount;
        aline.last30Days = poorlast30DaysCount;
        aline.last90Days = poorlast90DaysCount;
        ticketRatingList.push(aline);
        var aline = {};
        aline.category = 'Perc_ticketRating_Poor';
        aline.todayDate = calculateAverage(poortodayCount, totalTickets.todayDate) * 100;
        aline.weekToDate = calculateAverage(poorwtdCount, totalTickets.weekToDate) * 100;
        aline.monthToDate = calculateAverage(poormtdCount, totalTickets.monthToDate) * 100;
        aline.monthToDateLastYear = calculateAverage(poormtdLastYearCount, totalTickets.monthToDateLastYear) * 100;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = calculateAverage(poorqtdCount, totalTickets.quarterToDate) * 100;
        aline.quarterToDateLastYear = calculateAverage(poorqtdLastYearCount, totalTickets.quarterToDateLastYear) * 100;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = calculateAverage(poorytdCount, totalTickets.yearToDate) * 100;
        aline.yearToDateLastYear = calculateAverage(poorytdLastYearCount, totalTickets.yearToDateLastYear) * 100;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = calculateAverage(poorprevCalendarYearCount, totalTickets.priorCalendarYear) * 100;
        aline.priorTwelveMonths = calculateAverage(poorprev12MonthsCount, totalTickets.priorTwelveMonths) * 100;
        aline.last30Days = 0;
        aline.last90Days = 0;
        ticketRatingList.push(aline);
        var aline = {};
        aline.category = 'NumOf_ticketRating_Unspecified';
        aline.todayDate = unspecifiedtodayCount;
        aline.weekToDate = unspecifiedwtdCount;
        aline.monthToDate = unspecifiedmtdCount;
        aline.monthToDateLastYear = unspecifiedmtdLastYearCount;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = unspecifiedqtdCount;
        aline.quarterToDateLastYear = unspecifiedqtdLastYearCount;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = unspecifiedytdCount;
        aline.yearToDateLastYear = unspecifiedytdLastYearCount;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = unspecifiedprevCalendarYearCount;
        aline.priorTwelveMonths = unspecifiedprev12MonthsCount;
        aline.last30Days = unspecifiedlast30DaysCount;
        aline.last90Days = unspecifiedlast90DaysCount;
        ticketRatingList.push(aline);
        var aline = {};
        aline.category = 'Perc_ticketRating_Unspecified';
        aline.todayDate = calculateAverage(unspecifiedtodayCount, totalTickets.todayDate) * 100;
        aline.weekToDate = calculateAverage(unspecifiedwtdCount, totalTickets.weekToDate) * 100;
        aline.monthToDate = calculateAverage(unspecifiedmtdCount, totalTickets.monthToDate) * 100;
        aline.monthToDateLastYear = calculateAverage(unspecifiedmtdLastYearCount, totalTickets.monthToDateLastYear) * 100;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = calculateAverage(unspecifiedqtdCount, totalTickets.quarterToDate) * 100;
        aline.quarterToDateLastYear = calculateAverage(unspecifiedqtdLastYearCount, totalTickets.quarterToDateLastYear) * 100;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = calculateAverage(unspecifiedytdCount, totalTickets.yearToDate) * 100;
        aline.yearToDateLastYear = calculateAverage(unspecifiedytdLastYearCount, totalTickets.yearToDateLastYear) * 100;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = calculateAverage(unspecifiedprevCalendarYearCount, totalTickets.priorCalendarYear) * 100;
        aline.priorTwelveMonths = calculateAverage(unspecifiedprev12MonthsCount, totalTickets.priorTwelveMonths) * 100;
        aline.last30Days = 0;
        aline.last90Days = 0;
        ticketRatingList.push(aline);
    }
    return ticketRatingList;
}
function populateAmountActivityList(amountActivityList, numberActivityList) {
    if (amountActivityList && amountActivityList.length > 0) {
        var todaySales = 0;
        var wtdSales = 0;
        var mtdSales = 0;
        var mtdLastYearSales = 0;
        var qtdSales = 0;
        var qtdLastYearSales = 0;
        var ytdSales = 0;
        var ytdLastYearSales = 0;
        var prevCalendarYearSales = 0;
        var prev12MonthsSales = 0;
        var last30DaysSales = 0;
        var last90DaysSales = 0;
        for (var i = 0; i < amountActivityList.length; i++) {
            todaySales += amountActivityList[i].todayDate;
            wtdSales += amountActivityList[i].weekToDate;
            mtdSales += amountActivityList[i].monthToDate;
            mtdLastYearSales += amountActivityList[i].monthToDateLastYear;
            qtdSales += amountActivityList[i].quarterToDate;
            qtdLastYearSales += amountActivityList[i].quarterToDateLastYear;
            ytdSales += amountActivityList[i].yearToDate;
            ytdLastYearSales += amountActivityList[i].yearToDateLastYear;
            prevCalendarYearSales += amountActivityList[i].priorCalendarYear;
            prev12MonthsSales += amountActivityList[i].priorTwelveMonths;
            last30DaysSales += amountActivityList[i].last30Days;
            last90DaysSales += amountActivityList[i].last90Days;
        }
        var aline = {};
        aline.todayDate = todaySales;
        aline.weekToDate = wtdSales;
        aline.monthToDate = mtdSales;
        aline.monthToDateLastYear = mtdLastYearSales;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = qtdSales;
        aline.quarterToDateLastYear = qtdLastYearSales;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = ytdSales;
        aline.yearToDateLastYear = ytdLastYearSales;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = prevCalendarYearSales;
        aline.priorTwelveMonths = prev12MonthsSales;
        aline.last30Days = last30DaysSales;
        aline.last90Days = last90DaysSales;
        aline.category = 'Total';
        var temp = [];
        temp.push(aline);
        amountActivityList = temp.concat(amountActivityList);
        var perTicket = numberActivityList[0];
        var aline = {};
        aline.todayDate = calculateAverage(todaySales, perTicket.todayDate);
        aline.weekToDate = calculateAverage(wtdSales, perTicket.weekToDate);
        aline.monthToDate = calculateAverage(mtdSales, perTicket.monthToDate);
        aline.monthToDateLastYear = calculateAverage(mtdLastYearSales, perTicket.monthToDateLastYear);
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = calculateAverage(qtdSales, perTicket.quarterToDate);
        aline.quarterToDateLastYear = calculateAverage(qtdLastYearSales, perTicket.quarterToDateLastYear);
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = calculateAverage(ytdSales, perTicket.yearToDate);
        aline.yearToDateLastYear = calculateAverage(ytdLastYearSales, perTicket.yearToDateLastYear);
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = calculateAverage(prevCalendarYearSales, perTicket.priorCalendarYear);
        aline.priorTwelveMonths = calculateAverage(prev12MonthsSales, perTicket.priorTwelveMonths);
        aline.last30Days = 0;
        aline.last90Days = 0;
        aline.category = 'PerClient';
        amountActivityList.push(aline);
        return amountActivityList;
    }
}
function populateClientData(clientData, searchDate, workerId, numberActivityList) {
    var productTickets = numberActivityList[2];
    var totalTickets = numberActivityList[0];
    var clientActivityList = []
    var aline = {};
    aline.category = 'W_Retail';
    aline.todayDate = calculateAverage(productTickets.todayDate, totalTickets.todayDate) * 100;
    aline.weekToDate = calculateAverage(productTickets.weekToDate, totalTickets.weekToDate) * 100;
    aline.monthToDate = calculateAverage(productTickets.monthToDate, totalTickets.monthToDate) * 100;
    aline.monthToDateLastYear = calculateAverage(productTickets.monthToDateLastYear, totalTickets.monthToDateLastYear) * 100;
    aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
    aline.quarterToDate = calculateAverage(productTickets.quarterToDate, totalTickets.quarterToDate) * 100;
    aline.quarterToDateLastYear = calculateAverage(productTickets.quarterToDateLastYear, totalTickets.quarterToDateLastYear) * 100;
    aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
    aline.yearToDate = calculateAverage(productTickets.yearToDate, totalTickets.yearToDate) * 100;
    aline.yearToDateLastYear = calculateAverage(productTickets.yearToDateLastYear, totalTickets.yearToDateLastYear) * 100;
    aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
    aline.priorCalendarYear = calculateAverage(productTickets.priorCalendarYear, totalTickets.priorCalendarYear) * 100;
    aline.priorTwelveMonths = calculateAverage(productTickets.priorTwelveMonths, totalTickets.priorTwelveMonths) * 100;
    aline.last30Days = 0;
    aline.last90Days = 0;
    clientActivityList.push(aline);
    if (!workerId || workerId === null) {
        var reportData = generateAcitvityReportValues(clientData, searchDate, 'apptDate', null, null)
        countArray = reportData.countArray;
        productNumberArray = reportData.productNumberArray;
        salesArray = reportData.salesArray;
        var aline = {};
        aline.category = 'New_Clients';
        aline = populateActivityLine(aline, true, false, countArray, productNumberArray, salesArray);
        clientActivityList.push(aline);
    } else {
        var weekToDate = moment(searchDate).startOf('week').format('YYYY-MM-DD');
        var monthToDate = moment(searchDate).startOf('month').format('YYYY-MM-DD');
        todayDate = moment(searchDate).format('YYYY-MM-DD');
        var day = parseInt(todayDate.split('-')[2]);
        var month = parseInt(todayDate.split('-')[1]);
        var year = parseInt(todayDate.split('-')[0]);
        var lastYear = parseInt(year) - 1;
        var quarterToDate;
        if (month === 1 || month === 2 || month === 3)
            quarterToDate = year + '-' + 1 + '-' + 1;
        else if (month === 4 || month === 5 || month === 6)
            quarterToDate = year + '-' + 4 + '-' + 1;
        else if (month === 7 || month === 8 || month === 9)
            quarterToDate = year + '-' + 7 + '-' + 1;
        else
            quarterToDate = new Date(year + '-' + 10 + '-' + 1);
        yearToDate = year + '-' + 1 + '-' + 1;
        mtdLastYearStartDate = lastYear + '-' + month + '-' + 1;
        mtdLastYearEndDate = lastYear + '-' + month + '-' + day;
        qtdLastYearStartDate = lastYear + '-' + (new Date(quarterToDate).getMonth() + 1) + '-' + 1;
        qtdLastYearEndDate = lastYear + '-' + month + '-' + day;
        ytdLastYearStartDate = lastYear + '-' + 1 + '-' + 1;
        ytdLastYearEndDate = lastYear + '-' + month + '-' + day;
        prevCalendarYearStartDate = lastYear + '-' + 1 + '-' + 1;
        prevCalendarYearEndDate = lastYear + '-' + 12 + '-' + 31;
        prior12MonthsStartDate = moment(searchDate).add(-12, 'months').format('YYYY-MM-DD');
        prior12MonthsEndDate = searchDate;
        last30DaysStartDate = moment(searchDate).add(-30, 'days').format('YYYY-MM-DD');
        last30DaysEndDate = searchDate;
        last90DaysStartDate = moment(searchDate).add(-90, 'days').format('YYYY-MM-DD');
        last90DaysEndDate = searchDate;
        var todayCountSet = [];
        var wtdCountSet = [];
        var mtdCountSet = [];
        var mtdLastYearCountSet = [];
        var qtdCountSet = [];
        var qtdLastYearCountSet = [];
        var ytdCountSet = [];
        var ytdLastYearCountSet = [];
        var prevCalendarYearCountSet = [];
        var prev12MonthsCountSet = [];
        var todayCount = 0;
        var wtdCount = 0;
        var mtdCount = 0;
        var mtdLastYearCount = 0;
        var qtdCount = 0;
        var qtdLastYearCount = 0;
        var ytdCount = 0;
        var ytdLastYearCount = 0;
        var prevCalendarYearCount = 0;
        var prev12MonthsCount = 0;
        clientData.map((obj) => {
            var ticketId = obj.ticket;
            var reportDate = obj.serviceDate;
            //	only want to count one appointment ticket for each service
            //	if an appointment contains many services, it counts as 1
            if (reportDate == todayDate) {
                if (!todayCountSet.filter((obj) => obj === ticketId).length > 0) {
                    todayCountSet.push(ticketId);
                    todayCount++;
                }
            }
            if (reportDate >= weekToDate) {
                if (!wtdCountSet.filter((obj) => obj === ticketId).length > 0) {
                    wtdCountSet.push(ticketId);
                    wtdCount++;
                }
            }
            if (reportDate >= monthToDate) {
                if (!mtdCountSet.filter((obj) => obj === ticketId).length > 0) {
                    mtdCountSet.push(ticketId);
                    mtdCount++;
                }
            }
            if (reportDate >= quarterToDate) {
                if (!qtdCountSet.filter((obj) => obj === ticketId).length > 0) {
                    qtdCountSet.push(ticketId);
                    qtdCount++;
                }
            }
            if (reportDate >= yearToDate) {
                if (!ytdCountSet.filter((obj) => obj === ticketId).length > 0) {
                    ytdCountSet.push(ticketId);
                    ytdCount++;
                }
            }
            if (reportDate >= mtdLastYearStartDate && reportDate <= mtdLastYearEndDate) {
                if (!mtdLastYearCountSet.filter((obj) => obj === ticketId).length > 0) {
                    mtdLastYearCountSet.push(ticketId);
                    mtdLastYearCount++;
                }
            }
            if (reportDate >= qtdLastYearStartDate && reportDate <= qtdLastYearEndDate) {
                if (!qtdLastYearCountSet.filter((obj) => obj === ticketId).length > 0) {
                    qtdLastYearCountSet.push(ticketId);
                    qtdLastYearCount++;
                }
            }
            if (reportDate >= ytdLastYearStartDate && reportDate <= ytdLastYearEndDate) {
                if (!ytdLastYearCountSet.filter((obj) => obj === ticketId).length > 0) {
                    ytdLastYearCountSet.push(ticketId);
                    ytdLastYearCount++;
                }
            }
            if (reportDate >= prevCalendarYearStartDate && reportDate <= prevCalendarYearEndDate) {
                if (!prevCalendarYearCountSet.filter((obj) => obj === ticketId).length > 0) {
                    prevCalendarYearCountSet.push(ticketId);
                    prevCalendarYearCount++;
                }
            }
            if (reportDate >= prior12MonthsStartDate && reportDate <= prior12MonthsEndDate) {
                if (!prev12MonthsCountSet.filter((obj) => obj === ticketId).length > 0) {
                    prev12MonthsCountSet.push(ticketId);
                    prev12MonthsCount++;
                }
            }
        });
        var aline = {};
        aline.category = 'New_Clients';
        aline.todayDate = todayCount;
        aline.weekToDate = wtdCount;
        aline.monthToDate = mtdCount;
        aline.monthToDateLastYear = mtdLastYearCount;
        aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
        aline.quarterToDate = qtdCount;
        aline.quarterToDateLastYear = qtdLastYearCount;
        aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
        aline.yearToDate = ytdCount;
        aline.yearToDateLastYear = ytdLastYearCount;
        aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
        aline.priorCalendarYear = prevCalendarYearCount;
        aline.priorTwelveMonths = prev12MonthsCount;
        aline.last30Days = 0;
        aline.last90Days = 0;
        clientActivityList.push(aline);
    }
    return clientActivityList;
}
function populatePercentageSection(amountActivityList, searchDate, workerId, db, callback) {
    //	Can use existing rows to calculate the percentage row, no SOQL needed
    var productSales = amountActivityList[2];
    var totalSales = amountActivityList[0];
    var percentageActivityList = [];
    var aline = {};
    aline.category = 'Retail_of_Total_Sales';
    aline.todayDate = calculateAverage(productSales.todayDate, totalSales.todayDate) * 100;
    aline.weekToDate = calculateAverage(productSales.weekToDate, totalSales.weekToDate) * 100;
    aline.monthToDate = calculateAverage(productSales.monthToDate, totalSales.monthToDate) * 100;
    aline.monthToDateLastYear = calculateAverage(productSales.monthToDateLastYear, totalSales.monthToDateLastYear) * 100;
    aline.monthPercentChange = calculatePercentChange(aline.monthToDate, aline.monthToDateLastYear);
    aline.quarterToDate = calculateAverage(productSales.quarterToDate, totalSales.quarterToDate) * 100;
    aline.quarterToDateLastYear = calculateAverage(productSales.quarterToDateLastYear, totalSales.quarterToDateLastYear) * 100;
    aline.quarterPercentChange = calculatePercentChange(aline.quarterToDate, aline.quarterToDateLastYear);
    aline.yearToDate = calculateAverage(productSales.yearToDate, totalSales.yearToDate) * 100;
    aline.yearToDateLastYear = calculateAverage(productSales.yearToDateLastYear, totalSales.yearToDateLastYear) * 100;
    aline.yearPercentChange = calculatePercentChange(aline.yearToDate, aline.yearToDateLastYear);
    aline.priorCalendarYear = calculateAverage(productSales.priorCalendarYear, totalSales.priorCalendarYear) * 100;
    aline.priorTwelveMonths = calculateAverage(productSales.priorTwelveMonths, totalSales.priorTwelveMonths) * 100;
    aline.last30Days = 0;
    aline.last90Days = 0;
    percentageActivityList.push(aline);
    // if (workerId !== null) {
    var serviceCountList = [];
    var qry = `
                SELECT
                    COUNT(ts.Service__c) serviceCount,
                    s.Name serviceName,
                    ts.Service_Date_Time__c serviceDate
                FROM
                    Ticket_Service__c ts
                LEFT JOIN Service__c s ON
                    s.Id = ts.Service__c
                LEFT JOIN Appt_Ticket__c a ON
                    a.Id = ts.Appt_Ticket__c
                WHERE
                    (
                        a.isTicket__c = 1 OR a.Is_Class__c = 1
                    ) AND a.Status__c != 'Cancelled' AND a.Status__c != 'No Show' AND a.Is_Booked_Out__c != 1 AND ts.Worker__c = '` + workerId + `' AND DATE(ts.Service_Date_Time__c) <= '` + searchDate + `'
                GROUP BY
                    ts.Service_Date_Time__c,
                    s.Name;
                SELECT
                    Id,
                    NAME,
                    Active__c,
                    ServiceName__c,
                    Price__c,
                    Taxable__c,
                    Duration_1__c,
                    Duration_2__c,
                    Duration_3__c,
                    (
                        Duration_1__c + Duration_2__c + Duration_3__c + Buffer_After__c
                    ) Total_Duration__c,
                    Duration_1_Available_For_Other_Work__c,
                    Duration_2_Available_For_Other_Work__c,
                    Duration_3_Available_For_Other_Work__c,
                    Buffer_After__c,
                    Levels__c,
                    Service_Group__c,
                    Available_For_Client_To_Self_Book__c,
                    Client_Facing_Name__c,
                    Description__c,
                    Resource_Filter__c,
                    Deposit_Required__c,
                    Deposit_Percent__c,
                    Deposit_Amount__c,
                    Guest_Charge__c,
                    Max_Attendees__c,
                    Price_per_Attendee__c,
                    Is_Class__c
                FROM
                    Service__c
                ORDER BY NAME ASC
                LIMIT 1000;`
    getServicesByReportDate(qry, db, function (err, rtnData) {
        if (err) {
            logger.error('error While getting the getServicesByReportDate')
        } else {
            serviceCountList = rtnData[0];
            var serviceDurations = rtnData[1];
            if (serviceCountList && serviceCountList.length > 0) {
                var serviceNameMap = [];
                var weekToDate = moment(searchDate).startOf('week').format('YYYY-MM-DD');
                var monthToDate = moment(searchDate).startOf('month').format('YYYY-MM-DD');
                todayDate = moment(searchDate).format('YYYY-MM-DD');
                var day = parseInt(todayDate.split('-')[2]);
                var month = parseInt(todayDate.split('-')[1]);
                var year = parseInt(todayDate.split('-')[0]);
                var lastYear = parseInt(year) - 1;
                var quarterToDate;
                if (month === 1 || month === 2 || month === 3)
                    quarterToDate = year + '-' + 1 + '-' + 1;
                else if (month === 4 || month === 5 || month === 6)
                    quarterToDate = year + '-' + 4 + '-' + 1;
                else if (month === 7 || month === 8 || month === 9)
                    quarterToDate = year + '-' + 7 + '-' + 1;
                else
                    quarterToDate = new Date(year + '-' + 10 + '-' + 1);
                yearToDate = year + '-' + 1 + '-' + 1;
                mtdLastYearStartDate = lastYear + '-' + month + '-' + 1;
                mtdLastYearEndDate = lastYear + '-' + month + '-' + day;
                qtdLastYearStartDate = lastYear + '-' + (new Date(quarterToDate).getMonth() + 1) + '-' + 1;
                qtdLastYearEndDate = lastYear + '-' + month + '-' + day;
                ytdLastYearStartDate = lastYear + '-' + 1 + '-' + 1;
                ytdLastYearEndDate = lastYear + '-' + month + '-' + day;
                prevCalendarYearStartDate = lastYear + '-' + 1 + '-' + 1;
                prevCalendarYearEndDate = lastYear + '-' + 12 + '-' + 31;
                prior12MonthsStartDate = moment(searchDate).add(-12, 'months').format('YYYY-MM-DD');
                prior12MonthsEndDate = searchDate;
                last30DaysStartDate = moment(searchDate).add(-30, 'days').format('YYYY-MM-DD');
                last30DaysEndDate = searchDate;
                last90DaysStartDate = moment(searchDate).add(-90, 'days').format('YYYY-MM-DD');
                last90DaysEndDate = searchDate;
                serviceCountList.map((obj) => {
                    var reportDate = obj.serviceDate.split(' ')[0];
                    var serviceCount = parseInt(obj.serviceCount);
                    var serviceName = obj.serviceName;
                    var line;
                    if (!serviceNameMap.filter(obj1 => (obj1.serviceName === serviceName)).length)
                        line = {
                            todayDate: 0, weekToDate: 0, priorTwelveMonths: 0, monthToDate: 0, last30Days: 0,
                            last90Days: 0, quarterToDate: 0, yearToDate: 0, monthToDateLastYear: 0,
                            quarterToDateLastYear: 0, yearToDateLastYear: 0, priorCalendarYear: 0,
                            selectedYearData: 0, priorYearData: 0
                        };
                    else
                        line = serviceNameMap.filter(obj1 => (obj1.serviceName === serviceName)[0]);
                    if (serviceCount === null)
                        serviceCount = 0;
                    if (reportDate === todayDate)
                        line.todayDate += serviceCount;
                    if (reportDate >= weekToDate)
                        line.weekToDate += serviceCount;
                    if (reportDate >= monthToDate)
                        line.monthToDate += serviceCount;
                    if (reportDate >= quarterToDate)
                        line.quarterToDate += serviceCount;
                    if (reportDate >= yearToDate)
                        line.yearToDate += serviceCount;
                    if (reportDate >= mtdLastYearStartDate && reportDate <= mtdLastYearEndDate)
                        line.monthToDateLastYear += serviceCount;
                    if (reportDate >= qtdLastYearStartDate && reportDate <= qtdLastYearEndDate)
                        line.quarterToDateLastYear += serviceCount;
                    if (reportDate >= ytdLastYearStartDate && reportDate <= ytdLastYearEndDate)
                        line.yearToDateLastYear += serviceCount;
                    if (reportDate >= prevCalendarYearStartDate && reportDate <= prevCalendarYearEndDate)
                        line.priorCalendarYear += serviceCount;
                    if (reportDate >= prior12MonthsStartDate && reportDate <= prior12MonthsEndDate)
                        line.priorTwelveMonths += serviceCount;
                    if (reportDate >= last30DaysStartDate && reportDate <= last30DaysEndDate)
                        line.last30Days += serviceCount;
                    if (reportDate >= last90DaysStartDate && reportDate <= last90DaysEndDate)
                        line.last90Days += serviceCount;
                    serviceNameMap.push([serviceName, line]);
                })
                serviceDurations.map((obj) => {
                    var duration = 0
                    if (obj.Duration_1_Available_For_Other_Work__c !== 1) { duration += obj.Duration_1__c }
                    if (obj.Duration_2_Available_For_Other_Work__c !== 1) { duration += obj.Duration_2__c }
                    if (obj.Duration_3_Available_For_Other_Work__c !== 1) { duration += obj.Duration_3__c }
                    if (obj.Buffer_After__c !== null) { duration += obj.Buffer_After__c }
                    serviceNameMap.map((obj1) => {
                        if (obj1[0] === obj['NAME']) {
                            line = obj1[1];
                            line.todayDate = line.todayDate * duration;
                            line.weekToDate = line.weekToDate * duration;
                            line.monthToDate = line.monthToDate * duration;
                            line.quarterToDate = line.quarterToDate * duration;
                            line.yearToDate = line.yearToDate * duration;
                            line.monthToDateLastYear = line.monthToDateLastYear * duration;
                            line.quarterToDateLastYear = line.quarterToDateLastYear * duration;
                            line.yearToDateLastYear = line.yearToDateLastYear * duration;
                            line.priorCalendarYear = line.priorCalendarYear * duration;
                            line.priorTwelveMonths = line.priorTwelveMonths * duration;
                            line.last30Days = line.last30Days * duration;
                            line.last90Days = line.last90Days * duration;
                            line.selectedYearData = line.selectedYearData * duration;
                            line.priorYearData = line.priorYearData * duration;
                        }
                    })
                });
                var dto = {};
                dto.todayDate = 0;
                dto.weekToDate = 0;
                dto.monthToDate = 0;
                dto.quarterToDate = 0;
                dto.yearToDate = 0;
                dto.monthToDateLastYear = 0;
                dto.quarterToDateLastYear = 0;
                dto.yearToDateLastYear = 0;
                dto.priorCalendarYear = 0;
                dto.priorTwelveMonths = 0;
                dto.last30Days = 0;
                dto.last90Days = 0;
                dto.selectedYearData = 0;
                dto.priorYearData = 0;
                for (var i = 0; i < serviceNameMap.length; i++) {
                    dto.todayDate += line.todayDate;
                    dto.weekToDate += line.weekToDate;
                    dto.monthToDate += line.monthToDate;
                    dto.quarterToDate += line.quarterToDate;
                    dto.yearToDate += line.yearToDate;
                    dto.monthToDateLastYear += line.monthToDateLastYear;
                    dto.quarterToDateLastYear += line.quarterToDateLastYear;
                    dto.yearToDateLastYear += line.yearToDateLastYear;
                    dto.priorCalendarYear += line.priorCalendarYear;
                    dto.priorTwelveMonths += line.priorTwelveMonths;
                    dto.last30Days += line.last30Days;
                    dto.last90Days += line.last90Days;
                    dto.selectedYearData += line.selectedYearData;
                    dto.priorYearData += line.priorYearData;
                }
                aline = {};
                aline.category = 'Productivity_Percent_Rate';
                aline.todayDate = dto.todayDate;
                aline.weekToDate = dto.weekToDate;

                aline.monthToDate = dto.monthToDate;
                aline.monthToDateLastYear = dto.monthToDateLastYear;
                aline.monthPercentChange = calculatePercentChange(dto.monthToDate, dto.monthToDateLastYear);

                aline.quarterToDate = dto.quarterToDate;
                aline.quarterToDateLastYear = dto.quarterToDateLastYear;
                aline.quarterPercentChange = calculatePercentChange(dto.quarterToDate, dto.quarterToDateLastYear);

                aline.yearToDate = dto.yearToDate;
                aline.yearToDateLastYear = dto.yearToDateLastYear;
                aline.yearPercentChange = calculatePercentChange(dto.yearToDate, dto.yearToDateLastYear);

                aline.priorCalendarYear = dto.priorCalendarYear;
                aline.priorTwelveMonths = dto.priorTwelveMonths;

                aline.last30Days = dto.last30Days;
                aline.last90Days = dto.last90Days;
                percentageActivityList.push(aline);
            } else {
                aline = {};
                aline.category = 'Productivity_Percent_Rate';
                aline.todayDate = 'N/A';
                aline.weekToDate = 'N/A';

                aline.monthToDate = 'N/A';
                aline.monthToDateLastYear = 'N/A';
                aline.monthPercentChange = 'N/A';

                aline.quarterToDate = 'N/A';
                aline.quarterToDateLastYear = 'N/A';
                aline.quarterPercentChange = 'N/A';

                aline.yearToDate = 'N/A';
                aline.yearToDateLastYear = 'N/A';
                aline.yearPercentChange = 'N/A';

                aline.priorCalendarYear = 'N/A';
                aline.priorTwelveMonths = 'N/A';

                aline.last30Days = 'N/A';
                aline.last90Days = 'N/A';
                percentageActivityList.push(aline);
            }
        }
        callback(percentageActivityList);
    });
}
function populateCompensationSection(db, workerId, last30DaysStartDate, last30DaysEndDate, last90DaysStartDate, last90DaysEndDate,
    countArray, productNumberArray, salesArray, amountActivityList, numberActivityList, searchDate, callback) {
    var qry = '';
    qry = 'select Id, Worker__c, Begin_Date__c, End_Date__c, Compensation_Total__c ' +
        'from Compensation_Run__c where DATE(End_Date__c) >="' + last30DaysStartDate + '" and DATE(End_Date__c) <="' + last30DaysEndDate + '"'
    if (workerId) {
        qry += ' and Worker__c = "' + workerId + '"'
    }
    qry += ';select Id, Worker__c, Begin_Date__c, End_Date__c, Compensation_Total__c ' +
        'from Compensation_Run__c where DATE(End_Date__c) >="' + last90DaysStartDate + '" and DATE(End_Date__c) <="' + last90DaysEndDate + '"'
    if (workerId) {
        qry += ' and Worker__c = "' + workerId + '"'
    }
    var qry1 = '';
    if (workerId) {
        qry1 = `
    SELECT
        tot.Id,
        tot.Ticket__c,
        a.Appt_Date_Time__c ticketDate,
        tot.Transaction_Type__c,
        SUM(tot.Amount__c) otherSales
    FROM
        Ticket_Other__c tot
    LEFT JOIN Appt_Ticket__c a ON
        a.Id = tot.Ticket__c
    WHERE
        DATE(a.Appt_Date_Time__c) <= '`+ searchDate + `' AND tot.Transaction_Type__c IN('Membership', 'Package') AND a.Status__c = 'Complete'
    GROUP BY
        tot.Id,
        a.Appt_Date_Time__c,
        tot.Ticket__c,
        tot.Transaction_Type__c`
    }
    getServicesByReportDate(qry + ';' + qry1, db, function (err, rtnData) {
        if (err) {
            logger.error('error While getting the getServicesByReportDate')
        } else {
            var compensationList = [];
            var sum90DayGrossPay = 0;
            var sum30DayGrossPay = 0;
            var aline = {};
            aline.category = '';
            if (rtnData[0] && rtnData[0].length > 0) {
                rtnData[0].map((obj) => { sum30DayGrossPay += obj.Compensation_Total__c })
            }
            if (rtnData[1] && rtnData[1].length > 0) {
                rtnData[1].map((obj) => { sum90DayGrossPay += obj.Compensation_Total__c })
            }
            if (rtnData[2] && rtnData[2].length > 0) {
                var searchDate = last30DaysEndDate;
                var reportData = generateAcitvityReportValues(rtnData[2], searchDate, 'ticketDate', 'otherSales', null)
                countArray = reportData.countArray;
                productNumberArray = reportData.productNumberArray;
                salesArray = reportData.salesArray;

            }
            if (workerId) {
                aline = populateActivityLine(aline, false, false, countArray, productNumberArray, salesArray);
            } else {
                aline.last30Days = 0;
                aline.last90Days = 0;
            }
            totalAmountLine = amountActivityList[0];
            var last30DaysSales = (aline.last30Days + totalAmountLine.last30Days);
            var last90DaysSales = (aline.last90Days + totalAmountLine.last90Days);
            aline = {};
            aline.category = 'Last_30_Days';
            if (sum30DayGrossPay == null || sum30DayGrossPay == 0) {
                aline.todayDate = 'N/A';
            }

            aline.todayDate = calculateAverage(sum30DayGrossPay, last30DaysSales) * 100;

            compensationList.push(aline);

            var perTicket = numberActivityList[0];
            var aline = {};
            aline.category = 'Last_90_Days';

            if (sum90DayGrossPay == null || sum90DayGrossPay == 0)
                aline.todayDate = 'N/A';

            aline.todayDate = calculateAverage(sum90DayGrossPay, last90DaysSales) * 100;

            compensationList.push(aline);
            // }
            callback(null, compensationList);
        }
    });
}