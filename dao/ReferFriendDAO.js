var logger = require('../lib/logger');
var config = require('config');
var execute = require('../common/dbConnection');
var uniqid = require('uniqid');
var dateFns = require('./../common/dateFunctions');

module.exports = {
    referFriend: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var referFriendObj = req.body;
        referFriendObj.emailTemplate = referFriendObj.emailTemplate.replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        var referFriendData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateFns.getUTCDatTmStr(new Date()),
            CreatedById: loginId,
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedById: loginId,
            SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            JSON__c: [referFriendObj],
            Name: config.referFriend
        };
        this.getreferFriend(req, function (err, result) {
            if (result.statusCode === '9999') {
                var sqlQuery = 'INSERT INTO ' + config.dbTables.preferenceTBL + ' SET ?';
                execute.query(dbName, sqlQuery, referFriendData, function (err, data) {
                    if (err) {
                        logger.error('Error1 in referFriend dao - savereferFriend:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            } else if (result && result != null) {
                var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                    + " SET JSON__c = '" + JSON.stringify(referFriendObj)
                    + "', LastModifiedDate = '" + dateFns.getUTCDatTmStr(new Date())
                    + "', LastModifiedById = '" + loginId
                    + "' WHERE Name = '" + config.referFriend + "'";
                execute.query(dbName, sqlQuery, '', function (err, data) {
                    if (err) {
                        logger.error('Error2 in referFriend dao - savereferFriend:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            }
        });
    },
    /**
    * This function lists referFriend
    */
    getreferFriend: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.referFriend + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in referFriend dao - getreferFriend:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    JSON__c_str['emailTemplate'] = JSON__c_str['emailTemplate'].replace(/`/g, '\"');
                    done(err, JSON__c_str);
                }
            });
        } catch (err) {
            logger.error('Unknown error in referFriend dao - getreferFriend:', err);
            return (err, { statusCode: '9999' });
        }
    }
};