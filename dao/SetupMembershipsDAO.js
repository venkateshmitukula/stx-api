var config = require('config');
var logger = require('../lib/logger');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var dateFns = require('./../common/dateFunctions');

module.exports = {
    /**
     * This method create a single record in data_base
     */
    saveMemberships: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        try {
            var membershipsObj = req.body;
            var membershipsData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                Name: membershipsObj.Name,
                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                CreatedById: loginId,
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedById: loginId,
                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                Active__c: membershipsObj.active,
                Price__c: membershipsObj.price
            };
            var sqlQuery = 'INSERT INTO ' + config.dbTables.setupMembershipTBL + ' SET ?';
            execute.query(dbName, sqlQuery, membershipsData, function (err, result, fields) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Name') > 0) {
                        done(err, { statusCode: '2033' });
                    } else {
                        logger.error('Error in SetupMemberships dao - saveSetupMemberships:', err);
                        done(err, { statusCode: '9999' });
                    }
                } else {
                    done(err, result);
                }

            });
        } catch (err) {
            logger.error('Unknown error in SetupMembershipsDAO - saveSetupMemberships:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * This method updates memberships
     */
    editMemberships: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var updateObj = req.body;
        var Id = req.params.id;
        var queryString = 'UPDATE ' + config.dbTables.setupMembershipTBL + ' SET Active__c="'
            + updateObj.updateActive + '", LastModifiedDate="' + dateFns.getUTCDatTmStr(new Date()) + '", LastModifiedById = "' + loginId + '", Name="' + updateObj.updateName + '", Price__c="' + updateObj.updatePrice + '" WHERE Id="' + Id + '"';
        execute.query(dbName, queryString, function (error, results, fields) {
            if (error != null) {
                if (error.sqlMessage.indexOf('Name') > 0) {
                    done(error, { statusCode: '2033' });
                } else {
                    logger.error('Error in SetupMembershipsDAO dao - editMemberships:', err);
                    done(error, '9999');
                }
            } else {
                done(error, results);
            }
        });
    },
    /**
     * This method fetches all data from setup memberships
     */
    getMemberships: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.setupMembershipTBL + ' WHERE isDeleted = 0';
            if (parseInt(req.params.inActive) === config.booleanTrue)
                sqlQuery = sqlQuery + ' AND Active__c = ' + config.booleanTrue;
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupMembershipsDao - getMemberships:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupMembershipsDao - getMemberships:', err);
            done(err, null);
        }
    },
    getMemberSerach: function (req, done) {
        var dbName = req.headers['db'];
        if (req.params.id) {
            var removeSpace = req.params.id.replace(" ", "");
        }
        try {
            var sql = `SELECT
            DISTINCT Id,
            FirstName,
            LastName,
            Birthdate,
            Gender__c,
            Client_Pic__c AS image,
            IFNULL(MobilePhone,"") AS MobilePhone,
            IFNULL(Phone,"") AS Phone,
            Email AS Email,
            Membership_ID__c
        FROM 
            Contact__c
            WHERE IsDeleted =0 AND CONCAT(FirstName, ' ' , LastName) != 'NO CLIENT'
        HAVING
            CONCAT(FirstName," ",LastName) LIKE "%` + req.params.id + `%" OR
            MobilePhone LIKE "%` + req.params.id + `%" OR
            Phone LIKE "%` + req.params.id + `%" OR
            Email LIKE "%` + req.params.id + `%" OR 
            Membership_ID__c LIKE "%` + req.params.id + `%" 
         ORDER BY 
            Active__c desc,
            FirstName,
            LastName,
            Phone
        LIMIT 100`;
            execute.query(dbName, sql, '', function (err, result) {
                if (err) {
                    logger.error('Error in SetupMembershipsDao - getMemberSerach:', err);
                    done(err, result);
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in SetupMembershipsDao - getMemberSerach:', err);
            done(err, null);
        }
    },
    fullClientDetails: function (req, done) {
        var dbName = req.headers['db'];
        var ClientId = req.params.id;
        var sql = 'SELECT c.Id as clientId,cm.Id as clientMemberId,concat(c.FirstName," ",c.LastName) as name,c.FirstName, '
            + ' c.Birthdate, c.Gender__c, cm.Next_Bill_Date__c, ms.Name, ms.Price__c, ms.Billing_Cycle__c,c.Membership_ID__c as memberID, '
            + ' c.Client_Pic__c AS image'
            + ' FROM Contact__c as c '
            + ' left join Client_Membership__c as cm on cm.Client__c = c.Id '
            + ' left join Membership__c as ms on ms.Id = cm.Membership__c '
            + ' where cm.Client__c = "' + ClientId + '" '
            + ' and cm.IsDeleted = 0 '
            + ' and c.IsDeleted = 0 '
            + ' and ms.Active__c = 1 '
            + ' and ms.IsDeleted = 0';
        execute.query(dbName, sql, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - fullClientDetails:', err);
                done(err, result);
            } else {
                done(err, result);
            }
        });
    },
    getmemberDetails: function (req, done) {
        var dbName = req.headers['db'];
        var ClientId = req.params.id;
        var sql = 'SELECT c.Id as clientId,cm.Id as clientMemberId,concat(c.FirstName," ",c.LastName) as name,c.FirstName, '
            + ' c.Birthdate, c.Gender__c, cm.Next_Bill_Date__c, ms.Name, ms.Price__c, ms.Billing_Cycle__c,c.Membership_ID__c as memberID, '
            + ' c.Client_Pic__c AS image'
            + ' FROM Contact__c as c '
            + ' left join Client_Membership__c as cm on cm.Client__c = c.Id '
            + ' left join Membership__c as ms on ms.Id = cm.Membership__c '
            + ' where c.Membership_ID__c = "' + ClientId + '" '
            + ' and cm.IsDeleted = 0 '
            + ' and c.IsDeleted = 0 '
            + ' and ms.Active__c = 1 '
            + ' and ms.IsDeleted = 0';
        execute.query(dbName, sql, '', function (err, result) {
            if (err) {
                logger.error('Error in SetupMembershipsDao - getmemberDetails:', err);
                done(err, result);
            } else {
                done(err, result);
            }
        });
    }, ticketpayments: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var quickAddClientObj = req.body.obj[0];
        
        var autoBill = 0;
        if (quickAddClientObj.Auto_Bill__c === true) {
            autoBill = 1;
        } else {
            autoBill = 0;
        }
        var quickAddClientData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            FirstName: quickAddClientObj.firstName,
            LastName: quickAddClientObj.lastName,
            Birthdate: quickAddClientObj.birthday,
            Gender__c: quickAddClientObj.gender,
            Membership_ID__c: quickAddClientObj.memUniId,
            Phone: quickAddClientObj.Client_Type__c,
            Email: quickAddClientObj.email,
            Active__c: 1,
            Sms_Consent__c: quickAddClientObj.sms_checkbox,
            MobilePhone: quickAddClientObj.mobileNumber
        };
        var sqlQuery = 'INSERT INTO ' + config.dbTables.ContactTBL + ' SET ?';
        execute.query(dbName, sqlQuery, quickAddClientData, function (err, result) {
            if (err) {
                logger.error('Error in setupMemberShip dao - ticketpayments:', err);
                done(err, { statusCode: '9999' });
            } else {
                console.log(quickAddClientData.Id)
                if (result && result.affectedRows > 0) {

                    var addClientMemberShip = {
                        Id: uniqid(),
                        Client__c: quickAddClientData.Id,
                        Billing_Status__c: 'Approved',
                        Auto_Bill__c: autoBill,
                        Next_Bill_Date__c: quickAddClientObj.Next_Bill_Date__c,
                        Membership__c: quickAddClientObj.MemberShipType.split('$')[0],
                        Membership_Price__c: quickAddClientObj.MemberShipType.split('$')[1],
                        Payment_Type__c: quickAddClientObj.paymentTypes.split('$')[0],
                        Token__c: quickAddClientObj.token,
                        OwnerId: loginId,
                        IsDeleted: 0,
                        CreatedDate: dateTime,
                        CreatedById: loginId,
                        LastModifiedDate: dateTime,
                        LastModifiedById: loginId,
                        SystemModstamp: dateTime
                    }
                    console.log('----------', addClientMemberShip);
                    var sqlQuery = 'INSERT INTO ' + config.dbTables.clientMembershipTBL + ' SET ?';
                    execute.query(dbName, sqlQuery, addClientMemberShip, function (error, data) {
                        if (error) {
                            logger.error('Error in setupMemberShipDao - ticketpayments:', error);
                            done(error, data);
                        } else {
                            console.log('sucess----------------', data)
                            done(error, data);
                        }
                    });


                } else {
                    done(err, { statusCode: '9999' });
                }
            }
        });
    }

}