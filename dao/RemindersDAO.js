var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
var mail = require('../common/sendMail');
var dateFns = require('./../common/dateFunctions');
var CommonSRVC = require('../services/CommonSRVC');

module.exports = {
    sendReminders: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var remindersObj = req.body;
        remindersObj.emailTemplate = remindersObj.emailTemplate.replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        var remindersData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateFns.getUTCDatTmStr(new Date()),
            CreatedById: loginId,
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedById: loginId,
            SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            JSON__c: [remindersObj],
            Name: config.apptReminders
        };
        this.getReminders(req, function (err, result) {
            if (result.statusCode === '9999') {
                var sqlQuery = 'INSERT INTO ' + config.dbTables.preferenceTBL + ' SET ?';
                execute.query(dbName, sqlQuery, remindersData, function (err, data) {
                    if (err) {
                        logger.error('Error1 in Reminders dao - saveReminders:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            } else if (result && result != null) {
                var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                    + " SET JSON__c = '" + JSON.stringify(remindersObj)
                    + "', LastModifiedDate = '" + dateFns.getUTCDatTmStr(new Date())
                    + "', LastModifiedById = '" + loginId
                    + "' WHERE Name = '" + config.apptReminders + "'";
                execute.query(dbName, sqlQuery, '', function (err, data) {
                    if (err) {
                        logger.error('Error2 in apptReminders dao - saveReminders:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
                done(err, { statusCode: '9999' });
            }
        });
    },
    /**
     * This function lists Reminders
     */
    getReminders: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.apptReminders + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in apptReminders dao - apptReminders:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    JSON__c_str['emailTemplate'] = JSON__c_str['emailTemplate'].replace(/`/g, '\"');
                    done(err, JSON__c_str);
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptReminders dao - apptReminders:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
    * This function send Reminders
    */
    sendemailReminders: function (req, done) {
        try {
            CommonSRVC.getApptRemindersEmail(req.headers['db'], function (email) {
                mail.sendemail(req.body.reminderEmailAddress, email, req.body.subject, req.body.emailTemplate, '', function (err, response) {
                    if (response != null) {
                        done(err, response);
                    } else
                        done(err, '2056');
                });
            });
        } catch (err) {
            logger.error('Unknown error in apptReminders dao - sendemailReminders:', err);
            return (err, { statusCode: '9999' });
        }
    }
};
