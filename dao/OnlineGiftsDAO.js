var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
var dateFns = require('./../common/dateFunctions');

module.exports = {
    saveOnlineGifts: function(req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var onlineGiftsObj = req.body;
        onlineGiftsObj.emailTemplate = onlineGiftsObj.emailTemplate.replace(/"/g, '\`').replace(/\n/g,'').replace(/\t/g,'');
        var onlineGiftsData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateFns.getUTCDatTmStr(new Date()),
            CreatedById: loginId,
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedById: loginId,
            SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            JSON__c: [onlineGiftsObj],
            Name: config.giftsOnline
        };
        this.getOnlineGifts(req, function(err, result){
            if (result.statusCode === '9999') {
                var sqlQuery = 'INSERT INTO ' + config.dbTables.preferenceTBL + ' SET ?';
                        execute.query(dbName, sqlQuery, onlineGiftsData, function (err, data) {
                            if (err) {
                                logger.error('Error1 in OnlineGifts dao - saveOnlineGifts:', err);
                                done(err, { statusCode: '9999' });
                            } else {
                                done(err, data);
                            }
                        });
            } else if(result && result != null) {
                var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                    + " SET JSON__c = '" + JSON.stringify(onlineGiftsObj)
                    + "', LastModifiedDate = '" + dateFns.getUTCDatTmStr(new Date())
                    + "', LastModifiedById = '" + loginId
                    + "' WHERE Name = '" + config.giftsOnline + "'";
                execute.query(dbName, sqlQuery, '', function (err, data) {
                    if (err) {
                        logger.error('Error2 in giftsOnline dao - saveOnlineGifts:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            }
        });
    },
    /**
     * This function lists OnlineGifts
     */
    getOnlineGifts: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.giftsOnline + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in OnlineGifts dao - getOnlineGifts:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    JSON__c_str['emailTemplate'] = JSON__c_str['emailTemplate'].replace(/`/g, '\"');
                    done(err, JSON__c_str);
                }
            });
        } catch (err) {
            logger.error('Unknown error in OnlineGifts dao - getOnlineGifts:', err);
            return (err, { statusCode: '9999' });
        }
    }
};