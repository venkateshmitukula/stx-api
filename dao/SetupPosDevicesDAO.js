var config = require('config');
var logger = require('../lib/logger');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var _ = require("underscore");
var dateFns = require('./../common/dateFunctions');

module.exports = {
    /**
   * This method create a single record in data_base
   */
    savePosDevices: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        try {
            var posDevicesObj = req.body;
            var posDevices = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                CreatedById: loginId,
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedById: loginId,
                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                JSON__c: JSON.stringify([posDevicesObj.cashDrawers]),
                Name: config.cashDrawers
            };
            var receiptMemo = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                CreatedById: loginId,
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedById: loginId,
                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                JSON__c: JSON.stringify([posDevicesObj.receiptMemo]),
                Name: config.receiptMemo
            };
            var sqlQuery = 'INSERT INTO ' + config.dbTables.preferenceTBL + ' SET ?';
            this.getPosDevices(req, function (err, result) {
                if (result.statusCode === '9999') {
                    execute.query(dbName, sqlQuery, posDevices, function (err, data) {
                        if (err) {
                            logger.error('Error1 in posDevices dao - saveposDevices:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            execute.query(dbName, sqlQuery, receiptMemo, function (err, data) {
                                if (err) {
                                    logger.error('Error1 in posDevices dao - savereceiptMemo:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    execute.query(dbName, sqlQuery, posDevices, function (err, data) {
                                        if (err) {
                                            logger.error('Error1 in posDevices dao - savereceiptMemo:', err);
                                            done(err, { statusCode: '9999' });
                                        } else {
                                            done(err, data);
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else if (result && result.length > 0) {
                    var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                        + " SET JSON__c = '" + JSON.stringify(posDevicesObj.cashDrawers)
                        + "', LastModifiedDate = '" + dateFns.getUTCDatTmStr(new Date())
                        + "', LastModifiedById = '" + loginId
                        + "' WHERE Name = '" + config.cashDrawers + "'";
                    execute.query(dbName, sqlQuery, '', function (err, data) {
                        if (err) {
                            logger.error('Error2 in mobileCarriers dao - savemobileCarriers:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            var sqlQuery1 = 'UPDATE ' + config.dbTables.preferenceTBL
                                + ' SET JSON__c = "' + posDevicesObj.receiptMemo
                                + '", LastModifiedDate = "' + dateFns.getUTCDatTmStr(new Date())
                                + '", LastModifiedById = "' + loginId
                                + '" WHERE Name = "' + config.receiptMemo + '"';
                            execute.query(dbName, sqlQuery1, '', function (err, data) {
                                if (err) {
                                    logger.error('Error2 in mobileCarriers dao - savemobileCarriers:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    done(err, data);
                                }
                            });
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in posDevices dao - saveposDevices:', err);
            done(err, { statusCode: '9999' });
        }
    },
    /**
     * To get posDevices List
     */
    getPosDevices: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + " WHERE Name = 'Cash Drawers' or Name = 'Receipt Memo' ORDER BY Name";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in posDevices DAO - getposDevices:', err);
            done(err, null);
        }
    },
    /**
    * To get Pos List
    */
    getPos: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + " WHERE Name = 'Sales Tax' or Name = 'Merchant In Store' or Name = 'Merchant Online' or Name = 'Payment Gateway' ORDER BY Name";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in posDevices DAO - getposDevices:', err);
            done(err, null);
        }
    },
    getPosWithoutLogin: function (req, done) {
        var dbName = req.params.db;
        try {
            var sqlQuery = 'SELECT JSON__c FROM ' + config.dbTables.preferenceTBL + " WHERE Name = 'Sales Tax' or Name = 'Merchant In Store' or Name = 'Merchant Online' or Name = 'Payment Gateway' ORDER BY Name";
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (err) {
                    done(err, { statusCode: '9999' });
                } else {
                    done(err, result);
                }
            });
        } catch (err) {
            logger.error('Unknown error in posDevices DAO - getPosWithoutLogin:', err);
            done(err, null);
        }
    },
    savePos: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        try {
            var posObj = req.body;
            var pos = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                CreatedById: loginId,
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedById: loginId,
                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                JSON__c: JSON.stringify([posObj.salesTax]),
                Name: config.posTax
            };
            var paymentGateway = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                CreatedById: loginId,
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedById: loginId,
                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                JSON__c: JSON.stringify([posObj.paymentGateway]),
                Name: config.posPaymentGateway
            };
            var merchantInStore = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                CreatedById: loginId,
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedById: loginId,
                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                JSON__c: JSON.stringify([posObj.merchantInStore]),
                Name: config.posMerchantInStore
            };
            var merchantOnline = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateFns.getUTCDatTmStr(new Date()),
                CreatedById: loginId,
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedById: loginId,
                SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
                LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
                JSON__c: JSON.stringify([posObj.merchantOnline]),
                Name: config.posMerchantOnline
            };
            var sqlQuery = 'INSERT INTO ' + config.dbTables.preferenceTBL + ' SET ?';
            this.getPos(req, function (err, result) {
                if (result.statusCode === '9999') {
                    execute.query(dbName, sqlQuery, pos, function (err, data) {
                        if (err) {
                            logger.error('Error1 in posDevices dao - saveposDevices:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            execute.query(dbName, sqlQuery, pos, function (err, data) {
                                if (err) {
                                    logger.error('Error1 in posDevices dao - savereceiptMemo:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    done(err, data);
                                }
                            });
                        }
                    });
                } else if (result && result.length > 0) {
                    var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                        + " SET JSON__c = '" + JSON.stringify(posObj.salesTax)
                        + "', LastModifiedDate = '" + dateFns.getUTCDatTmStr(new Date())
                        + "', LastModifiedById = '" + loginId
                        + "' WHERE Name = '" + config.posTax + "'";
                    execute.query(dbName, sqlQuery, '', function (err, data) {
                        if (err) {
                            logger.error('Error2 in mobileCarriers dao - savemobileCarriers:', err);
                            done(err, { statusCode: '9999' });
                        } else {
                            var sqlQuery1 = "UPDATE " + config.dbTables.preferenceTBL
                                + " SET JSON__c = '" + JSON.stringify(posObj.merchantInStore)
                                + "', LastModifiedDate = '" + dateFns.getUTCDatTmStr(new Date())
                                + "', LastModifiedById = '" + loginId
                                + "' WHERE Name = '" + config.posMerchantInStore + "'";
                            execute.query(dbName, sqlQuery1, '', function (err, data) {
                                if (err) {
                                    logger.error('Error2 in mobileCarriers dao - savemobileCarriers:', err);
                                    done(err, { statusCode: '9999' });
                                } else {
                                    var sqlQuery2 = "UPDATE " + config.dbTables.preferenceTBL
                                        + " SET JSON__c = '" + JSON.stringify(posObj.merchantOnline)
                                        + "', LastModifiedDate = '" + dateFns.getUTCDatTmStr(new Date())
                                        + "', LastModifiedById = '" + loginId
                                        + "' WHERE Name = '" + config.posMerchantOnline + "'";
                                    execute.query(dbName, sqlQuery2, '', function (err, data) {
                                        if (err) {
                                            logger.error('Error2 in mobileCarriers dao - savemobileCarriers:', err);
                                            done(err, { statusCode: '9999' });
                                        } else {
                                            var sqlQuery3 = "UPDATE " + config.dbTables.preferenceTBL
                                                + " SET JSON__c = '" + JSON.stringify(posObj.paymentGateway)
                                                + "', LastModifiedDate = '" + dateFns.getUTCDatTmStr(new Date())
                                                + "', LastModifiedById = '" + loginId
                                                + "' WHERE Name = '" + config.posPaymentGateway + "'";
                                            execute.query(dbName, sqlQuery3, '', function (err, data) {
                                                if (err) {
                                                    logger.error('Error2 in mobileCarriers dao - savemobileCarriers:', err);
                                                    done(err, { statusCode: '9999' });
                                                } else {
                                                    done(err, data);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } catch (err) {
            logger.error('Unknown error in posDevices dao - saveposDevices:', err);
            done(err, { statusCode: '9999' });
        }
    },
}