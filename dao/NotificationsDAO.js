var config = require('config');
var uniqid = require('uniqid');
var execute = require('../common/dbConnection');
var logger = require('../lib/logger');
var mail = require('../common/sendMail');
var dateFns = require('./../common/dateFunctions');
var CommonSRVC = require('../services/CommonSRVC');

module.exports = {
    // Start of code to create Notification
    createNotifications: function (req, done) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var notificationsObj = req.body;
        notificationsObj.emailTemplate = notificationsObj.emailTemplate.replace(/"/g, '\`').replace(/\n/g, '').replace(/\t/g, '');
        var notificationsData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            CreatedDate: dateFns.getUTCDatTmStr(new Date()),
            CreatedById: loginId,
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedById: loginId,
            SystemModstamp: dateFns.getUTCDatTmStr(new Date()),
            LastModifiedDate: dateFns.getUTCDatTmStr(new Date()),
            JSON__c: [notificationsObj],
            Name: config.apptNotification
        };
        this.getNotifications(req, function (err, result) {
            if (result.statusCode === '9999') {
                var sqlQuery = 'INSERT INTO ' + config.dbTables.preferenceTBL + ' SET ?';
                execute.query(dbName, sqlQuery, notificationsData, function (err, data) {
                    if (err) {
                        logger.error('Error1 in Notifications dao - saveNotifications:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            } else if (result && result != null) {
                // result = JSON.parse(result);
                // result.push(notificationsObj);
                var sqlQuery = "UPDATE " + config.dbTables.preferenceTBL
                    + " SET JSON__c = '" + JSON.stringify(notificationsObj)
                    + "', LastModifiedDate = '" + dateFns.getUTCDatTmStr(new Date())
                    + "', LastModifiedById = '" + loginId
                    + "' WHERE Name = '" + config.apptNotification + "'";
                execute.query(dbName, sqlQuery, '', function (err, data) {
                    if (err) {
                        logger.error('Error2 in Notification dao - saveNotification:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        done(err, data);
                    }
                });
            }
        });
    },
    /**
     * This function lists Notifications
     */
    getNotifications: function (req, done) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM ' + config.dbTables.preferenceTBL
                + ' WHERE Name = "' + config.apptNotification + '"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    JSON__c_str['emailTemplate'] = JSON__c_str['emailTemplate'].replace(/`/g, '\"');
                    done(err, JSON__c_str);
                } else {
                    logger.error('Error in Notifications dao - getNotifications:', err);
                    done(err, { statusCode: '9999' });
                }
            });
        } catch (err) {
            logger.error('Unknown error in Notifications dao - getNotifications:', err);
            return (err, { statusCode: '9999' });
        }
    },
    /**
    * This function send Notifications
    */
    sendNotifications: function (req, done) {
        try {
            CommonSRVC.getApptNotificationEmail(req.headers['db'], function (email) {
                mail.sendemail(req.body.notificationEmailAddress, email, req.body.subject, req.body.emailTemplate, '', function (err, response) {
                    if (response != null) {
                        done(err, response);
                    } else
                        done(err, '2056');
                });
            });
        } catch (err) {
            logger.error('Unknown error in Notifications dao - getNotifications:', err);
            return (err, { statusCode: '9999' });
        }
    }
};
