var TimeClockDAO = require('../dao/TimeClockDAO');

module.exports = {
    saveTimeClock: function(req, done) {
        TimeClockDAO.saveTimeClock(req, function(err, data) {
          if(err) {
                done({httpCode: 500, statusCode: '9999', result: {}});
            } else {
                done({httpCode: 200, statusCode: '1001', result: data});
            }
      });
    },
    getTimeClock: function(req, done) {
        TimeClockDAO.getTimeClock(req, function(err, data) {
          if(err) {
                done({httpCode: 500, statusCode: '9999', result: {}});
            } else {
                done({httpCode: 200, statusCode: '1001', result: data});
            }
      });
    },
    getWorkerByPin: function(req, done) {
        TimeClockDAO.getWorkerByPin(req, function(err, data) {
          if(err) {
                done({httpCode: 500, statusCode: '9999', result: {}});
            } else {
                done({httpCode: 200, statusCode: '1001', result: data});
            }
      });
    },
    saveMultipleTimeClockData: function(req, done) {
        TimeClockDAO.saveMultipleTimeClockData(req, function(err, data) {
          if(err) {
                done({httpCode: 500, statusCode: '9999', result: {}});
            } else {
                done({httpCode: 200, statusCode: '1001', result: data});
            }
      });
    }
};


