var config = require('config');
var utils = require('../lib/util');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');

module.exports.controller = function (app) {

  //--- Start of code to execute query in all DBs ---//
  app.post('/api/execute/query', function (req, res) {
    getAllDBList(function (dbList) {
      executeQuery(dbList, req['body']['query'], function (result) {
        utils.sendResponse(res, 200, 1001, result);
      });
    });
  });
  //--- End of code to execute query in all DBs ---//
};

//--- Start of code to get all DB List ---//
function getAllDBList(callback) {
  var sqlQuery = `SELECT
        SCHEMA_NAME
      FROM
        SCHEMA_LIST
      WHERE
        ACTIVE = 1`;
  execute.query(config.globalDBName, sqlQuery, '', function (error, results) {
    if (error) {
      logger.error('Error in getAllDBList:', error);
      callback(null);
    } else {
      callback(results);
    }
  });
}
//--- End of code to get all DB List ---//

//--- Start of code to execute query ---//
function executeQuery(schemaList, qry, callback) {
  if (schemaList && schemaList.length > 0) {
    var rtnObj = [];
    var index = 0;
    for (var i = 0; i < schemaList.length; i++) {
      rtnObj[schemaList[i]['SCHEMA_NAME']] = null;
      execute.query(schemaList[i]['SCHEMA_NAME'], qry, '', function (error, results) {
        index++;
        if (error) {
          logger.error('Error in getRemnTemp:', error);
        } else {
          rtnObj.push(results);
        }
        if (index === schemaList.length) {
          callback(rtnObj);
        }
      });
    }
  } else {
    callback(null);
  }
}
//--- End of code to execute query ---//
