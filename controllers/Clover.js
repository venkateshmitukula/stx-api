var utils = require('../lib/util');
var config = require('config');
var request = require('request');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var uniqid = require('uniqid');

module.exports.controller = function (app) {
    app.get('/api/clover/device/list', function (req, res) {
        request({
            url: config.cloverCfg.server
                + '/v3/merchants/'
                + config.cloverCfg.merchantId
                + '/devices',
            qs: {
                access_token: config.cloverCfg.accessToken
            },
            method: 'GET'
        }, function (error, response, body) {
            if (error) {
                utils.sendResponse(res, 200, 1001, []);
            } else {
                utils.sendResponse(res, 200, 1001, JSON.parse(body)['elements']);
            }
        });
    });

    app.post('/api/clover/device/initialize', function (req, res) {
        request({
            url: config.cloverCfg.server
                + '/v2/merchant/'
                + config.cloverCfg.merchantId
                + '/remote_pay',
            qs: {
                access_token: config.cloverCfg.accessToken
            },
            method: 'POST',
            form: req.body
        }, function (error, response, body) {
            if (error) {
                utils.sendResponse(res, 200, 1001, []);
            } else {
                utils.sendResponse(res, 200, 1001, JSON.parse(body));
            }
        });
    });

    app.post('/api/clover/tip/insert', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var apptId = req.body['apptid'];
        var tip = req.body['tip'];
        var query = `SELECT
                Worker__c WORKER,
                Net_Price__c + IF(Service_Tax__c IS NOT NULL AND Service_Tax__c != '', Service_Tax__c, 0) AS SALE
            FROM
                Ticket_Service__c
            WHERE
                Appt_Ticket__c = ?
                AND Worker__c IS NOT NULL
                AND Worker__c != '';
            SELECT
                Worker__c WORKER,
                Net_Price__c + IF(Product_Tax__c IS NOT NULL AND Product_Tax__c != '', Product_Tax__c, 0) AS SALE
            FROM
                Ticket_Product__c
            WHERE
                Appt_Ticket__c = ?
                AND Worker__c IS NOT NULL
                AND Worker__c != '';
            SELECT
                Worker__c WORKER,
                Amount__c +
                    IF(Product_Tax__c IS NOT NULL AND Product_Tax__c != '', Product_Tax__c, 0) +
                    IF(Service_Tax__c IS NOT NULL AND Service_Tax__c != '', Service_Tax__c, 0) AS SALE
            FROM
                Ticket_Other__c
            WHERE
                Ticket__c = ?
                AND Worker__c IS NOT NULL
                AND Worker__c != ''`;
        var parms = [apptId, apptId, apptId];
        execute.query(dbName, query, parms, function (err, data) {
            if (err) {
                logger.error('Error in Clover Tips Insertion: ', err);
                utils.sendResponse(res, 500, 9999, []);
            } else {
                var insObj = [];
                var totalSale = 0;
                data = data.filter(obj => obj.length > 0);
                data.forEach((item1) => {
                    item1.forEach((item2) => {
                        totalSale += item2['SALE'];
                        insObj.push(item2);
                    });
                });
                query = `INSERT INTO ` + config.dbTables.ticketTipTBL + ` (
                        Id,
                        Name,
                        LastActivityDate,
                        Appt_Ticket__c,
                        Drawer_Number__c,
                        Tip_Amount__c,
                        Tip_Option__c,
                        Worker__c,
                        CreatedById,
                        LastModifiedById
                    )
                    VALUES ?`;
                var values = [];
                insObj.forEach((item) => {
                    values.push([
                        uniqid(),
                        null,
                        null,
                        apptId,
                        null,
                        parseInt(tip * item['SALE'] / totalSale) / 100,
                        'Tip Left in Drawer',
                        item['WORKER'],
                        loginId,
                        loginId
                    ])
                });
                execute.query(dbName, query, [values], function (err) {
                    if (err) {
                        logger.error('Error in Clover Tips Insertion: ', err);
                        utils.sendResponse(res, 500, 9999, []);
                    } else {
                        utils.sendResponse(res, 200, 1001, []);
                    }
                });
            }
        });
    });
};
