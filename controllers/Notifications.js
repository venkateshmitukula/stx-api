var utils = require('../lib/util');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var config = require('config');
var client = require('twilio');
var clientConf = client(config.twilioAccountSid, config.twilioAuthToken);
var mail = require('../common/sendMail');
var sms = require('../common/sendSms');
var CommonSRVC = require('../services/CommonSRVC');

module.exports.controller = function (app) {
    app.post('/api/client/send/text', function (req, res) {
        var qry = 'SELECT JSON__c FROM `Preference__c` WHERE Name = "Appt Reminders"; SELECT DATE_FORMAT(Appt_Date_Time__c,"%W, %D %M, %Y %l: %i %p") AS apptDtTm FROM `Appt_Ticket__c` WHERE Id = "' + req.body.apptId + '"';
        execute.query(req.headers['db'], qry, function (error, result) {
            if (error) {
                logger.info('data fecthing failed');
            } else {
                var tempText = JSON.parse(result[0][0].JSON__c).reminderTextMessage.replace(/<br>/g, '\n');
                tempText = tempText.replace(/{{AppointmentDate\/Time}}/g, result[1][0]['apptDtTm']);
                sms.sendsms(req.body.mobileNum, tempText, function (err, result1) {
                    if (err) {
                        logger.info('Sms not Sent to :' + result1);
                    } else {
                        logger.info('Sms Sent to :' + result1['to']);
                        utils.sendResponse(res, 200, 1001, '');
                    }
                })
            }
        });
    });
    app.post('/api/notification/email/cancel', function (req, res) {
        var apptId = req.body.apptId
        var qry = `
        SELECT
                JSON__c
            FROM
                Preference__c
            WHERE
                Name='Appt Notification';
        SELECT 
            a.Id,
        GROUP_CONCAT(DATE_FORMAT(ts.Service_Date_Time__c, '%W, %D %M, %Y %l:%i %p')) AS apptDtTm, 
            u.FirstName,
            u.LastName,
            CONCAT(c.FirstName, ' ', c.LastName) as clientName,
            GROUP_CONCAT(CONCAT(' ', s.Name)) AS services,
            u.Phone,
            u.Email as primaryEmail,
            'cancelled' as Action
        FROM 
            Appt_Ticket__c as a 
            LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id
            LEFT JOIN Service__c as s on s.Id = ts.Service__c
            LEFT JOIN User__c as u on u.Id = ts.Worker__c
            LEFT JOIN Contact__c as c on c.Id = a.Client__c
        WHERE
            a.Id = '`+ apptId + `' AND
            u.Send_Notification_for_Canceled_Appt__c = 1
        GROUP BY
            u.Id
        ORDER BY 
            ts.Id
        DESC`;
        execute.query(req.headers['db'], qry, function (error, result) {
            if (error) {
                logger.error('Error in Notifications query:', error);
                utils.sendResponse(res, 500, 9999, '');
            } else {
                if (result[1] && result[1].length > 0) {
                    var templJSON = JSON.parse(result[0][0]['JSON__c'].replace(/`/g, '\''));
                    templJSON = templJSON['subject'].split('-')[0] + ' - Appointment Cancelled '
                    sendNotificaionsToWorker(result[1], templJSON, req.headers['db'], function (workerEmailError, workerEmailResult) {
                        if (workerEmailError) {
                            logger.error('Error in sendApptNotfEmail:', workerEmailError);
                        } else {
                            logger.info('Mail Sent to Succesfully to the Respected Client:');
                        }
                    })
                }
            }
        });
    });
    app.post('/api/notification/email', function (req, res, done) {
        var qry = `SELECT
                Name,
                Phone__c,
                Email__c
            FROM
                Company__c
            WHERE
                IsDeleted=0
            ORDER BY
                LastModifiedDate DESC;
            SELECT
                JSON__c
            FROM
                Preference__c
            WHERE
                Name='Appt Notification';
            SELECT
                a.Id,
                DATE_FORMAT(a.Appt_Date_Time__c, '%W, %D %M, %Y %l:%i %p') AS apptDtTm,
                c.FirstName,
                c.LastName,
                IF(c.Notification_Mobile_Phone__c, c.MobilePhone, '') AS Phone,
                IF(c.Notification_Primary_Email__c, c.Email, '') AS primaryEmail,
                IF(c.Notification_Secondary_Email__c, c.Secondary_Email__c, '') AS secondaryEmail,
                GROUP_CONCAT(CONCAT(' ', s.Name)) AS services
            FROM
                Appt_Ticket__c a 
                LEFT JOIN Ticket_Service__c ts ON a.Id = ts.Appt_Ticket__c,
                Contact__c c,
                Service__c s
            WHERE
                a.Client__c = c.Id AND
                c.Notification_Opt_Out__c = 0 AND
                (
                    (c.Notification_Primary_Email__c = 1 AND c.Email IS NOT NULL AND c.Email != '') OR
                    (c.Notification_Mobile_Phone__c = 1 AND c.MobilePhone IS NOT NULL AND c.MobilePhone != '') OR
                    (c.Notification_Secondary_Email__c = 1 AND c.Secondary_Email__c IS NOT NULL AND c.Secondary_Email__c != '')
                ) AND
                ts.IsDeleted = 0 AND
                s.Id = ts.Service__c AND
                a.Id IN ` + getInQryStr(req.body['apptIds']) + `
            GROUP BY
                a.Id;
            SELECT 
            a.Id,
            GROUP_CONCAT(DATE_FORMAT(ts.Service_Date_Time__c, '%W, %D %M, %Y %l:%i %p')) AS apptDtTm, 
                u.FirstName,
                u.LastName,
                CONCAT(c.FirstName, ' ', c.LastName) as clientName,
                GROUP_CONCAT(CONCAT(' ', s.Name)) AS services,
                IFNULL(u.MobilePhone, u.Phone) Phone,
                u.Email as primaryEmail,
                'booked' as Action
            FROM 
                Appt_Ticket__c as a 
                LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = a.Id
                LEFT JOIN Service__c as s on s.Id = ts.Service__c
                LEFT JOIN User__c as u on u.Id = ts.Worker__c
                LEFT JOIN Contact__c as c on c.Id = a.Client__c
            WHERE
                a.Id IN ` + getInQryStr(req.body['apptIds']) + ` AND
                u.Send_Notification_for_Booked_Appointment__c = 1
            GROUP BY
                u.Id
            ORDER BY 
                ts.Id
            DESC`;
        execute.query(req.headers['db'], qry, function (error, result) {
            if (error) {
                logger.error('Error in Notifications query:', error);
                utils.sendResponse(res, 500, 9999, '');
            } else {
                var compData = result[0][0];
                var templJSON = JSON.parse(result[1][0]['JSON__c'].replace(/`/g, '\''));
                var apptData = result[2];
                var workerData = result[3]
                if (templJSON['sendNotifications']) {
                    if (apptData && apptData.length > 0) {
                        sendEmailNotification(compData, templJSON, apptData, req.headers['db'], function (emailError, emailResult) {
                            if (emailError) {
                                logger.error('Error in sendApptNotfEmail:', emailError);
                            } else {
                                logger.info('Mail Sent to Succesfully to the Respected Client:');
                            }
                        })
                    }
                    if (workerData && workerData.length > 0) {
                        sendNotificaionsToWorker(workerData, templJSON['subject'], req.headers['db'], function (workerEmailError, workerEmailResult) {
                            if (workerEmailError) {
                                logger.error('Error in sendApptNotfEmail:', workerEmailError);
                            } else {
                                logger.info('Mail Sent to Succesfully to the Respected Client:');
                            }
                        })

                    }


                }
                utils.sendResponse(res, 200, 1001, '');
            }
        });
    });
    app.post('/api/notification/email/owner', function (req, res, done) {
        var apptId = req.body.apptId;
        var sql = `
    SELECT
        c.FirstName,
        c.LastName,
        IF(c.BR_Reason_No_Show__c = 1, 'Persistent No Show,', '') as R1,
        IF(c.BR_Reason_Account_Charge_Balance__c = 1 , 'Account Charge Balance,', '') as R2,
        IF(c.BR_Reason_Deposit_Required__c = 1, 'Deposit Required,', '') as R3,
        IF(c.BR_Reason_No_Email__c, 'No Email,', '') as R4,
        GROUP_CONCAT(s.Name) as services,
        GROUP_CONCAT(DATE_FORMAT(ts.Service_Date_Time__c, '%W, %D %M, %Y, %l:%i %p')) AS apptDtTm,
        CONCAT(u.FirstName, ' ', u.LastName) as workerName  
    FROM
        Contact__c c
    LEFT JOIN Appt_Ticket__c a ON
        a.Client__c = c.Id
        LEFT JOIN Ticket_Service__c ts on a.Id = ts.Appt_Ticket__c
        LEFT JOIN Service__c s on s.Id = ts.Service__c
        LEFT JOIN User__c u on u.Id = ts.Worker__c
    WHERE
        a.Id = '`+ apptId + `' AND c.Booking_Restriction_Type__c = 'Alert Only'
        GROUP BY a.Id;
    SELECT
        u.FirstName,
        u.LastName,
        u.Email
    FROM
        User__c u
    WHERE
        u.UserType = 'Business Owner'
    GROUP BY
        u.Id`;
        execute.query(req.headers['db'], sql, function (error, result) {
            if (error) {
                logger.error('error while fetching owner data', error);
            } else {
                if ((result[0] && result[0].length > 0) && (result[1] && result[1].length > 0)) {
                    var resons = result[0][0]['R1'] + ' ' + result[0][0]['R2'] + ' ' + result[0][0]['R3'] + ' ' + result[0][0]['R4'];
                    resons = resons.slice(0, -1);
                    var rtnStr = [];
                    for (var i = 0; i < result[1].length; i++) {
                        rtnStr.push({
                            "email": result[1][i]['Email']
                        });
                    }
                    var emailTempl = `
                    <HTML>
                    <body>
                    <p><b>{{FirstName}} {{LastName}} </b> just booked an appointment online for the following <br/><br/><b>{{BookedServices}}</b> Date: <b>{{AppointmentDate/Time}}</b>
                    worker: <b>{{workerName}}</b></p>
                    <p>{{FirstName}} {{LastName}} has a booking restrinction for the following restrictions</p><p><b>{{resons}}</b></p>
                    </body>
                    Thanks
                    </HTML>
                    `;
                    var tempTempl = emailTempl;
                    tempTempl = tempTempl.replace(/{{FirstName}}/g, result[0][0]['FirstName']);
                    tempTempl = tempTempl.replace(/{{LastName}}/g, result[0][0]['LastName']);
                    tempTempl = tempTempl.replace(/{{resons}}/g, resons);
                    tempTempl = tempTempl.replace(/{{workerName}}/g, result[0][0]['workerName']);
                    tempTempl = tempTempl.replace(/{{AppointmentDate\/Time}}/g, result[0][0]['apptDtTm']);
                    tempTempl = tempTempl.replace(/{{BookedServices}}/g, result[0][0]['services'].trim());
                    // }
                    var subject = 'Booking Alert For ' + result[0][0]['FirstName'] + ' ' + result[0][0]['LastName'];
                    CommonSRVC.getApptNotificationEmail(req.headers['db'], function (email) {
                        mail.sendemail(rtnStr, email, subject, tempTempl, '', function (err, result) {
                        });
                    });
                }
                utils.sendResponse(res, 200, 1001, '');
            }
        });
    });
}

//--- Start of function to generate string from array for IN query parameters ---//
function getInQryStr(arryObj) {
    var rtnStr = '';
    if (arryObj && arryObj.length > 0) {
        rtnStr += '(';
        for (var i = 0; i < arryObj.length; i++) {
            rtnStr += '\'' + arryObj[i] + '\',';
        }
        rtnStr = rtnStr.slice(0, -1);
        rtnStr += ')';
    }
    return rtnStr;
}
//--- End of function to generate string from array for IN query parameters ---//
/**
 * 
 * @param {*} compData 
 * @param {*} templJSON 
 * @param {*} apptData 
 * @param {*} done 
 */
function sendEmailNotification(compData, templJSON, apptData, db, done) {
    var emailTempl = templJSON['emailTemplate'].replace(/`/g, '\'');
    var textBody = templJSON['notificationTextMessage'].replace(/`/g, '\'').replace(/<br>/g, '\n');
    emailTempl = emailTempl.replace(/{{CompanyName}}/g, compData['Name']);
    emailTempl = emailTempl.replace(/{{CompanyEmail}}/g, compData['Email__c']);
    emailTempl = emailTempl.replace(/{{CompanyPrimaryPhone}}/g, compData['Phone__c']);
    CommonSRVC.getApptNotificationEmail(db, function (email) {
        for (var i = 0; i < apptData.length; i++) {
            var tempTempl = emailTempl;
            tempTempl = tempTempl.replace(/{{ClientFirstName}}/g, apptData[i]['FirstName']);
            tempTempl = tempTempl.replace(/{{ClientLastName}}/g, apptData[i]['LastName']);
            tempTempl = tempTempl.replace(/{{ClientPrimaryEmail}}/, apptData[i]['primaryEmail']);
            tempTempl = tempTempl.replace(/{{ClientPrimaryPhone}}/, apptData[i]['Phone']);
            tempTempl = tempTempl.replace(/{{AppointmentDate\/Time}}/g, apptData[i]['apptDtTm']);
            tempTempl = tempTempl.replace(/{{BookedServices}}/g, apptData[i]['services'].trim());
            if (apptData[i]['primaryEmail'] || apptData[i]['secondaryEmail']) {
                toAddress = [];
                if (apptData[i]['primaryEmail']) {
                    toAddress.push(apptData[i]['primaryEmail']);
                }
                if (apptData[i]['secondaryEmail'] && apptData[i]['primaryEmail'] !== apptData[i]['secondaryEmail']) {
                    toAddress.push(apptData[i]['secondaryEmail']);
                }
                templJSON['subject'] = templJSON['subject'].replace(/{{CompanyName}}/g, compData['Name'])
                mail.sendemail(toAddress, email, templJSON['subject'], tempTempl, '', function (error, result) {
                    if (error) {
                        logger.error('Error in sendApptNotfEmail:', error);
                    }
                });
            }
            /**
             * for Mobile sms
             */
            if (apptData[i]['Phone']) {
                const firstName = apptData[i]['FirstName'];
                const lastName = apptData[i]['LastName']
                var textSms = textBody;
                textSms = textBody.replace(/{{AppointmentDate\/Time}}/g, apptData[i]['apptDtTm']);
                textSms = textSms.replace(/{{BookedServices}}/g, apptData[i]['services'].trim());
                sms.sendsms(apptData[i]['Phone'], textSms, function (err, result1) {
                    if (err) {
                        logger.info('Sms not Sent to :' + result1);
                    } else {
                        logger.info('Sms Sent to :' + result1['to']);
                        done(err, result1);
                    }
                })
            }
        }
    });
}
function sendNotificaionsToWorker(workerData, templJSON, db, done) {
    CommonSRVC.getApptNotificationEmail(db, function (email) {
        for (var i = 0; i < workerData.length; i++) {
            var emailTempl = `
            <HTML>
            <body>
            <p> Hi <b>{{FirstName}} {{LastName}} </b><br/><br> An appointment has been {{Action}} by <b>{{ClientName}}</b> with the Services of <b>{{BookedServices}}</b> on <b>{{AppointmentDate/Time}}</b></p>
            </body>
            Thanks
            </HTML>
            `;
            var tempTempl = emailTempl;
            tempTempl = tempTempl.replace(/{{FirstName}}/g, workerData[i]['FirstName']);
            tempTempl = tempTempl.replace(/{{LastName}}/g, workerData[i]['LastName']);
            tempTempl = tempTempl.replace(/{{ClientName}}/g, workerData[i]['clientName']);
            tempTempl = tempTempl.replace(/{{Action}}/g, workerData[i]['Action']);
            tempTempl = tempTempl.replace(/{{AppointmentDate\/Time}}/g, workerData[i]['apptDtTm']);
            tempTempl = tempTempl.replace(/{{BookedServices}}/g, workerData[i]['services'].trim());
            if (workerData[i]['primaryEmail'] || workerData[i]['secondaryEmail']) {
                toAddress = [];
                if (workerData[i]['primaryEmail']) {
                    toAddress.push(workerData[i]['primaryEmail']);
                }
                if (workerData[i]['secondaryEmail'] && workerData[i]['primaryEmail'] !== workerData[i]['secondaryEmail']) {
                    toAddress.push(workerData[i]['secondaryEmail']);
                }
                mail.sendemail(toAddress, email, templJSON, tempTempl, '', function (error, result) {
                    if (error) {
                        logger.error('Error in sendApptNotfEmail:', error);
                    }
                });

            }
            if (workerData[i]['Phone']) {
                const firstName = workerData[i]['FirstName'];
                const lastName = workerData[i]['LastName'];
                var textBody = 'Hi, {{FirstName}} {{LastName}} An appointment has been {{Action}} by {{clientName}} with the Services of {{BookedServices}} on {{AppointmentDate/Time}}';
                var textSms = textBody;
                textSms = textBody.replace(/{{AppointmentDate\/Time}}/g, workerData[i]['apptDtTm']);
                textSms = textSms.replace(/{{BookedServices}}/g, workerData[i]['services'].trim());
                textSms = textSms.replace(/{{FirstName}}/g, workerData[i]['FirstName']);
                textSms = textSms.replace(/{{LastName}}/g, workerData[i]['LastName']);
                textSms = textSms.replace(/{{clientName}}/g, workerData[i]['clientName']);
                textSms = textSms.replace(/{{Action}}/g, workerData[i]['Action']);
                sms.sendsms(workerData[i]['Phone'], textSms, function (err, result1) {
                    if (err) {
                        logger.info('Sms not Sent to :' + result1);
                    } else {
                        logger.info('Sms Sent to :' + result1['to']);
                        done(err, result1);
                    }
                })
            }
        }
    });
    // utils.sendResponse(res, 200, 1001, '');
}