var utils = require('../lib/util');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
module.exports.controller = function (app) {

	app.post('/api/payment', function (req, res) {
		var request = require("request");
		request.post({
			url: req.body.url,
			method: "POST",
			headers: {
				'Content-Type': 'application/xml',
			},
			body: req.body.xml
		},
			function (error, response, body) {
				if (error) {
					logger.error('Error in Payment: ', error);
					utils.sendResponse(res, 500, 9999, []);
				} else {
					utils.sendResponse(res, response.statusCode, 1001, body);
				}
			});
	});
	app.delete('/api/payment/:id', function (req, res) {
		var dbName = req.headers['db'];
		var onlinebook = req.headers['onlinebooking'];
		var query = '';
		if (onlinebook) {
			query = 'DELETE FROM `Ticket_Service__c` WHERE Appt_Ticket__c = "' + req.params.id + '";'
		} else {
			query += 'DELETE FROM `Ticket_Other__c` WHERE Ticket__c = "' + req.params.id + '";'
		}
		query += 'DELETE FROM `Appt_Ticket__c` WHERE Id = "' + req.params.id + '";'
		execute.query(dbName, query, '', function (error, response, result) {
			if (error) {
				logger.error('Error in Payment: ', error);
				utils.sendResponse(res, 500, 9999, []);
			} else {
				utils.sendResponse(res, 200, 1001, '');
			}
		});
	});
};
