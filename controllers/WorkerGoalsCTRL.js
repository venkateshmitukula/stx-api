var workerGoalsSRVC = require('../services/WorkerGoalsSRVC');
var utils = require('../lib/util');

module.exports.controller = function(app) {
    /**
     * This API saves workergoals
     */
    app.post('/api/setup/workers/workergoals', function (req, res) {
        workerGoalsSRVC.saveWorkerGoals(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API gets workergoals
     */
    app.get('/api/setup/workers/workergoals', function (req, res) {
        workerGoalsSRVC.getWorkerGoals(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.put('/api/setup/workers/workergoals/calculategoal/:id', function (req, res) {
        workerGoalsSRVC.updtaeCalculateGoal(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};
