var cfg = require('config');
var SetupWorkerDetailSRVC = require('../services/SetupWorkerDetailSRVC');
var utils = require('../lib/util');
var logger = require('../lib/logger');
var multer = require('multer');
var uniqid = require('uniqid');
var fs = require('fs');

var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: cfg.AWSAccessKey,
    secretAccessKey: cfg.AWSSecretKey,
    region: cfg.S3Region
});
var s3 = new AWS.S3();
// --- Start of Controller
module.exports.controller = function (app) {
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            var cmpyId = req.headers['cid'];
            var uplLoc = cfg.uploadsPath + cmpyId
            if (!fs.existsSync(uplLoc)) {
                fs.mkdirSync(uplLoc);
            }
            uplLoc += '/' + cfg.workerFilePath;
            if (!fs.existsSync(uplLoc)) {
                fs.mkdirSync(uplLoc);
            }
            callback(null, uplLoc);
        }, filename: function (req, file, callback) {
            callback(null, file.originalname);
        }
    });
    var uploadWorkerImage = multer({ storage: storage }).single('workerImage');
    app.put('/api/setupworkers/setupworkerdetail/:id?', function (req, res) {
        uploadWorkerImage(req, res, function (err) {
            var workerId = uniqid();
            if (JSON.parse(req.body.workerInfo).page === 'edit') {
                workerId = req.params.id;
            }
            try {
                var uplLoc = cfg.uploadsPath + req.headers['cid']
                    + '/' + cfg.workerFilePath + '/' + workerId;
                var options = {
                    Bucket: cfg.S3Name,
                    Key: uplLoc
                };
                if (req.file) {
                    s3.deleteObject(options, function (err) {
                        options.Body = fs.readFileSync(uplLoc);
                        s3.putObject(options, function (err) {
                            if (err) {
                                logger.info('unable to upload pworker img to s3', uplLoc)
                            } else {
                                fs.unlink(uplLoc, function () { })
                            }
                        });
                    });
                }
            } catch (err) {
                logger.info('unable to upload pworker img', err)
            }
            if (err) {
                logger.error('Error uploading pworker Logo', err);
            } else {
                SetupWorkerDetailSRVC.editWorkerDetail(req, workerId, function (data) {
                    utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
                });
            }
        });

    });
    app.get('/api/setupworkers/setupworkerdetail', function (req, res) {
        SetupWorkerDetailSRVC.getWorkerDetail(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });

    app.get('/api/setupworkers/setupworkerdetail/workerservices/:id?', function (req, res) {
        SetupWorkerDetailSRVC.getWorkerservicesByUser(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/setupworkers/setupworkerdetail/workerhours/:id?', function (req, res) {
        SetupWorkerDetailSRVC.getWorkersHoursByUser(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * This API saves custom hours
     */
    app.post('/api/setupworkers/setupworkerdetail/workercustomhours', function (req, res) {
        SetupWorkerDetailSRVC.saveWorkerCustomHours(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * Getting the custom hours timings
     */
    app.get('/api/setupworkers/setupworkerdetail/workercustomhours/:id', function (req, res) {
        SetupWorkerDetailSRVC.getWorkerCustomHours(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * Getting the custom hours timings
     */
    app.delete('/api/setupworkers/setupworkerdetail/customhours/:id', function (req, res) {
        SetupWorkerDetailSRVC.deleteWorkerCustomHours(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.put('/api/setupworkers/setupworkerservice', function (req, res) {
        SetupWorkerDetailSRVC.editWorkerService(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    /**
     * To Create Worker
     */
    app.post('/api/setupworkers', function (req, res) {
        SetupWorkerDetailSRVC.createWorker(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
};