var utils = require('../lib/util');
var execute = require('../common/dbConnection');
var config = require('config');
module.exports.controller = function (app) {

    //--- Start of API to get Clients data for Email Marketing ---//
    app.post('/api/em/client/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = `SELECT Id, FirstName, LastName, Email FROM Contact__c WHERE 
             IsDeleted = 0 AND Email IS NOT NULL AND Email !=''`;
        var firstDay = new Date();
        var lastDay = new Date(firstDay);
        lastDay.setDate(lastDay.getDate() - 2);
        if (req.body.type === '0' || req.body.type === 0) {
            sql += ` AND LastModifiedDate >= ? AND LastModifiedDate < ? `;
        }
        sql += ` GROUP BY Email`;
        execute.query(dbName, sql, [lastDay, firstDay], function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get Clients data for Email Marketing ---//

    //--- Start of API to get Users data for Email Marketing ---//
    app.post('/api/em/users/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = `SELECT Id, FirstName, LastName, Email FROM User__c`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get Users data for Email Marketing ---//

    //--- Start of API to get Clients data based on User for Email Marketing ---//
    app.post('/api/em/clients/user/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var userId = req.body.staff_id;
        var sql = `SELECT c.Id, c.FirstName, c.LastName, c.Email FROM Contact__c c 
            LEFT JOIN Appt_Ticket__c as at on at.Client__c=c.Id
            LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c=at.Id and ts.IsDeleted = 0
            WHERE ts.Worker__c= '`+ userId + `' and c.IsDeleted = 0 AND c.Email IS NOT NULL AND c.Email !=''
            GROUP by c.Email`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get Clients data based on User for Email Marketing ---//
    //--- Start of API to get Clients data for Email Marketing ---//
    app.post('/api/em/client/typelist', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = '';
        var type = req.body.list_type
        if (type === '1' || type === 1) {
            sql = `
            SELECT
                c.Id,
                c.FirstName,
                c.LastName,
                c.Email
            FROM
                Contact__c c
            LEFT JOIN
                Appt_Ticket__c a on c.Id = a.Client__c
            WHERE
                c.IsDeleted = 0 AND
                c.Email IS NOT NULL AND
                c.Email !='' AND
                a.Status__c NOT IN ('Canceled')
            GROUP BY
                c.Email
            HAVING
                COUNT(a.Id) = 1`
        } else if (type === '2' || type === 2) {
            sql = `
                SELECT
                    c2.Id, 
                    c2.FirstName, 
                    c2.LastName, 
                    c2.Email 
                FROM
                    Contact__c c1, 
                    Contact__c c2 
                WHERE
                    c2.IsDeleted = 0 AND
                    c1.Referred_By__c IS NOT NULL
                    AND c1.Referred_By__c !=''
                    AND c1.Referred_By__c = c2.Id 
                    AND c2.Email IS NOT NULL 
                    AND c2.Email !=''
                GROUP BY 
                    c2.Id, 
                    c2.Email`
        } else if (type === '3' || type === 3) {
            var date = new Date();
            sql = `
                SELECT 
                    c.Id, c.FirstName, c.LastName, c.Email 
                FROM Contact__c c 
                WHERE 
                    c.BirthDateNumber__c=`+ date.getDate() + ` AND
                    c.BirthMonthNumber__c=`+ (date.getMonth() + 1) + `
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`
        } else if (type === '4' || type === 4) {
            sql = `
                SELECT 
                    c.Id, c.FirstName, c.LastName, c.Email 
                FROM Contact__c c 
                WHERE 
                c.BirthMonthNumber__c=`+ (new Date().getMonth() + 1) + `
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`
        }
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get Clients data for Email Marketing ---//

    //--- Start of API to get Services data for Email Marketing ---//
    app.post('/api/em/services/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = `
            SELECT 
                Id, 
                Name 
            FROM 
                Service__c 
            WHERE
            IsDeleted = 0 and Is_Class__c =0`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get Services data for Email Marketing ---//

    //--- Start of API to get services data based on User for Email Marketing ---//
    app.post('/api/em/clients/services/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var service_id = req.body.service_id;
        var sql = `
            SELECT 
                c.Id, c.FirstName, c.LastName, c.Email 
            FROM Contact__c c 
            LEFT JOIN Ticket_Service__c as ts on ts.Client__c=c.Id and ts.IsDeleted = 0            
            WHERE 
                ts.Service__c= '`+ service_id + `'
                AND c.IsDeleted = 0 
                AND c.Email IS NOT NULL 
                AND c.Email !=''
            GROUP by c.Email`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    //--- End of API to get services data based on User for Email Marketing ---//
    app.post('/api/em/servicegroups/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = ` 
            SELECT * 
                FROM  
                    Preference__c
                    WHERE Name = '` + config.serviceGroups + `'`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                var JSON__c_str = JSON.parse(result[0].JSON__c);

                result[0].JSON__c = JSON__c_str.sort(function (a, b) {
                    return a.sortOrder - b.sortOrder
                });
                result[0].JSON__c = result[0].JSON__c
                    .filter(filterList => filterList.active && !filterList.isSystem);
                for (var i = 0; i < result[0].JSON__c.length; i++) {
                    result[0].JSON__c[i] = {
                        'serviceGroupName': result[0].JSON__c[i].serviceGroupName,
                        'sortOrder': result[0].JSON__c[i].sortOrder
                    }
                }
                utils.sendResponse(res, 200, '1001', result[0].JSON__c);
            }
        });
    });
    app.post('/api/em/clients/servicegroups/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var service_group = req.body.service_group;
        var sql = `
            SELECT 
                c.Id, c.FirstName, c.LastName, c.Email 
            FROM Contact__c c 
            LEFT JOIN Ticket_Service__c as ts on ts.Client__c=c.Id and ts.IsDeleted = 0  
            LEFT JOIN Service__c as s on s.Id=ts.Service__c
            WHERE 
                s.Service_Group__c='`+ service_group + `'
                AND c.IsDeleted = 0 
                AND c.Email IS NOT NULL 
                AND c.Email !=''
            GROUP by c.Email`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/flags/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = ` 
            SELECT * 
                FROM  
                    Preference__c
                    WHERE Name = '` + config.clientFlags + `'`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                var JSON__c_str = JSON.parse(result[0].JSON__c);
                JSON__c_str = JSON__c_str
                    .filter(filterList => filterList.active && filterList.flagName);
                for (var i = 0; i < JSON__c_str.length; i++) {
                    JSON__c_str[i] = {
                        'flagName': JSON__c_str[i].flagName
                    }
                }
                utils.sendResponse(res, 200, '1001', JSON__c_str);
            }
        });
    });
    app.post('/api/em/clients/flags/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var flag = req.body.flag_name;
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
                FROM 
                    Contact__c c
                WHERE 
                    c.Client_Flag__c LIKE '%`+ flag + `%'
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/clients/gender/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var gender = req.body.gender;
        var start_date = req.body.start_date;
        var end_date = req.body.end_date;
        var gederType = 'All'
        if (gender && start_date && end_date) {
            if (gender === 1 || gender === '1') {
                gederType = 'Male';
            } else if (gender === 2 || gender === '2') {
                gederType = 'Female';
            } else if (gender === 3 || gender === '3') {
                gederType = 'Unspecified';
            }
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
            FROM 
                Contact__c c
            WHERE`;
        if (gederType !== 'All') {
            sql += ` c.Gender__c = '` + gederType + `' AND`
        }
        sql += `    DATE(c.LastModifiedDate) >= '` + start_date + `'
                    AND DATE(c.LastModifiedDate) <'`+ end_date + `'
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                    AND c.Gender__c IS NOT NULL 
                    AND c.Gender__c !=''
                GROUP by c.Email`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/clients/status/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var status = req.body.status;
        var start_date = req.body.start_date;
        var end_date = req.body.end_date;
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
                FROM 
                    Contact__c c
                        LEFT JOIN Appt_Ticket__c as a on a.Client__c=c.Id
                        WHERE 
                            a.Status__c='`+ status + `' 
                            AND DATE(a.Appt_Date_Time__c) >= '`+ start_date + `'
                            AND DATE(a.Appt_Date_Time__c) <= '`+ end_date + `'
                            AND c.IsDeleted = 0
                            AND c.Email IS NOT NULL
                            AND c.Email != ''
                            GROUP by c.Email`;
        if (status && start_date && end_date) {
            execute.query(dbName, sql, '', function (err, result) {
                if (err && err.code == 'ER_BAD_DB_ERROR') {
                    utils.sendResponse(res, 400, '2095', 'Invalid Key');
                } else if (err) {
                    utils.sendResponse(res, 500, '9999', []);
                } else {
                    utils.sendResponse(res, 200, '1001', result);
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });
    app.post('/api/em/prdlines/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var sql = ` 
                SELECT 
                    Id, Name 
                FROM 
                    Product_Line__c 
                WHERE 
                    IsDeleted = 0  
                    AND Active__c = 1 
                    ORDER BY Name ASC`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/products/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var product_line = req.body.product_line;
        var sql = `SELECT 
                        Id, Name 
                    FROM 
                    Product__c 
                    WHERE 
                        IsDeleted = 0  
                        AND Active__c = 1 
                        AND Product_Line__c = '`+ product_line + `'
                        ORDER BY Name ASC`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err && err.code == 'ER_BAD_DB_ERROR') {
                utils.sendResponse(res, 400, '2095', 'Invalid Key');
            } else if (err) {
                utils.sendResponse(res, 500, '9999', []);
            } else {
                utils.sendResponse(res, 200, '1001', result);
            }
        });
    });
    app.post('/api/em/clients/products/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var product_line = req.body.product_line;
        var product = req.body.product;
        var start_date = req.body.start_date;
        var end_date = req.body.end_date;
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
            FROM 
                Contact__c c
                    LEFT JOIN Appt_Ticket__c as a on a.Client__c=c.Id
                    LEFT JOIN Ticket_Product__c as tp on tp.Appt_Ticket__c=a.Id
                    LEFT JOIN Product__c p on p.Id=tp.Product__c
                    LEFT JOIN Product_Line__c pl on pl.Id=p.Product_Line__c
                    WHERE 
                        pl.Id='`+ product_line + `' `;
        if (product) {
            sql += `AND p.Id='` + product + `' `
        }
        sql += `AND DATE(a.Appt_Date_Time__c) >= '` + start_date + `' 
                    AND DATE(a.Appt_Date_Time__c) <= '`+ end_date + `' 
                    AND c.IsDeleted = 0
                    AND c.Email IS NOT NULL
                    AND c.Email != ''
                    GROUP by c.Email`;
        if (product_line && start_date && end_date) {
            execute.query(dbName, sql, '', function (err, result) {
                if (err && err.code == 'ER_BAD_DB_ERROR') {
                    utils.sendResponse(res, 400, '2095', 'Invalid Key');
                } else if (err) {
                    utils.sendResponse(res, 500, '9999', []);
                } else {
                    utils.sendResponse(res, 200, '1001', result);
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });
    app.post('/api/em/clients/list', function (req, res) {
        var dbName = 'stx_' + req.body.key;
        var active = req.body.active;
        var start_date = req.body.start_date;
        var end_date = req.body.end_date;
        var sql = ` 
            SELECT c.Id, c.FirstName, c.LastName, c.Email 
                FROM 
                    Contact__c c
                WHERE 
                    c.Active__c = `+ active + `
                    AND DATE(c.LastModifiedDate) >= '` + start_date + `'
                    AND DATE(c.LastModifiedDate) <='`+ end_date + `'
                    AND c.IsDeleted = 0 
                    AND c.Email IS NOT NULL 
                    AND c.Email !=''
                GROUP by c.Email`;
        if (active && start_date && end_date) {
            execute.query(dbName, sql, '', function (err, result) {
                if (err && err.code == 'ER_BAD_DB_ERROR') {
                    utils.sendResponse(res, 400, '2095', 'Invalid Key');
                } else if (err) {
                    utils.sendResponse(res, 500, '9999', []);
                } else {
                    utils.sendResponse(res, 200, '1001', result);
                }
            });
        } else {
            utils.sendResponse(res, 400, '9995', []);
        }
    });
};