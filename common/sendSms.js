var utils = require('../lib/util');
var logger = require('../lib/logger');
var execute = require('../common/dbConnection');
var config = require('config');
var client = require('twilio');
var clientConf = client(config.twilioAccountSid, config.twilioAuthToken);

module.exports = {
    sendsms: function (toAddress, messageBody, callback) {
        if (toAddress[0] === '0') {
            toAddress = toAddress.slice(1);
            if (toAddress[0] === '0') {
                toAddress = toAddress.slice(1)
            }
        }
        clientConf.messages.create({
            to: '+' + toAddress,
            from: config.fromNumber,
            body: messageBody
        }, function (error, result) {
            if (error) {
                logger.error('Error in sending Sms:', error);
                callback(error, toAddress);
            } else {
                callback(null, result);
            }
        });
    }
}
