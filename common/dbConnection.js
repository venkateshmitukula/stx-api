var mysql = require('mysql');
var logger = require('../lib/logger');
var config = require('config');

var query = function (db, query, data, done) {
    try {
        mySQLConnection = mysql.createConnection({
            // AWS
            // host: config.auroraMySQLDB.host,
            // user: config.auroraMySQLDB.user,
            // password: config.auroraMySQLDB.password,

            // Local
            host: config.MySQLDB.host,
            user: config.MySQLDB.user,
            password: config.MySQLDB.password,

            database: db,
            dateStrings: 'date',
            multipleStatements: true,
            connectTimeout: 30000
        });
        mySQLConnection.connect(function (err) {
            if (err) {
                logger.error(err);
            }
        });
        if (data === '') {
            mySQLConnection.query(query, function (err, queryResult) {
                done(err, queryResult);
            });
        } else {
            mySQLConnection.query(query, data, function (err, queryResult) {
                done(err, queryResult);
            });
        }
        mySQLConnection.end();

    } catch (e) {
        logger.log('Error in execute query');
        logger.log(e);
    }
}

module.exports.query = query;